package chess.net;

import chess.Chess;
import chess.presentation.scenes.SceneManager;
import java.io.*;
import java.net.*;

/**
 * Verwaltet den Netzwerkmodus.
 * Benutzt das Singleton-Entwurfsmuster.
 */
public class NetController {

    /**
     * Die einzige Instanz des Controllers.
     */
    private static NetController instance;
    /**
     * Zeigt an, ob der Server gerade lauft oder nicht.
     */
    private static boolean serverIsRunning;
    /**
     * Zeigt an, ob der Server gerade auf eine Verbindung wartet.
     */
    private static boolean serverIsWaiting;
    /**
     * Der Port des Servers.
     */
    private static final int port = 12345;
    /**
     * Reader des Input-Streams.
     */
    private BufferedReader in;
    /**
     * Writer des Output-Streams.
     */
    private PrintWriter out;
    /**
     * Socket des Servers.
     */
    private ServerSocket serverSocket;
    /**
     * Socket des Clients.
     */
    private Socket clientSocket;
    /**
     * Referenz auf den Listener-Thread.
     */
    private Thread listenerThread;

    private NetController() {
        serverIsRunning = false;
        serverIsWaiting = false;
    }

    /**
     * Liefert die Instanz des Controllers.
     */
    public static NetController getInstance() {
        if (instance == null) {
            instance = new NetController();
        }
        return instance;
    }

    /**
     * Startet die Server-Routine und initialisiert das Spiel.
     */
    public void startServerRoutine(final String playername) {

        new Thread(new Runnable() {

            public void run() {
                if (serverIsRunning) {
                    System.err.println("Server is already running");
                    return;
                }
                try {
                    // Socket oeffnen und auf Client warten
                    serverSocket = new ServerSocket(port);
                    System.out.println("Server started.\nWaiting for Clients...");
                    serverIsWaiting = true;
                    clientSocket = serverSocket.accept();
                    serverIsWaiting = false;
                    System.out.println("Client connected");

                    // Stroeme oeffnen
                    out = new PrintWriter(clientSocket.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                    String inputLine, clientName;
                    String[] inputLineSplit;

                    inputLine = in.readLine();

                    // Client Requset empfangen
                    inputLineSplit = inputLine.split(":");
                    if (inputLineSplit.length == 2 && inputLineSplit[0].equals("joinRequest")) {
                        clientName = inputLineSplit[1];
                    } else {
                        System.err.println("Server: Protocol-Error");
                        closeConnection();
                        return;
                    }

                    // Accept-Msg senden
                    String serverName = playername;
                    String acceptMsg = "joinAccept:" + serverName;
                    out.println(acceptMsg);

                    // Spieldaten senden
                    String[] gameData = Chess.getLogic().persistanceInputPosition();
                    StringBuffer sb = new StringBuffer();
                    sb.append("loadgame:");
                    for (int i = 0; i < gameData.length; i++) {
                        sb.append(gameData[i]);
                        if (i != (gameData.length - 1)) {
                            sb.append("/");
                        }
                    }
                    String gameDataString = sb.toString();
                    out.println(gameDataString);

                    listenerThread = new Thread(new NetListener(in));
                    listenerThread.start();

                    Chess.getLogic().newGame(serverName, clientName, "white", true, "white");
                    Chess.getPresentation().netInputPlayer(serverName, clientName);

                    serverIsRunning = true;
                    SceneManager.getInstance().goToScene("Game");

                } catch (SocketException ex) {
                    System.out.println("ServerSocket closed or Port " + port + " not free.");
                    SceneManager.getInstance().goToScene("Network");
                } catch (IOException ex) {
                    ex.printStackTrace();
                    stopServer();
                    SceneManager.getInstance().goToScene("Network");
                }
            }
        }).start();
    }

    /**
     * Startet die Client-Routine und initialisiert das Spiel.
     * @param ip IP-Adresse des Servers.
     */
    public void startClientRoutine(final String ip, final String playername) {

        new Thread(new Runnable() {

            public void run() {
                try {
                    // Verbindung aufbauen
                    clientSocket = new Socket(ip, port);
                    out = new PrintWriter(clientSocket.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                    // Request
                    String clientName = playername;
                    out.println("joinRequest:" + clientName);

                    String inputLine, serverName;
                    String[] inputLineSplit;

                    inputLine = in.readLine();
                    // System.out.println("Client-> inputLine: " + inputLine);

                    // Server-Antwort empfangen
                    inputLineSplit = inputLine.split(":");
                    if (inputLineSplit.length == 2 && inputLineSplit[0].equals("joinAccept")) {
                        serverName = inputLineSplit[1];
                    } else {
                        closeConnection();
                        SceneManager.getInstance().goToScene("Network");
                        return;
                    }

                    // Spieldaten empfangen
                    String[] positions;
                    inputLine = in.readLine();
                    inputLineSplit = inputLine.split(":");
                    if (inputLineSplit.length == 2 && inputLineSplit[0].equals("loadgame")) {
                        positions = inputLineSplit[1].split("/");
                    } else {
                        closeConnection();
                        SceneManager.getInstance().goToScene("Network");
                        return;
                    }

                    Chess.getLogic().persistanceInputPosition(serverName, clientName, positions);
                    Chess.getLogic().newGame(serverName, clientName, "white", true, "black");

                    Chess.getPresentation().netInputPlayer(serverName, clientName);

                    listenerThread = new Thread(new NetListener(in));
                    listenerThread.start();
                    SceneManager.getInstance().goToScene("Game");

                } catch (ConnectException ex) {
                    System.err.println("Server nicht gefunden");
                    SceneManager.getInstance().goToScene("Network");
                } catch (UnknownHostException ex) {
                    ex.printStackTrace();
                    SceneManager.getInstance().goToScene("Network");
                } catch (IOException ex) {
                    ex.printStackTrace();
                    SceneManager.getInstance().goToScene("Network");
                }
            }
        }).start();
    }

    /**
     * Sendet den Befehl zum Bewegen einer Figur an den anderen Spieler.
     * Formatierung der Parameter: z.B. a8, b7, h1 usw.
     *
     * @param source Das Ausgangsfeld.
     * @param target Das Zielfeld.
     */
    public void sendMoveOutput(String source, String target) {
        String formatString = "move:" + source + "/" + target;
        formatString = formatString.toLowerCase();
        if (this.out != null) {
            out.println(formatString);
        }
    }

    public void sendRemisOutput(String type) {
        String formatString = "remis:" + type;
        formatString = formatString.toLowerCase();
        if (this.out != null) {
            out.println(formatString);
        }
    }

    public void sendChatOutput(/* ... */) {
       
    }

    public void sendDisconnectOutput(/* ... */) {
    }

    public void sendResignOutput() {
        if (this.out != null) {
            out.println("resign");
        }
    }

    public void sendPromotionOutput(/* ... */) {
    }

    /**
     * Stoppt die Client-Routine.
     */
    public void stopClient() {
        if (in != null) {
            try {
                in.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if (out != null) {
            out.close();
        }
        if (clientSocket != null) {
            try {
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Stoppt den Server.
     */
    public void stopServer() {
        if (serverIsWaiting) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                serverIsWaiting = false;
            }
        }
        if (serverIsRunning) {
            listenerThread.interrupt();
            closeConnection();
        }
    }

    /**
     * Trennt die Verbindung.
     */
    private void closeConnection() {
        try {
            in.close();
            in = null;
            out.close();
            out = null;
            serverSocket.close();
            serverSocket = null;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            serverIsRunning = false;
        }
    }
}
