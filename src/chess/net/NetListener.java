package chess.net;

import chess.Chess;
import chess.logic.Field;
import chess.logic.Gameboard;
import chess.presentation.ChessFieldModel;
import chess.presentation.Engine;
import chess.presentation.scenes.SceneManager;
import chess.presentation.utilities.Players;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.ConnectException;
import javax.swing.JOptionPane;

/**
 * Der Listener-Thread fuer den Netzwerkmodus.
 * Er empfaengt Nachrichten des anderen Spielers.
 */
public class NetListener implements Runnable {

    /**
     * BufferedReader fuer den Input-Stream.
     */
    private BufferedReader in;

    public NetListener(BufferedReader in) {
        this.in = in;
    }

    public void run() {
        String inputLine;
        String[] inputLineSplit;
        while (!Thread.interrupted()) {
            try {
                inputLine = in.readLine();
                // System.out.println("NetListener empfing: " + inputLine);

                // Die beiden einzigen Nachrichten ohne ':'
                if (inputLine.equals("disconnect")) {
                    // TODO
                } else if (inputLine.equals("resign")) {
                    handleResignInput();
                }

                inputLineSplit = inputLine.split(":");

                if (inputLineSplit.length == 2) {

                    String messageType = inputLineSplit[0];

                    if (messageType.equals("chat")) {
                    } else if (messageType.equals("move")) {
                        String[] fields = inputLineSplit[1].split("/");
                        if (fields.length != 2) {
                            // Protokollerror
                            System.err.println("Illegal Message recieved: " + inputLine);
                        } else {
                            String start = fields[0];
                            String destination = fields[1];
                            handleMoveInput(start, destination);
                        }

                    } else if (messageType.equals("remis")) {
                        handleRemisInput(inputLineSplit[1]);
                    } else if (messageType.equals("promotion")) {
                    }
                }
            } catch (ConnectException ex) {
                System.out.println("NetListener->ConnectException: " + ex.getMessage());
                NetController.getInstance().stopServer();
                NetController.getInstance().stopClient();
                SceneManager.getInstance().goToScene("Network");
                break;
            } catch (IOException ex) {
                System.out.println("NetListener->IOException: " + ex.getMessage());
                NetController.getInstance().stopServer();
                NetController.getInstance().stopClient();
                SceneManager.getInstance().goToScene("Network");
                break;
            }
        }
    }

    /**
     * Behandelt einen Bewegungsbefehl.
     *
     * @param source Das Ausgangsfeld (z.B. a1).
     * @param target Das Zielfeld.
     */
    private void handleMoveInput(String source, String target) {

        int sourceX = source.toLowerCase().charAt(0) - 97;
        int sourceY = Integer.parseInt(String.valueOf(source.charAt(1))) - 1;

        int targetX = target.toLowerCase().charAt(0) - 97;
        int targetY = Integer.parseInt(String.valueOf(target.charAt(1))) - 1;

        // Ignoriert leider (noch) unsere Modularisierung...
        Gameboard.getInstance().moveFigure(new Field(sourceX, sourceY), new Field(targetX, targetY));
        Chess.getLogic().switchPlayers();
        ChessFieldModel.getInstance().setSourceField(new chess.presentation.utilities.Field(sourceX, sourceY));
        Chess.getPresentation().logicMoveInput(targetX, targetY);
    }

    private void handleChatInput(/* ... */) {
    }

    private void handleRemisInput(String type) {
        if (type.equals("offer")) {
            String[] options = {"Ja", "Nein"};
            int choice = JOptionPane.showOptionDialog(
                    Engine.getCanvas(),
                    "Ihnen wurde ein Remis angeboten.\n" +
                    "Wollen Sie akzeptieren?",
                    "Remis-Angebot",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null, options, options[0]);

            if (choice == JOptionPane.YES_OPTION) {
                NetController.getInstance().sendRemisOutput("accept");
                Chess.getPresentation().logicInputCheck(false);
                Chess.getPresentation().logicInputRemis();
            } else if (choice == JOptionPane.NO_OPTION) {
                NetController.getInstance().sendRemisOutput("deny");
            }
        } else if (type.equals("accept")) {
            Chess.getPresentation().logicInputCheck(false);
            Chess.getPresentation().logicInputRemis();
            JOptionPane.showMessageDialog(Engine.getCanvas(), "Remis wurde akzeptiert");
        } else if (type.equals("deny")) {
            JOptionPane.showMessageDialog(Engine.getCanvas(), "Remis wurde abgelehnt");
        }
    }

    private void handleResignInput() {
        JOptionPane.showMessageDialog(Engine.getCanvas(), "Der andere Spieler hat aufgegeben.\nSie haben gewonnen");
        String onTurnColer = Chess.getLogic().getOnTurnColor();
        if (onTurnColer.toLowerCase().equals("white")) {
            // Der andere Spieler gewinnt
            Chess.getPersistance().presentationInputEndgame(Players.getInstance().getPlayer2());
        } else {
            // Der andere Spieler gewinnt
            Chess.getPersistance().presentationInputEndgame(Players.getInstance().getPlayer1());
        }
    }

    private void handlePromotionInput(/* ... */) {
    }
}
