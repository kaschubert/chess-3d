package chess.persistance;

import java.util.*;

/**
 * Statistikklasse zum Zwischenspeichern der Statistiken.
 */
public class Statistics {
    /**
     * Jedem Statistikeintrag wird ein Wert zugeordnet.
     */
    private Hashtable<String, Integer> stats;

    /**
     * Konstruktor zum erstellen des Objekts.
     *
     * @param possibleStats moegliche Statistikeintraege
     */
    public Statistics(Hashtable<String, Boolean> possibleStats) {
        stats = new Hashtable<String, Integer>();

        // alle Statistiken mit 0 initialisieren
        for (String stat : possibleStats.keySet()) {
            stats.put(stat, 0);
        }
    }

    /**
     * Prueft einen Statistikeintrag auf Akkumulation und Relation.
     *
     * @param key Statistikeintrag
     * @param value zu addierender Wert
     */
    private void checkStat(String key, int value) {
        stats.put(key, stats.get(key) + value);

        if (key.equals("gewonnene Spiele") || key.equals("verlorene Spiele") || key.equals("Remis")) {
            stats.put("Anzahl Spiele", stats.get("gewonnene Spiele") + stats.get("verlorene Spiele") + stats.get("Remis"));

            if (key.equals("gewonnene Spiele")) {
                stats.put("letztes Spiel gewonnen", 1);
                stats.put("momentane Siegesreihe", stats.get("momentane Siegesreihe") + stats.get("letztes Spiel gewonnen"));
                
                if (stats.get("momentane Siegesreihe") > stats.get("laengste Siegesreihe"))
                    stats.put("laengste Siegesreihe", stats.get("momentane Siegesreihe"));
            }
            else if (key.equals("verlorene Spiele")) {
                stats.put("letztes Spiel gewonnen", 0);
                stats.put("momentane Siegesreihe", 0);
            }
        }

        else if (key.equals("Spieldauer")) {
            stats.put("gesamte Spieldauer", stats.get("gesamte Spieldauer") + stats.get("Spieldauer"));

            if (stats.get("Spieldauer") > stats.get("hoechste Spieldauer"))
                stats.put("hoechste Spieldauer", stats.get("Spieldauer"));

            if (stats.get("niedrigste Spieldauer") == 0)
                stats.put("niedrigste Spieldauer", stats.get("Spieldauer"));
            else if (stats.get("Spieldauer") < stats.get("niedrigste Spieldauer"))
                stats.put("niedrigste Spieldauer", stats.get("Spieldauer"));
        }

        else if (key.equals("Anzahl Zuege")) {
            stats.put("gesamte Anzahl Zuege", stats.get("gesamte Anzahl Zuege") + stats.get("Anzahl Zuege"));
            
            if (stats.get("Anzahl Zuege") > stats.get("hoechste Anzahl Zuege"))
                stats.put("hoechste Anzahl Zuege", stats.get("Anzahl Zuege"));

            if (stats.get("niedrigste Anzahl Zuege") == 0)
                stats.put("niedrigste Anzahl Zuege", stats.get("Anzahl Zuege"));
            else if (stats.get("Anzahl Zuege") < stats.get("niedrigste Anzahl Zuege"))
                stats.put("niedrigste Anzahl Zuege", stats.get("Anzahl Zuege"));
        }

        else if (key.equals("geschlagene Bauern") || key.equals("geschlagene Tuerme") || key.equals("geschlagene Springer") || key.equals("geschlagene Laeufer") || key.equals("geschlagene Damen"))
            stats.put("geschlagene Figuren", stats.get("geschlagene Bauern") + stats.get("geschlagene Tuerme") + stats.get("geschlagene Springer") + stats.get("geschlagene Laeufer") + stats.get("geschlagene Damen"));

        else if (key.equals("verlorene Bauern") || key.equals("verlorene Tuerme") || key.equals("verlorene Springer") || key.equals("verlorene Laeufer") || key.equals("verlorene Damen"))
            stats.put("verlorene Figuren", stats.get("verlorene Bauern") + stats.get("verlorene Tuerme") + stats.get("verlorene Springer") + stats.get("verlorene Laeufer") + stats.get("verlorene Damen"));

        if (stats.get("Anzahl Spiele") > 0) {
            if (key.equals("Spieldauer"))
                stats.put("durchschnittliche Spieldauer", stats.get("gesamte Spieldauer")/stats.get("Anzahl Spiele"));
            else if (key.equals("Anzahl Zuege"))
                stats.put("durchschnittliche Anzahl Zuege", stats.get("gesamte Anzahl Zuege")/stats.get("Anzahl Spiele"));
        }
    }

    /**
     * Einen Statiktikeintrag eines Spielers um eine bestimmt Groesse erhoehen.
     *
     * @param key Statistikeintrag
     * @param value zu addierender Wert
     */
    public void addStat(String key, int value) {
        checkStat(key, value);
    }

    /**
     * Einen bestimmten Statiktikeintrag eines Spielers um 1 erhoehen.
     *
     * @param key Statistikeintrag
     */
    public void addStat(String key) {
        checkStat(key, 1);
    }

    /**
     * Wiedergabe des Werts eines Statiskeintrags.
     *
     * @param key Statistikeintrag
     * @return Inhalt des Eintrags
     */
    public int getStat(String key) {
        return stats.get(key);
    }
}
