package chess.persistance;

import java.util.Hashtable;

/**
 * Interface zur Persistenzklasse zur Statisk- und Spielstandverwaltung,
 * diese Methoden sind fuer die Kommunikation mit der Logik sowie der Praesentation erforderlich.
 */
public interface IPersistance {

    /**
     * Liefert alle Statistikstrings.
     * 
     * @return alle Statistikstrings
     */
    public Hashtable<String, Boolean> getPossibleStats();

    /**
     * Liefert alle in der Datenbank gespeicherten Statistiken.
     * 
     * @return alle in der Datenebank gespeicherten Statistiken
     */
    public Hashtable<String, Statistics> getStatistics();

    /**
     * Liefert alle Savegames in einem String Array zurück.
     * 
     * @return alle Savegames
     */
    public String[] getSavenames();

    /**
     * Einen Statiktikeintrag eines Spielers um eine bestimmt Groesse erhoehen.
     *
     * @param name Name des Spielers
     * @param key Statistikeintrag
     * @param value zu addierender Wert
     */
    public void logicInput(String name, String key, int value);
	
    /**
     * Einen bestimmten Statiktikeintrag eines Spielers um 1 erhoehen.
     *
     * @param name Name des Spielers
     * @param key Statistikeintrag
     */
    public void logicInput(String name, String key);

    /**
     * Uebergabe des Schachlogs.
     *
     * @param notation Ausfuehrliche algebraische Notation des Zugs
     */
    public void logicInputLog(String notation);
	
    /**
     * Diese Methode veranlasst das Speichern eines Spieles.
     */
    public void presentationInputSave();
	
    /**
     * Diese Methode veranlasst das Laden eines Spieles.
     *
     * @param savename Name des gespeicherten Spiels
     */
    public void presentationInputLoad(String savename);

    /**
     * Wird am Anfang eines neuen Spiels aufgerufen.
     * Registriert die beiden Spieler in der Statistik.
     *
     * @param name1 Name des ersten Spielers
     * @param name2 Name des zweiten Spielers
     */
    public void presentationInputStartgame(String name1, String name2);

    /**
     * Wird am Ende eines Spiels aufgerufen.
     * Erstellt ein Replay des beendeten Spiels und schreibt die Statistik in die Datenbank.
     *
     * @param winner Gewinner des Spiels ("" = Remis)
     */
    public void presentationInputEndgame(String winner);
}