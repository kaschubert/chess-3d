package chess.persistance;

import chess.*;
import java.util.*;
import java.sql.*;
import java.text.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Persistenzklasse zum speichern und laden von Spielstaenden sowie der Verwaltung
 * aller Spielerstatistiken in einer embedded H2-Datenbank.
 *
 * Datenbank: H2 (http://www.h2database.com/)
 *
 * DB Einstellungen per .jar:
 * java -cp h2*.jar org.h2.tools.Server
 *
 * User: chess
 * Password: Schachmatt
 */
public class ChessPersistance implements IPersistance {
    /**
     * Jeder Statistikeintrag wird als String gespeichert, wenn der bool false ist,
     * dann wird dieser Statistikeintrag nur Persistenzintern verwendet.
     */
    private Hashtable<String, Boolean> possibleStats;

    /**
     * Jeder Spieler erhaelt seine eigene Statistik, falls sie noch nicht existiert
     * wird sie in der Datenbank neu angelegt, anonsten nur ausgelesen.
     */
    private Hashtable<String, Statistics> players;

    /**
     * Enthaelt die Namen der Spieler welche derzeit spielen. Wenn der bool true ist,
     * dann existiert der Spieler noch nicht in der Datenbank.
     */
    private Hashtable<String, Boolean> currentplayers;

    /**
     * Enthaelt den bisherigen Spielablauf.
     */
    private ArrayList<String> notationLog;

    /**
     * Connection-Objekt um zur Datenbank eine Verbindung herzustellen.
     */
    private Connection connection;

    /**
     * Datenbank existiert.
     */
    private boolean dbexists;
    
    /**
     * Konstruktor zum erstellen des Objekts.
     */
    public ChessPersistance() {
        possibleStats = new Hashtable<String, Boolean>();
        possibleStats.put("Anzahl Spiele", false);
        possibleStats.put("gewonnene Spiele", false);
        possibleStats.put("verlorene Spiele", false);
        possibleStats.put("Remis", false);
        
        possibleStats.put("letztes Spiel gewonnen", false);
        possibleStats.put("momentane Siegesreihe", false);
        possibleStats.put("laengste Siegesreihe", false);

        possibleStats.put("Spieldauer", true);
        possibleStats.put("gesamte Spieldauer", false);
        possibleStats.put("durchschnittliche Spieldauer", false);
        possibleStats.put("hoechste Spieldauer", false);
        possibleStats.put("niedrigste Spieldauer", false);

        possibleStats.put("Anzahl Zuege", true);
        possibleStats.put("gesamte Anzahl Zuege", false);
        possibleStats.put("durchschnittliche Anzahl Zuege", false);
        possibleStats.put("hoechste Anzahl Zuege", false);
        possibleStats.put("niedrigste Anzahl Zuege", false);

        possibleStats.put("geschlagene Figuren", false);
        possibleStats.put("verlorene Figuren", false);

        possibleStats.put("geschlagene Bauern", true);
        possibleStats.put("verlorene Bauern", true);

        possibleStats.put("geschlagene Tuerme", true);
        possibleStats.put("verlorene Tuerme", true);

        possibleStats.put("geschlagene Springer", true);
        possibleStats.put("verlorene Springer", true);

        possibleStats.put("geschlagene Laeufer", true);
        possibleStats.put("verlorene Laeufer", true);

        possibleStats.put("geschlagene Damen", true);
        possibleStats.put("verlorene Damen", true);

        possibleStats.put("Anzahl Bauernumwandlungen", true);
        possibleStats.put("Anzahl Rochaden", true);
        possibleStats.put("Anzahl en-passants", true);

        possibleStats.put("gegnerischer Koenig Schach gesetzt", true);
        possibleStats.put("eigener Koenig Schach gesetzt", true);

        players = new Hashtable<String, Statistics>();
        currentplayers = new Hashtable<String, Boolean>();
        notationLog = new ArrayList<String>();

        // Satistikeintraege an Logik uebergeben
        String[] statistics = new String[17];

        int i = 0;
        for (String stat : possibleStats.keySet())
            if (possibleStats.get(stat))
                statistics[i++] = stat;

        //Chess.getLogic().persistanceInput(statistics);

        // Spielerstatistiken aus der Datenbank auslesen
        openConnection();
        if (!dbexists)
            createDatabase();

        try {
            ResultSet selectPlayers = connection.createStatement().executeQuery("SELECT * FROM Statistik");
            while (selectPlayers.next()) {
                Statistics statistic = new Statistics(possibleStats);

                for (String stat : possibleStats.keySet())
                    if (!possibleStats.get(stat))
                        statistic.addStat(stat, selectPlayers.getInt(stat));

                players.put(selectPlayers.getString("Name").toLowerCase(), statistic);
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        /* Check
        for (String name : players.keySet())
            for (String stat : possibleStats.keySet())
                //if (!possibleStats.get(stat))
                    System.out.println(name + "=" + stat + "=" + players.get(name).getStat(stat));
         */
        
        closeConnection();
    }

    /**
     * Erstellt die Datenbank inklusive Tabellen. Oeffnet die Datenbankverbindung.
     */
    private void createDatabase() {
        try {
            // Treiber laden
            Class.forName("org.h2.Driver");

            if (connection == null || connection.isClosed())
                connection = DriverManager.getConnection("jdbc:h2:statistics", "chess", "Schachmatt");

            // Tabelle erstellen
            connection.createStatement().execute(
                    "CREATE TABLE Statistik(" +
                    "Name VARCHAR(255) PRIMARY KEY, " +
                    "\"Anzahl Spiele\" INT, " +
                    "\"gewonnene Spiele\" INT, " +
                    "\"verlorene Spiele\" INT, " +
                    "\"Remis\" INT, " +
                    "\"letztes Spiel gewonnen\" INT, " +
                    "\"momentane Siegesreihe\" INT, " +
                    "\"laengste Siegesreihe\" INT, " +
                    "\"gesamte Spieldauer\" INT, " +
                    "\"durchschnittliche Spieldauer\" INT, " +
                    "\"hoechste Spieldauer\" INT, " +
                    "\"niedrigste Spieldauer\" INT, " +
                    "\"gesamte Anzahl Zuege\" INT, " +
                    "\"durchschnittliche Anzahl Zuege\" INT, " +
                    "\"hoechste Anzahl Zuege\" INT, " +
                    "\"niedrigste Anzahl Zuege\" INT, " +
                    "\"geschlagene Figuren\" INT, " +
                    "\"verlorene Figuren\" INT);"
                    );
        }
        catch (Exception e) { }

        dbexists = true;
    }

    /**
     * Stellt die Datenbankverbindung her.
     */
    private void openConnection() {
        try {
            // Treiber laden
            Class.forName("org.h2.Driver");

            if (connection == null || connection.isClosed())
                connection = DriverManager.getConnection("jdbc:h2:statistics;IFEXISTS=TRUE", "chess", "Schachmatt");

            dbexists = true;
        }
        catch (Exception e) {
            if (e.getMessage().contains("Database") && e.getMessage().contains("not found"))
                dbexists = false;
        }
    }

    /**
     * Schliesst die Datenbankverbindung.
     */
    private void closeConnection() {
        try {
            if (connection != null && !connection.isClosed())
                connection.close();
        }
        catch (Exception e) { }
    }

    public void logicInput(String name, String key, int value) {
        name = name.toLowerCase();

        if (players.containsKey(name) && possibleStats.containsKey(key) && possibleStats.get(key))
            players.get(name).addStat(key, value);
    }

    public void logicInput(String name, String key) {
        name = name.toLowerCase();

        if (players.containsKey(name) && possibleStats.containsKey(key) && possibleStats.get(key))
            players.get(name).addStat(key);
    }

    public void logicInputLog(String notation) {
        notationLog.add(notation);
    }

    public void presentationInputSave() {
        // Position der Figuren holen
        String[] positions = Chess.getLogic().persistanceInputPosition();

        String savename = "";

        for (String name : currentplayers.keySet())
            savename += name + "_";

        savename += new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
        File save = new File("savegames/" + savename + "_" + notationLog.size() + ".save");

        try {
            new File("savegames").mkdir();

            if (save.createNewFile()) {
                BufferedWriter saveout = new BufferedWriter(new FileWriter(save));

                for (String position : positions)
                    saveout.write("pos=" + position + "\n");

                for (String notation : notationLog)
                    saveout.write("log=" + notation + "\n");

                for (String name : currentplayers.keySet()) {
                    saveout.write("name=" + name + "\n");
                    Statistics statistic = players.get(name);

                    for (String stat : possibleStats.keySet())
                        if (possibleStats.get(stat))
                            saveout.write(stat + "=" + statistic.getStat(stat) + "\n");
                }

                saveout.close();
            }
        }
        catch (Exception e) { }
    }

    public void presentationInputLoad(String savename) {
        File save = new File("savegames/" + savename);

        String name1 = "", name2 = "";
        ArrayList<String> positions = new ArrayList<String>();

        try {
            if (save.exists()) {
                BufferedReader savein = new BufferedReader(new FileReader(save));

                String line, name = "";
                String[] lineparts;
                while ((line = savein.readLine()) != null) {
                    lineparts = line.split("=");
                    if (lineparts.length < 2)
                        continue;

                    if (lineparts[0].equals("pos"))
                        positions.add(lineparts[1]);

                    else if (lineparts[0].equals("log"))
                        notationLog.add(lineparts[1]);

                    else if (lineparts[0].equals("name")) {
                        name = lineparts[1];

                        if (name1.equals(""))
                            name1 = name;
                        else
                            name2 = name;

                        if (!players.containsKey(name)) {
                            // neue Spielerstatistik anlegen
                            players.put(name, new Statistics(possibleStats));
                            currentplayers.put(name, true);
                        }
                        else {
                            // Spieler ist bereits vorhanden
                            currentplayers.put(name, false);
                        }
                    }

                    else
                        players.get(name).addStat(lineparts[0], Integer.parseInt(lineparts[1]));
                }

                savein.close();
            }
        }
        catch (Exception e) { }

        String[] temp = new String[positions.size()];
        Chess.getLogic().persistanceInputPosition(name1, name2, positions.toArray(temp));
        //Chess.getPresentation().persistanceInput((String[]) notationLog.toArray());

    }

    public void presentationInputStartgame(String name1, String name2) {
        ArrayList<String> names = new ArrayList<String>();
        names.add(name1.toLowerCase());
        names.add(name2.toLowerCase());

        for (String name : names) {
            if (!players.containsKey(name)) {
                // neue Spielerstatistik anlegen
                players.put(name, new Statistics(possibleStats));
                currentplayers.put(name, true);
            }
            else {
                // Spieler ist bereits vorhanden
                currentplayers.put(name, false);
            }
        }
    }

    public void presentationInputEndgame(String winner) {
        // Gewinnsituation auswerten + Replay speichern
        winner = winner.toLowerCase();
        String replayname = "";

        for (String name : currentplayers.keySet()) {
            if (winner.equals(""))
                players.get(name).addStat("Remis");
            else if (winner.equals(name))
                players.get(name).addStat("gewonnene Spiele");
            else
                players.get(name).addStat("verlorene Spiele");

            replayname += name + "_";
        }
        
        replayname += new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
        File replay = new File("replays/" + replayname + ".replay");
        
        try {
            new File("replays").mkdir();

            if (replay.createNewFile()) {
                BufferedWriter replayout = new BufferedWriter(new FileWriter(replay));

                for (String notation : notationLog)
                    replayout.write("log=" + notation + "\n");
                
                replayout.close();
                notationLog.clear();
            }
        }
        catch (Exception e) { }

        // Spielerstatistiken in die Datenbank speichern
        openConnection();

        try {
            for (String name : currentplayers.keySet()) {
                Statistics statistic = players.get(name);

                if (currentplayers.get(name)) {
                    // Spieler steht noch nicht in der Datenbank -> INSERT INTO
                    
                    String  keys = "name",
                            values = "'" + name + "'";
                    for (String stat : possibleStats.keySet()) {
                        if (!possibleStats.get(stat)) {
                            keys += ", \"" + stat + "\"";
                            values += ", " + statistic.getStat(stat);
                        }
                    }

                    connection.createStatement().execute("INSERT INTO Statistik (" + keys + ") VALUES(" + values + ");");
                    }
                else {
                    // Spieler steht bereits in der Datenbank -> UPDATE

                    String updates = "";
                    for (String stat : possibleStats.keySet())
                        if (!possibleStats.get(stat))
                            updates += "\"" + stat + "\"='" + statistic.getStat(stat) + "', ";

                    connection.createStatement().execute("UPDATE Statistik SET " + updates.substring(0, updates.length()-2) + " WHERE name='" + name + "';");
                }
            }
        }
        catch (Exception e) { }
        
        closeConnection();

        /*
        String[] statistics = new String[34];

        int i = 0;
        for (String stat : possibleStats.keySet())
            if (possibleStats.get(stat))
                for (String name : currentplayers.keySet())
                    statistics[i++] = name + "=" + stat + "=" + players.get(name).getStat(stat);

        */

        currentplayers.clear();
    }

    public String[] getSavenames() {
        if (new File("savegames").exists()) {
            File[] savefiles = new File("savegames").listFiles();
            String[] savestrings = new String[savefiles.length];
            int counter = 0;

            for (File file : savefiles) {
                savestrings[counter++] = file.getName();
            }

            return savestrings;
        }
        else
            return null;
    }

    public Hashtable<String, Statistics> getStatistics() {
        return players;
    }

    public Hashtable<String, Boolean> getPossibleStats() {
        return possibleStats;
    }

}