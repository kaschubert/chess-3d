package chess.logic.rules;

public class KingRule extends ARule {

    /**
     * Erzeugt die Regel fuer den Koenig.
     */
    public KingRule() {
        vectors = new Vector[8];
        vectors[0] = new Vector(0, 1, false);  // Norden
        vectors[1] = new Vector(1, 0, false);  // Osten
        vectors[2] = new Vector(0, -1, false); // Sueden
        vectors[3] = new Vector(-1, 0, false); // Westen
        vectors[4] = new Vector(1, 1, false);  // Nordost
        vectors[5] = new Vector(-1, 1, false); // Nordwest
        vectors[6] = new Vector(1, -1, false); // Suedost
        vectors[7] = new Vector(-1, -1, false);// Suedwest
    }
}
