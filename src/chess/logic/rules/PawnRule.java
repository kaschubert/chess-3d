package chess.logic.rules;

import chess.logic.Color;
import chess.logic.Field;
import chess.logic.Gameboard;
import chess.logic.figures.IGameFigure;
import chess.logic.figures.King;
import chess.logic.figures.Knight;
import chess.logic.figures.Pawn;
import java.util.ArrayList;

public class PawnRule extends ARule {

    /**
     * Erzeugt die Regel fuer den Bauern.
     */
    public PawnRule() {
    }

    @Override
    public Field[] getMoves(IGameFigure figure) {

        Color color = figure.getColor();

        Gameboard gb = Gameboard.getInstance();
        Field position = figure.getPosition();
        ArrayList<Field> possibleFields = new ArrayList<Field>();

        if (!(figure instanceof Pawn)) {
            // Kommt nicht vor
            throw new RuntimeException("Error: PawnRule was used with another figure");
        }

        Pawn pawn = (Pawn) figure;

        Vector forward, attackLeft, attackRight;
        if (pawn.getColor() == Color.WHITE) {
            forward = new Vector(0, 1, false);
            attackLeft = new Vector(-1, 1, false);
            attackRight = new Vector(1, 1, false);
        } else {
            forward = new Vector(0, -1, false);
            attackLeft = new Vector(-1, -1, false);
            attackRight = new Vector(1, -1, false);
        }

        // Geradeaus laufen
        int x = position.getX();
        int y = position.getY();

        x += forward.getX();
        y += forward.getY();

        if (Field.exists(x, y)) {
            Field newField = new Field(x, y);
            if (!gb.isFieldOccupied(newField)) {
                possibleFields.add(newField);
                // Doppelschritt
                if (!pawn.hasMoved()) {
                    x += forward.getX();
                    y += forward.getY();
                    if (Field.exists(x, y)) {
                        newField = new Field(x, y);
                        if (!gb.isFieldOccupied(newField)) {
                            possibleFields.add(newField);
                        }
                    }
                }
            }
        }

        // Eventuell Figur schlagen
        x = position.getX();
        y = position.getY();
        x += attackLeft.getX();
        y += attackLeft.getY();
        if (Field.exists(x, y)) {
            Field newField = new Field(x, y);
            if (gb.isFieldOccupied(newField)) {
                if (pawn.getColor() != gb.getFigure(newField).getColor()) {
                    possibleFields.add(newField);
                }
            }
        }

        x = position.getX();
        y = position.getY();
        x += attackRight.getX();
        y += attackRight.getY();
        if (Field.exists(x, y)) {
            Field newField = new Field(x, y);
            if (gb.isFieldOccupied(newField)) {
                if (pawn.getColor() != gb.getFigure(newField).getColor()) {
                    possibleFields.add(newField);
                }
            }
        }
        
        if(pawn.getEnPassantField() != null){
            possibleFields.add(pawn.getEnPassantField());
        }

        Field[] posF = new Field[possibleFields.size()];
        posF = possibleFields.toArray(posF);


        // Ermitteln, ob die Figur ueberhaupt ziehen darf
        King king = gb.getKing(color);
        if (king.isCheck() && !(figure instanceof King)) {

            ArrayList<Field> fields = new ArrayList<Field>();
            // Kann die bedrohende Figur geschlagen werden?
            IGameFigure attackingFigure = king.getAttackingFigure();
            Field attackPosition = attackingFigure.getPosition();
            for (Field f : posF) {
                if (f.equals(attackPosition)) {
                    fields.add(f);
                }
            }

            // Kann die Figur dazwischengestellt werden?
            if ((attackingFigure instanceof Knight) || (attackingFigure instanceof Pawn)) {
                // Geht bei Bauern oder Springern nicht
                Field[] f = new Field[fields.size()];
                return fields.toArray(f);
            }

            Field kingPosition = king.getPosition();
            Field[] betweenFields = Field.getFieldsBetween(kingPosition, attackPosition);

            for (Field f : posF) {
                for (Field b : betweenFields) {
                    if (f.equals(b)) {
                        fields.add(f);
                    }
                }
            }
            Field[] f = new Field[fields.size()];
            return fields.toArray(f);
        } else {
            return posF;
        }
    }
}
