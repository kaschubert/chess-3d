package chess.logic.rules;

public class KnightRule extends ARule {

    /**
     * Erzeugt die Regel fuer den Springer.
     */
    public KnightRule() {
        vectors = new Vector[8];
        vectors[0] = new Vector(1, 2, false);
        vectors[1] = new Vector(2, 1, false);
        vectors[2] = new Vector(2, -1, false);
        vectors[3] = new Vector(1, -2, false);
        vectors[4] = new Vector(-1, -2, false);
        vectors[5] = new Vector(-2, -1, false);
        vectors[6] = new Vector(-2, 1, false);
        vectors[7] = new Vector(-1, 2, false);
    }
}
