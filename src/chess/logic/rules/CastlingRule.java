package chess.logic.rules;

import chess.logic.Color;
import chess.logic.Field;
import chess.logic.Gameboard;
import chess.logic.figures.IGameFigure;
import chess.logic.figures.King;
import chess.logic.figures.Rook;
import java.util.ArrayList;

public class CastlingRule extends ARule {

    /**
     * Erzeugt die Regel fuer die Rochade.
     */
    public CastlingRule() {
    }

    /**
     * Wenn eine Rochade moeglich ist, liefert diese Methode das Zielfeld fuer den Koenig, anderenfalls <i>null</i>.
     * @param figure Der Koenig.
     * @return Die Zielfelder fuer den Koenig, bzw. null wenn keine Rochade moeglich.
     */
    @Override
    public Field[] getMoves(IGameFigure figure) {
        Gameboard gb = Gameboard.getInstance();
        ArrayList<Field> possibleFields = new ArrayList<Field>();

        if (!(figure instanceof King)) {
            // Kommt nicht vor
            throw new RuntimeException("The CastlingRule may only be used by the king");
        }

        King king = (King) figure;

        if (king.hasMoved() || king.isCheck()) {
            return null;
        }

        if (king.getColor() == Color.WHITE) {
            // Linker weisser Turm
            if (checkRook(new Field('a', 1))) {
                // Felder zwischen Koenig und Turm checken
                Field b1, c1, d1;

                b1 = new Field('b', 1);
                c1 = new Field('c', 1);
                d1 = new Field('d', 1);

                if (!gb.isFieldOccupied(b1) && !gb.isFieldUnderAttack(b1, Color.BLACK, false)) {

                    if (!gb.isFieldOccupied(c1) && !gb.isFieldUnderAttack(c1, Color.BLACK, false)) {

                        if (!gb.isFieldOccupied(d1) && !gb.isFieldUnderAttack(d1, Color.BLACK, false)) {
                            // Rochade moeglich
                            possibleFields.add(new Field('c', 1));
                        }
                    }
                }
            }
            // Rechter weisser Turm
            if (checkRook(new Field('h', 1))) {
                // Felder zwischen Koenig und Turm checken
                Field f1, g1;

                f1 = new Field('f', 1);
                g1 = new Field('g', 1);

                if (!gb.isFieldOccupied(f1) && !gb.isFieldUnderAttack(f1, Color.BLACK, false)) {

                    if (!gb.isFieldOccupied(g1) && !gb.isFieldUnderAttack(g1, Color.BLACK, false)) {
                        // Rochade moeglich
                        possibleFields.add(new Field('g', 1));
                    }
                }
            }
        } else {
            // Linker schwarzer Turm
            if (checkRook(new Field('a', 8))) {
                // Felder zwischen Koenig und Turm checken
                Field b8, c8, d8;

                b8 = new Field('b', 8);
                c8 = new Field('c', 8);
                d8 = new Field('d', 8);

                if (!gb.isFieldOccupied(b8) && !gb.isFieldUnderAttack(b8, Color.WHITE, false)) {

                    if (!gb.isFieldOccupied(c8) && !gb.isFieldUnderAttack(c8, Color.WHITE, false)) {

                        if (!gb.isFieldOccupied(d8) && !gb.isFieldUnderAttack(d8, Color.WHITE, false)) {
                            // Rochade moeglich
                            possibleFields.add(new Field('c', 8));
                        }
                    }
                }
            }
            // Rechter schwarzer Turm
            if (checkRook(new Field('h', 8))) {
                // Felder zwischen Koenig und Turm checken
                Field f8, g8;

                f8 = new Field('f', 8);
                g8 = new Field('g', 8);

                if (!gb.isFieldOccupied(f8) && !gb.isFieldUnderAttack(f8, Color.WHITE, false)) {

                    if (!gb.isFieldOccupied(g8) && !gb.isFieldUnderAttack(g8, Color.WHITE, false)) {
                        // Rochade moeglich
                        possibleFields.add(new Field('g', 8));
                    }
                }
            }
        }
        Field[] f = new Field[possibleFields.size()];
        return possibleFields.toArray(f);
    }

    /**
     * Prueft, ob auf dem gegebenen Feld ein Turm steht und ob dieser noch nicht bewegt wurde.
     *
     * @param field Ein Feld.
     * @return Ein unbewegter Turm steht auf dem Feld ? true : false
     */
    private boolean checkRook(Field field) {
        IGameFigure figure = Gameboard.getInstance().getFigure(field);

        if (figure != null) {
            if (figure instanceof Rook) {
                Rook rook = (Rook) figure;
                if (!rook.hasMoved()) {
                    return true;
                }
            }
        }
        return false;
    }
}
