package chess.logic.rules;

public class BishopRule extends ARule {

    /**
     * Erzeugt die Regel fuer den Laeufer.
     */
    public BishopRule() {
        vectors = new Vector[4];
        vectors[0] = Vector.NORTHEAST;
        vectors[1] = Vector.NORTHWEST;
        vectors[2] = Vector.SOUTHEAST;
        vectors[3] = Vector.SOUTHWEST;
    }
}
