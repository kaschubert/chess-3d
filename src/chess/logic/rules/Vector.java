package chess.logic.rules;

/**
 *  Diese Klasse repraesentiert <b>eine</b> der moeglichen Zugrichtungen einer Figur.
 *
 */
public class Vector {

    /**
     * Vector fuer beliebig viele Schritte in Noerdliche Richtung.
     */
    public static final Vector NORTH = new Vector(0, 1, true);
    /**
     * Vector fuer beliebig viele Schritte in Oestliche Richtung.
     */
    public static final Vector EAST = new Vector(1, 0, true);
    /**
     * Vector fuer beliebig viele Schritte in Suedliche Richtung.
     */
    public static final Vector SOUTH = new Vector(0, -1, true);
    /**
     * Vector fuer beliebig viele Schritte in Westliche Richtung.
     */
    public static final Vector WEST = new Vector(-1, 0, true);
    /**
     * Vector fuer beliebig viele Schritte in Nordoestliche Richtung.
     */
    public static final Vector NORTHEAST = new Vector(1, 1, true);
    /**
     * Vector fuer beliebig viele Schritte in Nordwestliche Richtung.
     */
    public static final Vector NORTHWEST = new Vector(-1, 1, true);
    /**
     * Vector fuer beliebig viele Schritte in Suedoestliche Richtung.
     */
    public static final Vector SOUTHEAST = new Vector(1, -1, true);
    /**
     * Vector fuer beliebig viele Schritte in Suedwestliche Richtung.
     */
    public static final Vector SOUTHWEST = new Vector(-1, -1, true);
    /**
     * Die Anzahl der Felder in x-Richtung
     */
    private final int x;
    /**
     * Die Anzahl der Felder in y-Richtung
     */
    private final int y;
    /**
     * Dieses Flag bestimmt, ob eine Figur mehrere Schritte in die im Vector angegebene Richtung gehen kann.
     * Der Springer kann beispielweise nur einen Schritt machen, die meisten anderen Figuren mehrere.
     */
    private final boolean multiple;

    /**
     * Erzeugt einen neuen Vector.
     * Ein Vector repraesentiert eine moegliche Zugrichtung einer Figur.
     * @param x Die Anzahl der Felder in x-Richtung.
     * @param y Die Anzahl der Felder in y-Richtung.
     * @param multiple Bestimmt, ob eine Figur mehrere Schritte in die im Vector angegebene Richtung gehen kann.
     */
    public Vector(int x, int y, boolean multiple) {
        this.x = x;
        this.y = y;
        this.multiple = multiple;
    }

    /**
     * Liefert true zurueck, wenn eine Figur mehrere Schritte in die im Vector angegebene Richtung gehen kann.
     * @return Mehrere Schritte moeglich? true : false
     */
    public boolean isMultiple() {
        return multiple;
    }

    /**
     * Liefert die Anzahl der Felder, die eine Figur waehrend eines Schrittes in x-Richtung gehen darf.
     * @return Anzahl der Felder
     */
    public int getX() {
        return x;
    }

    /**
     * Liefert die Anzahl der Felder, die eine Figur waehrend eines Schrittes in y-Richtung gehen darf.
     * @return Anzahl der Felder
     */
    public int getY() {
        return y;
    }
}
