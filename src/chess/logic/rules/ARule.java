package chess.logic.rules;

import chess.logic.Color;
import chess.logic.Field;
import chess.logic.Gameboard;
import chess.logic.figures.IGameFigure;
import chess.logic.figures.King;
import chess.logic.figures.Knight;
import chess.logic.figures.Pawn;
import java.util.ArrayList;

public abstract class ARule {

    /**
     * Dieses Array enthaelt alle Vectoren, die zu dieser Regel gehoeren.
     */
    protected Vector[] vectors;

    /**
     * Diese Methode ermittelt alle Felder, die durch diese Regel erreicht werden koennen.
     *
     * Andere Figuren, die den Weg versperren, werden dabei beruecksichtigt.
     * Moegliche Schach-Situationen weden ebenfalls beruecksichtigt.
     *
     * @param figure Die aufrufende Figur.
     * @return Alle moeglichen Felder.
     */
    public Field[] getMoves(IGameFigure figure) {
        ArrayList<Field> fields = new ArrayList<Field>();
        Gameboard gb = Gameboard.getInstance();
        Color color = figure.getColor();

        Field[] possibleFields = useVectors(figure, false);

        // Ermitteln, ob die Figur ueberhaupt ziehen darf
        King king = gb.getKing(color);
        if (king.isCheck() && !(figure instanceof King)) {
            // Kann die bedrohende Figur geschlagen werden?
            IGameFigure attackingFigure = king.getAttackingFigure();
            Field attackPosition = attackingFigure.getPosition();
            for (Field f : possibleFields) {
                if (f.equals(attackPosition)) {
                    fields.add(f);
                }
            }

            // Kann die Figur dazwischengestellt werden?
            if ((attackingFigure instanceof Knight) || (attackingFigure instanceof Pawn)) {
                // Geht bei Bauern oder Springern nicht
                Field[] f = new Field[fields.size()];
                return fields.toArray(f);
            }

            Field kingPosition = king.getPosition();
            Field[] betweenFields = Field.getFieldsBetween(kingPosition, attackPosition);

            for (Field f : possibleFields) {
                for (Field b : betweenFields) {
                    if (f.equals(b)) {
                        fields.add(f);
                    }
                }
            }
            Field[] f = new Field[fields.size()];
            return fields.toArray(f);
        } else {
            return possibleFields;
        }
    }

    public Field[] getAttacks(IGameFigure figure) {
        return useVectors(figure, true);
    }

    /**
     * Diese Methode ermittelt alle Felder, die durch die Vektoren dieser Regel erreicht werden koennen.
     *
     * Andere Figuren, die den Weg versperren, werden dabei beruecksichtigt.
     * @param position Die aufrufende Figur.
     * @param attack true, wenn die bedrohten Felder ermittlet werden sollen.
     * @return Alle moeglichen Felder.
     */
    private Field[] useVectors(IGameFigure figure, boolean attack) {
        int x, y;
        ArrayList<Field> fields = new ArrayList<Field>();
        Gameboard gb = Gameboard.getInstance();

        Field position = figure.getPosition();
        Color color = figure.getColor();
        for (Vector v : vectors) {

            x = position.getX();
            y = position.getY();

            while (true) {
                // Neues Feld ermitteln
                x += v.getX();
                y += v.getY();

                // Pruefen, ob es sich um ein legales Feld handelt
                if (!Field.exists(x, y)) {
                    // Außerhalb der Spielfeldgrenzen -> weitermachen mit naechstem Vector
                    break;
                }

                Field f = new Field(x, y);

                // Pruefen, ob das Feld belegt ist
                if (gb.isFieldOccupied(f)) {
                    // Wenn die bedrohten(!) Felder ermittelt werden sollen, wird das Feld
                    // hinzugefuegt, egal welche farbe die Figur hat.
                    if(attack){
                        fields.add(f);
                        break;
                    }
                    // Gegnerische Figur darauf?
                    if (gb.getFigure(f).getColor() != color) {
                        // Lt. Spezifikation bekommt die Praesentation auch ein belegtes Feld
                        // uebergeben, wenn sich eine gegnerische Figur darauf befindet
                        fields.add(f);
                    }
                    break;
                }

                fields.add(f);

                if (!v.isMultiple()) {
                    // Schleifendurchlauf abbrechen, wenn
                    // der Vector nur einmal verwendet werden darf
                    break;
                }
            }
        }
        Field[] f = new Field[fields.size()];
        return fields.toArray(f);
    }
}
