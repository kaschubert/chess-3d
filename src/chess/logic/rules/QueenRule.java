package chess.logic.rules;

public class QueenRule extends ARule {

    /**
     * Erzeugt die Regel fuer die Dame.
     */
    public QueenRule() {
        vectors = new Vector[8];
        vectors[0] = Vector.NORTH;
        vectors[1] = Vector.NORTHEAST;
        vectors[2] = Vector.EAST;
        vectors[3] = Vector.SOUTHEAST;
        vectors[4] = Vector.SOUTH;
        vectors[5] = Vector.SOUTHWEST;
        vectors[6] = Vector.WEST;
        vectors[7] = Vector.NORTHWEST;
    }
}
