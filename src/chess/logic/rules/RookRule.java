package chess.logic.rules;

public class RookRule extends ARule {

    /**
     * Erzeugt die Regel fuer den Turm.
     */
    public RookRule() {
        vectors = new Vector[4];
        vectors[0] = Vector.EAST;
        vectors[1] = Vector.NORTH;
        vectors[2] = Vector.WEST;
        vectors[3] = Vector.SOUTH;
    }
}
