package chess.logic;

/**
 * Die Farben für die beiden Spieler
 */
public enum Color {

    WHITE, BLACK
}

