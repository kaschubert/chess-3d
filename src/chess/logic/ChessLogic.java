package chess.logic;

import chess.Chess;
import chess.logic.figures.*;
import chess.logic.figures.Pawn;
import chess.net.NetController;
import chess.persistance.IPersistance;
import chess.presentation.ChessFieldModel;
import chess.presentation.IPresentation;
import java.util.ArrayList;

public class ChessLogic implements ILogic {

    /**
     * Referenz auf die Praesentation.
     */
    private IPresentation presentation;
    /**
     * Referenz auf die Persistenz.
     */
    private IPersistance persistance;
    /**
     * Dient zur Zustandsunterscheidung.
     * Modus false: Es ist keine Figur selektiert.
     * Modus true: Es ist eine Figur selektiert und die moeglichen Felder sind angezeigt.
     */
    private boolean actionMode;
    /**
     * Referenz auf das Spielbrett.
     */
    private Gameboard gameboard;
    /**
     * Der Spieler der Farbe weiss.
     */
    private Player player1;
    /**
     * Der Spieler der Farbe schwarz.
     */
    private Player player2;
    /**
     * Das Feld, auf dem die markierte Figur steht.
     */
    private Field sourceField;
    /**
     * Gibt an, ob es sich um ein Netzwerkspiel handelt.
     */
    private boolean isNetworkGame;
    /**
     * Bei einem Netzwerkspiel enthaelt diese Variable die Spielerfarbe
     */
    private Color playerColor;
    /**
     * Alle Felder, die die momentan selektierte Figur erreichen kann.
     * Die Felder werden zwischengespeichert, um spaeter pruefen zu koennen,
     * ob das naechste angeklickte Feld bei den moeglichen Zielfeldern dabei war
     */
    private Field[] possibleFields;

    /**
     * Erzeugt die Schachlogik.
     */
    public ChessLogic() {
        presentation = Chess.getPresentation();
        persistance = Chess.getPersistance();
        gameboard = Gameboard.getInstance();
        actionMode = false;

        player1 = new Player("Luke Skywalker", Color.WHITE);
        player1.setOnTurn(true);
        player2 = new Player("Darth Vader", Color.BLACK);
        player2.setOnTurn(false);
        this.isNetworkGame = false;
    }

    public void presentationInput(int x, int y) {
        // Ungueltige Eingaben abfangen
        if (!Field.exists(x, y)) {
            return;
        }

        // Wenn man bei einem Netzwerkspiel nicht an der Reihe ist, passiert nichts
        if (isNetworkGame) {
            if (playerColor != Player.getOnTurnColor(player1, player2)) {
                System.err.println("Sie sind nicht an der Reihe!");
                return;
            }
        }

        Field klickedField = new Field(x, y);

        Color onTurnColor, otherColor;
        onTurnColor = Player.getOnTurnColor(player1, player2);
        if (onTurnColor == Color.WHITE) {
            otherColor = Color.BLACK;
        } else {
            otherColor = Color.WHITE;
        }

        if (!actionMode) {
            if (!gameboard.isFieldOccupied(klickedField)) {
                // Feld ist leer -> nichts passiert
                presentation.logicInput();
                return;
            }

            /* Das angeklickte Feld zwischenspeichern, um beim naechsten Klick die Figur
            bewegen zu koennen, falls notwendig. */
            sourceField = klickedField;

            // Vorlaeufig
            ChessFieldModel.getInstance().setSourceField(new chess.presentation.utilities.Field(x, y));

            IGameFigure figure = gameboard.getFigure(klickedField);

            assert figure != null;

            if (figure.getColor() != onTurnColor) {
                // Auf gegnerische Figur geklickt -> nichts passiert
                System.err.println("Auf gegnerische Figur geklickt");
                presentation.logicInput();
                return;
            }

            if (gameboard.wouldBeCheckWithoutFigure(figure)) {
                possibleFields = gameboard.getSpecialFields(figure);
            } else {
                possibleFields = figure.getMoves();
            }

            // Der Presentation alle Felder mitteilen
            for (Field f : possibleFields) {
                presentation.logicInput(f.getX(), f.getY());
            }
            actionMode = true;
        } else {
            actionMode = false;
            IGameFigure temp;
            if (!isMarkedField(klickedField)) {
                // Feld wurde zuvor nicht als moegliches Ziel markiert -> nichts passiert
                presentation.logicInput();
                presentationInput(klickedField.getX(), klickedField.getY());
                return;
            }
            if ((temp = gameboard.getFigure(klickedField)) != null) {
                if (temp.getColor() == onTurnColor) {
                    // Auf eigene Figur geklickt -> Erneut alle Felder ermitteln
                    presentationInput(klickedField.getX(), klickedField.getY());
                    return;
                }
            }
            // Figur bewegen
            gameboard.moveFigure(sourceField, klickedField);
            presentation.logicMoveInput(klickedField.getX(), klickedField.getY());
            if (isNetworkGame/* && Player.getOnTurnColor(player1, player2) == playerColor*/) {
                NetController.getInstance().sendMoveOutput(sourceField.toString(), klickedField.toString());
            }

            // Anderer Spieler ist an der Reihe
            Player.switchOnTurn(player1, player2);
        }
    }

    public void persistanceInput(String[] statistics) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public synchronized String[] persistanceInputPosition() {
        ArrayList<String> temp = new ArrayList<String>();
        IGameFigure[] figures = Gameboard.getInstance().getAllFigures();
        String type = null, color, posX, posY, formattedString, hasMoved;
        for (IGameFigure figure : figures) {
            hasMoved = "false";
            if (figure instanceof Pawn) {
                type = "pawn";
                Pawn pawn = (Pawn) figure;
                if (pawn.hasMoved()) {
                    hasMoved = "true";
                }
            } else if (figure instanceof Bishop) {
                type = "bishop";
            } else if (figure instanceof Rook) {
                type = "rook";
                Rook rook = (Rook) figure;
                if (rook.hasMoved()) {
                    hasMoved = "true";
                }
            } else if (figure instanceof King) {
                type = "king";
                King king = (King) figure;
                if (king.hasMoved()) {
                    hasMoved = "true";
                }
            } else if (figure instanceof Queen) {
                type = "queen";
            } else if (figure instanceof Knight) {
                type = "knight";
            }

            if (figure.getColor() == Color.WHITE) {
                color = "white";
            } else {
                color = "black";
            }

            Field field = figure.getPosition();

            char column = (char) (field.getX() + 'a');
            posX = String.valueOf(column);
            posY = String.valueOf((field.getY() + 1));

            formattedString = type + ";" + color + ";" + posX + ";" + posY + ";" + hasMoved;
            temp.add(formattedString);
        }

        // Spieler hinzufuegen
        // player;<Spielername>;<Farbe>;<Am Zug?true:false>

        String playerString = "player;" + player1.getName() + ";" + "white;";
        if (player1.isOnTurn()) {
            playerString += "true";
        } else {
            playerString += "false";
        }
        temp.add(playerString);

        playerString = "player;" + player2.getName() + ";" + "black;";
        if (player2.isOnTurn()) {
            playerString += "true";
        } else {
            playerString += "false";
        }

        temp.add(playerString);

        String[] s = new String[temp.size()];
        return temp.toArray(s);
    }

    public void persistanceInputPosition(String name1, String name2, String[] positions) {
        player1 = new Player(name1, Color.WHITE);
        player2 = new Player(name2, Color.BLACK);

        player1.setOnTurn(true);
        player2.setOnTurn(false);

        Gameboard gb = Gameboard.getInstance();
        gb.clear();

        // Presentation Spielfeld leeren
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                presentation.logicInputRemoveFigure(i, j);
            }
        }

        IGameFigure figure = null;
        Color c = null;
        char column;
        int row;
        Field position;
        String figureType, color, posX, posY, hasMoved;
        String[] saveDataSplit;

        for (String s : positions) {

            saveDataSplit = s.split(";");
            if (saveDataSplit.length != 5) {
                // Spielstand kaputt -> evtl. false zurueckgeben?
            }

            if (saveDataSplit[0].equals("player")) {
                String name = saveDataSplit[1];
                String playerColor = saveDataSplit[2];
                String onTurn = saveDataSplit[3];

                if (playerColor.equals("white")) {
                    player1 = new Player(name, Color.WHITE);
                    if (onTurn.equals("true")) {
                        player1.setOnTurn(true);
                    } else {
                        player1.setOnTurn(false);
                    }
                } else if (playerColor.equals("black")) {
                    player2 = new Player(name, Color.BLACK);
                    if (onTurn.equals("true")) {
                        player2.setOnTurn(true);
                    } else {
                        player2.setOnTurn(false);
                    }
                } else {
                }

                continue;
            }

            figureType = saveDataSplit[0].toLowerCase();
            color = saveDataSplit[1].toLowerCase();
            posX = saveDataSplit[2].toLowerCase();
            posY = saveDataSplit[3].toLowerCase();
            hasMoved = saveDataSplit[4].toLowerCase();

            if (color.equals("white")) {
                c = Color.WHITE;
            } else if (color.equals("black")) {
                c = Color.BLACK;
            } else {
                // Spielstand kaputt
            }

            column = posX.charAt(0);
            row = Integer.parseInt(posY);
            position = new Field(column, row);

            if (figureType.equals("pawn")) {
                Pawn pawn = new Pawn(c, position);
                if (hasMoved.equals("true")) {
                    pawn.setHasMoved(true);
                }
                figure = pawn;
            } else if (figureType.equals("bishop")) {
                figure = new Bishop(c, position);
            } else if (figureType.equals("rook")) {
                Rook rook = new Rook(c, position);
                if (hasMoved.equals("true")) {
                    rook.setHasMoved(true);
                }
                figure = rook;
            } else if (figureType.equals("king")) {
                King king = new King(c, position);
                if (hasMoved.equals("true")) {
                    king.setHasMoved(true);
                }
                figure = king;
            } else if (figureType.equals("queen")) {
                figure = new Queen(c, position);
            } else if (figureType.equals("knight")) {
                figure = new Knight(c, position);
            } else {
                // Spielstand kaputt
            }
            gb.putFigure(figure);
            presentation.logicInputPutFigure(figureType, color, position.getX(), position.getY());
        }
        // Evtl. das Schach-Flag des Koenigs setzen
        figure = null;
        if ((figure = gb.isCheck(Color.WHITE)) != null) {
            gb.getKing(Color.WHITE).setCheck(true, figure);
        } else if ((figure = gb.isCheck(Color.BLACK)) != null) {
            gb.getKing(Color.BLACK).setCheck(true, figure);
        }
    }

    /**
     * Diese Methode prueft, ob das angegebene Feld ein moegliches Zielfeld ist.
     */
    private boolean isMarkedField(Field klickedField) {
        for (Field posField : possibleFields) {
            if (posField.equals(klickedField)) {
                return true;
            }
        }
        return false;
    }

    public void newGame(String nameWhite, String nameBlack, String onTurnColor, boolean networkGame, String playerColor) {

        player1 = new Player(nameWhite, Color.WHITE);
        player2 = new Player(nameBlack, Color.BLACK);

        if (onTurnColor.toLowerCase().equals("white")) {
            player1.setOnTurn(true);
            player2.setOnTurn(false);
        } else if (onTurnColor.toLowerCase().equals("black")) {
            player1.setOnTurn(false);
            player2.setOnTurn(true);
        } else {
            throw new IllegalArgumentException(playerColor + " is not a color. Must be \"white\" or \"black\"");
        }
        if (networkGame) {
            this.isNetworkGame = true;
            if (playerColor.toLowerCase().equals("white")) {
                this.playerColor = Color.WHITE;
            } else if (playerColor.toLowerCase().equals("black")) {
                this.playerColor = Color.BLACK;
            } else {
                throw new IllegalArgumentException(playerColor + " is not a color. Must be \"white\" or \"black\"");
            }
        } else {
            this.isNetworkGame = false;
            gameboard.reset();
        }
    }

    public void switchPlayers() {
        Player.switchOnTurn(player1, player2);
    }

    public String getOnTurnColor() {
        Color color = Player.getOnTurnColor(player1, player2);
        if (color == Color.WHITE) {
            return "white";
        } else {
            return "black";
        }
    }

    public String[] getPlayerNames() {
        String[] names = {player1.getName(), player2.getName()};
        return names;
    }
}
