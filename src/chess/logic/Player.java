package chess.logic;

public class Player {

    private final String name;
    private final Color color;
    private boolean onTurn;

    public Player(String name, Color color) {
        this.name = name;
        this.color = color;
        this.onTurn = false;
    }

    public boolean isOnTurn() {
        return onTurn;
    }

    public void setOnTurn(boolean onTurn) {
        this.onTurn = onTurn;
    }

    public Color getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    /**
     * Liefert die Farbe des Spielers, der an der Reihe ist.
     * @param player1 Ein Spieler.
     * @param player2 Ein Spieler
     * @return Farbe des Spielers, der an der Reihe ist.
     */
    public static Color getOnTurnColor(Player player1, Player player2) {
        if (player1.isOnTurn() && player2.isOnTurn()) {
            throw new RuntimeException("Invalid Player State: Both players are on turn");
        }
        if (player1.onTurn) {
            return player1.getColor();
        } else {
            return player2.getColor();
        }
    }

    /**
     * Tauscht den Spieler, der an der Reihe ist.
     * @param player1 Ein Spieler.
     * @param player2 Ein Spieler.
     */
    public static synchronized void switchOnTurn(Player player1, Player player2) {
        if (player1.isOnTurn() && player2.isOnTurn()) {
            throw new RuntimeException("Invalid Player State: Both players are on turn");
        }
        if (player1.isOnTurn()) {
            player1.setOnTurn(false);
            player2.setOnTurn(true);
        } else {
            player1.setOnTurn(true);
            player2.setOnTurn(false);
        }
    }
}
