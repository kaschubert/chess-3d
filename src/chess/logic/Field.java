package chess.logic;

import java.util.ArrayList;

/**
 * Diese Klasse repräsentiert die Position eines Schachfeldes.
 */
public class Field {

    private int x;
    private int y;

    /**
     * Erzeugt ein neues Feld.
     * @param x Die x-Koordinate des Feldes.
     * @param y Die y-Koordinate des Feldes.
     */
    public Field(int x, int y) {
        if ((x < 0) || (x > 7) || (y < 0) || (y > 7)) {
            throw new IllegalArgumentException("Field (" + x + "," + y + ") doesn't exist");
        }
        this.x = x;
        this.y = y;
    }

    /**
     * Erzeugt ein neues Feld unter Angabe der alphanumerischen Feldposition (z.B. b7 oder h3).
     * @param column Der Buchstabe der Zeile.
     * @param row Die Nummer der Spalte.
     */
    public Field(char column, int row) {
        column = Character.toLowerCase(column);
        switch (column) {
            case 'a':
                x = 0;
                break;
            case 'b':
                x = 1;
                break;
            case 'c':
                x = 2;
                break;
            case 'd':
                x = 3;
                break;
            case 'e':
                x = 4;
                break;
            case 'f':
                x = 5;
                break;
            case 'g':
                x = 6;
                break;
            case 'h':
                x = 7;
                break;
            default:
                throw new IllegalArgumentException("Field (" + column + "," + row + ") doesn't exist");
        }
        if ((row < 1) || (row > 8)) {
            throw new IllegalArgumentException("Field (" + column + "," + row + ") doesn't exist");
        }
        y = row - 1;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Field other = (Field) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + this.x;
        hash = 73 * hash + this.y;
        return hash;
    }

    @Override
    public String toString() {
        // return "[" + x + ";" + y + "](" + (char) (x + 'a') + (y + 1) + ")";
        return (char) (x + 'a') + "" + (y + 1);
    }

    /**
     * Prueft, ob die Parameter ein legales Feld beschreiben.
     * @param x Die x-Koordinate
     * @param y Die y-Koordinate
     * @return Legales Feld ? true : false
     */
    public static boolean exists(int x, int y) {
        if ((x < 0) || (x > 7) || (y < 0) || (y > 7)) {
            return false;
        }
        return true;
    }

    /**
     * Liefert alle Felder in horizontaler, vertikaler bzw. diagonaler Richtung zwischen zwei Feldern.
     * @pre Die Felder sind nicht identisch.
     * @param field1 Ein Feld.
     * @param field2 Ein Feld.
     * @return Alle Felder, die zwischen den beiden angegebenen Feldern liegen.
     */
    public static Field[] getFieldsBetween(Field field1, Field field2) {
        ArrayList<Field> fields = new ArrayList<Field>();
        int x1, y1, x2, y2;

        x1 = field1.getX();
        y1 = field1.getY();
        x2 = field2.getX();
        y2 = field2.getY();

        int deltaX = x2 - x1;
        int deltaY = y2 - y1;

        Field temp;
        if (deltaX == 0) {
            int deltaYStep = deltaY / Math.abs(deltaY);
            for (int i = 1; i < Math.abs(deltaY); i++) {
                temp = new Field(x1, y1 + deltaYStep * i);
                fields.add(temp);
            }
        } else if (deltaY == 0) {
            int deltaXStep = deltaX / Math.abs(deltaX);
            for (int i = 1; i < Math.abs(deltaX); i++) {
                temp = new Field(x1 + deltaXStep * i, y1);
                fields.add(temp);
            }
        } else if (Math.abs(deltaX) == Math.abs(deltaY)) {
            int deltaXStep = deltaX / Math.abs(deltaX);
            int deltaYStep = deltaY / Math.abs(deltaY);
            for (int i = 1; i < Math.abs(deltaX); i++) {
                temp = new Field(x1 + deltaXStep * i, y1 + deltaYStep * i);
                fields.add(temp);
            }
        }else {
            // throw new RuntimeException("Illegal use of getFieldsBetween-Method, used with Fields " + field1 + "," + field2);
            Field[] f = {};
            return f;
        }
        Field[] f = new Field[fields.size()];
        return fields.toArray(f);
    }
}
