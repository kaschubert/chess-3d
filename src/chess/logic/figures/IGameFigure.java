package chess.logic.figures;

import chess.logic.Color;
import chess.logic.Field;

public interface IGameFigure {

    /**
     * Liefert die Farbe des Spielers, dem die Figur gehoert.
     * @return Die Farbe des Besitzers.
     */
    public Color getColor();

    /**
     * Liefert die Postion der Figur auf dem Schachfeld.
     * @return Die aktulle Position der Figur.
     */
    public Field getPosition();

    /**
     * Aendert den aktuellen Standort der Figur.
     * @param position Die neue Position.
     */
    public void setPosition(Field position);

    /**
     * Diese Methode ermittelt alle Felder, auf die sich die Figur bewegen kann.
     * 
     * Andere Figuren, die eventuell den Weg versperren, werden dabei beruecksichtigt.
     * @return Alle moeglichen Felder.
     */
    public Field[] getMoves();

    /**
     * Diese Methode liefer alle Felder, die die Figur bedroht.
     * @return Alle bedrohten Felder.
     */
    public Field[] getAttackFields();
}
