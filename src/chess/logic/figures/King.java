package chess.logic.figures;

import chess.logic.Color;
import chess.logic.Field;
import chess.logic.Gameboard;
import chess.logic.rules.ARule;
import chess.logic.rules.CastlingRule;
import chess.logic.rules.KingRule;
import java.util.ArrayList;

/**
 * Diese Klasse repraesentiert den Koenig.
 */
public class King implements IGameFigure {

    private final Color color;
    private boolean hasMoved;
    private boolean check;
    private boolean castling;
    private Field position;
    private ARule[] rules;
    private IGameFigure attackingFigure;

    /**
     * Erzeugt einen neuen Koenig.
     *
     * @param color Die Farbe des Spieles, dem der Koenig gehoert.
     * @param position Die Startposition der Figur.
     */
    public King(Color color, Field position) {
        this.hasMoved = false;
        this.check = false;
        this.castling = false;
        this.color = color;
        this.position = position;

        this.rules = new ARule[2];
        this.rules[0] = new KingRule();
        this.rules[1] = new CastlingRule();
    }

    /**
     * Gibt true zurueck, wenn die Figur mindestens einmal bewegt wurde.
     * @return Figur bereits bewegt ? true : false
     */
    public boolean hasMoved() {
        return hasMoved;
    }

    public void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    /**
     * Gibt true zurueck, wenn der Koenig im Schach steht.
     * @return Koenig im Schach ? true : false
     */
    public boolean isCheck() {
        return check;
    }

    /**
     * Setzt das "Steht-im-Schach"-Flag
     * @param check
     * @param attackingFigure Die Figur, die dem Koenig Schach bietet.
     */
    public void setCheck(boolean check, IGameFigure attackingFigure) {
        this.check = check;
        this.attackingFigure = attackingFigure;
    }

    /**
     * Liefert die Figur, die dem Koenig schach bietet, bzw. <i>null</i>, wenn der Koenig nicht im Schach steht
     * @return Eine Figur.
     */
    public IGameFigure getAttackingFigure(){
        return attackingFigure;
    }

    /**
     * Gibt true zurueck, wenn der Koenig die Rochade bereits ausgeführt hat.
     * @return Rochade bereits ausgefuehrt ? true : false
     */
    public boolean didCastling() {
        return castling;
    }

    /**
     * Setzt das Rochade-Flag auf 'true'.
     */
    public void setCastling() {
        this.castling = true;
    }

    public Color getColor() {
        return color;
    }

    public Field getPosition() {
        return position;
    }

    public synchronized void setPosition(Field position) {
        if (!hasMoved) {
            hasMoved = true;
        }
        this.position = position;
    }

    public Field[] getAttackFields(){
        return rules[0].getAttacks(this);
    }

    public Field[] getMoves() {
        ArrayList<Field> temp = new ArrayList<Field>();
        Gameboard gb = Gameboard.getInstance();

        Field[] moves = rules[0].getMoves(this);
        Field[] castlingMoves = rules[1].getMoves(this);

        Color enemyColor;
        if (color == Color.WHITE) {
            enemyColor = Color.BLACK;
        } else {
            enemyColor = Color.WHITE;
        }

        for (Field f : moves) {
            if (!gb.isFieldUnderAttack(f, enemyColor, true)) {
                // Das Feld darf nur hinzugefuegt werden, wenn
                // der Koenig dort nicht im Schach stehen wuerde
                temp.add(f);
            }
        }

        if (castlingMoves != null) {
            for (Field f : castlingMoves) {
                temp.add(f);
            }
        }

        moves = new Field[temp.size()];
        return temp.toArray(moves);
    }
}
