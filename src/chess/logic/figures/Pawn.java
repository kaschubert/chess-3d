package chess.logic.figures;

import chess.logic.Color;
import chess.logic.Field;
import chess.logic.rules.ARule;
import chess.logic.rules.PawnRule;
import chess.logic.rules.Vector;
import java.util.ArrayList;

/**
 * Diese Klasse repraesentiert den Bauern.
 */
public class Pawn implements IGameFigure {

    private final Color color;
    private boolean hasMoved;
    private Field position;
    private Field enPassantField;
    private ARule[] rules;

    /**
     * Erzeugt einen neuen Bauern.
     *
     * @param color Die Farbe des Spieles, dem die Figur gehoert.
     * @param position Die Startposition der Figur.
     */
    public Pawn(Color color, Field position) {
        this.color = color;
        this.position = position;
        this.hasMoved = false;
        this.enPassantField = null;

        rules = new ARule[2];
        rules[0] = new PawnRule();
    }

    /**
     * Gibt true zurueck, wenn die Figur mindestens einmal bewegt wurde.
     * @return Figur bereits bewegt ? true : false
     */
    public boolean hasMoved() {
        return this.hasMoved;
    }

    public void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    public Color getColor() {
        return color;
    }

    public Field getPosition() {
        return position;
    }

    public Field getEnPassantField() {
        return enPassantField;
    }

    public void setEnPassantField(Field enPassantField) {
        this.enPassantField = enPassantField;
    }

    public synchronized void setPosition(Field position) {
        if (!hasMoved) {
            hasMoved = true;
        }
        this.position = position;
    }

    /**
     * Liefert alle Felder, die der Bauer bedroht.
     * @return Alle Felder, die der Bauer bedroht.
     */
    public Field[] getAttackFields() {
        ArrayList<Field> possibleFields = new ArrayList<Field>();

        Vector attackLeft, attackRight;
        if (this.color == Color.WHITE) {
            attackLeft = new Vector(-1, 1, false);
            attackRight = new Vector(1, 1, false);
        } else {
            attackLeft = new Vector(-1, -1, false);
            attackRight = new Vector(1, -1, false);
        }

        int x, y;
        
        x = position.getX();
        y = position.getY();
        x += attackLeft.getX();
        y += attackLeft.getY();
        if (Field.exists(x, y)) {
            possibleFields.add(new Field(x, y));
        }
        x = position.getX();
        y = position.getY();
        x += attackRight.getX();
        y += attackRight.getY();
        if (Field.exists(x, y)) {
            possibleFields.add(new Field(x, y));
        }

        Field[] f = new Field[possibleFields.size()];
        return possibleFields.toArray(f);
    }

    public Field[] getMoves() {
        return rules[0].getMoves(this);
    }
}
