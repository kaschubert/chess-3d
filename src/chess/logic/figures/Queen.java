package chess.logic.figures;

import chess.logic.Color;
import chess.logic.Field;
import chess.logic.rules.ARule;
import chess.logic.rules.QueenRule;

/**
 * Diese Klasse repraesentiert die Dame.
 */
public class Queen implements IGameFigure {

    private final Color color;
    private Field position;
    private ARule rule;

     /**
     * Erzeugt eine neue Dame.
     *
     * @param color Die Farbe des Spieles, dem die Figur gehoert.
     * @param position Die Startposition der Figur.
     */
    public Queen(Color color, Field position) {
        this.color = color;
        this.position = position;
        this.rule = new QueenRule();
    }

    public Color getColor() {
        return color;
    }

    public Field getPosition() {
        return position;
    }

    public void setPosition(Field position) {
        this.position = position;
    }

    public Field[] getMoves() {
        return rule.getMoves(this);
    }

    public Field[] getAttackFields() {
        return rule.getAttacks(this);
    }
}
