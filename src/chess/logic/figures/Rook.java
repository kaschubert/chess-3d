package chess.logic.figures;

import chess.logic.Color;
import chess.logic.Field;
import chess.logic.rules.ARule;
import chess.logic.rules.RookRule;

/**
 * Diese Klasse repräsentiert den Turm.
 */
public class Rook implements IGameFigure {

    private final Color color;
    private boolean hasMoved;
    private ARule rule;
    private Field position;

    /**
     * Erzeugt einen neuen Turm.
     *
     * @param color Die Farbe des Spieles, dem die Figur gehört.
     * @param position Die Startposition der Figur.
     */
    public Rook(Color color, Field position) {
        this.color = color;
        this.hasMoved = false;
        this.position = position;
        this.rule = new RookRule();
    }

    /**
     * Gibt true zurück, wenn die Figur mindestens einmal bewegt wurde.
     * @return Figur bereits bewegt ? true : false
     */
    public boolean hasMoved() {
        return this.hasMoved;
    }

    public void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    public Color getColor() {
        return color;
    }

    public Field getPosition() {
        return position;
    }

    public synchronized void setPosition(Field position) {
        if (!hasMoved) {
            hasMoved = true;
        }
        this.position = position;
    }

    public Field[] getMoves() {
        return rule.getMoves(this);
    }

    public Field[] getAttackFields() {
        return rule.getAttacks(this);
    }
}
