package chess.logic.figures;

import chess.logic.Color;
import chess.logic.Field;
import chess.logic.rules.ARule;
import chess.logic.rules.BishopRule;

/**
 * Diese Klasse repraesentiert den Laeufer.
 */
public class Bishop implements IGameFigure {

    private final Color color;
    private ARule rule;
    private Field position;

     /**
     * Erzeugt einen neuen Laeufer.
     * 
     * @param color Die Farbe des Spieles, dem die Figur gehoert.
     * @param position Die Startposition der Figur.
     */
    public Bishop(Color color, Field position) {
        this.color = color;
        this.position = position;
        this.rule = new BishopRule();
    }

    public Color getColor() {
        return color;
    }

    public Field getPosition() {
        return position;
    }

    public void setPosition(Field position) {
        this.position = position;
    }

    public Field[] getMoves() {
        return rule.getMoves(this);
    }

    public Field[] getAttackFields() {
        return rule.getAttacks(this);
    }
}
