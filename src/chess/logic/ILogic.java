package chess.logic;

public interface ILogic {
    /**
     * Wird aufgerufen, wenn ein Feld angeklickt wird.
     * @param x Die x-Koordinate des Feldes
     * @param y Die y-Koordinate des Feldes
     */
    public void presentationInput(int x, int y);

    /**
     * Dient zur Übermittlung von Daten aus der Statistik.
     * @param statistics
     */
    public void persistanceInput(String[] statistics);

    /**
     * Wird von der Persistenz aufgerufen und liefert alle Spielfiguren
     * auf dem Spielfeld samt Farbe und Position.
     * Die Strings sind folgendermaßen formatiert: &lt;Figur&gt;;&lt;Farbe&gt;;&lt;PositionX&gt;;&lt;PositionY&gt;, z.B. pawn;black;a;2.
     *
     * @return Ein Array mit allen Figuren
     */
    public String[] persistanceInputPosition();


    /**
     * Dient zur Uebermittlung der Daten aus einem Savegame.
     *
     * @param name1 Spielername, bekommt die Farbe weiss.
     * @param name2 Spielername, bekommt die Farbe schwarz.
     * @param positions Ein formatiertes Array mit allen Figuren.
     */
    public void persistanceInputPosition(String name1, String name2, String[] positions);

    /**
     * Uebermittelt Daten fuer ein neues Spiel.
     * -> Signatur kann sich noch aendern.
     *
     * @param nameWhite Names des Spielers der Farbe weiß.
     * @param nameBlack Names des Spielers der Farbe schwarz.
     * @param onTurnColor Farbe des Spielers, der an der Reihe ist (d.h. der den ersten Zug macht).
     * @param networkGame Gibt an, ob es sich um ein Netzwerkspiel handelt.
     * @param playerColor Gibt bei einem Netzwerkspiel an, welche Farbe der Spieler hat.
     */
    public void newGame(String nameWhite, String nameBlack, String onTurnColor, boolean networkGame, String playerColor);

    /**
     *
     */
    public void switchPlayers();

    /**
     * Liefert die Farbe des Spielers, der an der Reihe ist.
     */
    public String getOnTurnColor();

    /*
     * Liefert die Namen der beiden Spieler.
     * Spieler weiß steht an erster Stelle.
     */
    public String[] getPlayerNames();

}
