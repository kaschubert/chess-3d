package chess.presentation;

import chess.presentation.scenes.*;
import chess.presentation.utilities.*;
import com.sun.opengl.util.FPSAnimator;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.*;

/**
 * Engine.
 * Hier wird das Fenster und die darin enthaltende Zeichenfläche erzeugt und
 * dadurch, dass der GLEventListener implementiert wird auch direkt genutzt 
 * @author k.a.schubert
 */
public class Engine implements GLEventListener {
    private int frameCount;
    private FPSAnimator fpsAnimator;
    private SceneManager sceneManager;
    private int fsaamode;

    /**
     * Konstruktor.
     * Erzeugung des Fensters (frame), FSAA (FullScreenAntiAliasing), fpsAnimator übernimmt
     * die aktualisierung des Zielfenster mit der vorgegebenen Framezahl
     * SceneManager wird erzeugt
     */
    public Engine(){
        this.fsaamode = 16;
        this.frameCount = 60;
        this.sceneManager = SceneManager.getInstance();

        Frame frame = new Frame("Chess");

        GLCapabilities caps = new GLCapabilities();
        
        caps.setDoubleBuffered(true);
        caps.setHardwareAccelerated(true);

        // FSAA
        caps.setSampleBuffers(true);
        // FSAA-Mode
        caps.setNumSamples(fsaamode);

        GLCanvas canvas = new GLCanvas(caps);

        canvas.addGLEventListener(this);

        canvas.setAutoSwapBufferMode(true);

        frame.add(canvas);

        frame.setResizable(false);
        frame.setLocation(100, 0);

        fpsAnimator = new FPSAnimator(canvas, this.frameCount);

        frame.setSize(Resolution.getInstance(0, 0).getX(), Resolution.getInstance(0, 0).getY());

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        frame.setVisible(true);

        this.fpsAnimator.start();
    }
    
    /**
     * init.
     * OpenGL-Callback
     * OpenGLContext wird erzeugt und konfiguriert OpenGL
     * Szenen werden erzeugt und im SceneManager registriert
     * @param drawable
     */
    public void init(GLAutoDrawable drawable) {
        //GL Context erzeugen
        OpenGLContext oglContext = OpenGLContext.getInstance(drawable);
        //GL Context konfigurieren
        oglContext.configureOpenGL();
        //Szenen erstellen
        MainMenuScene mainMenuScene = new MainMenuScene();
        GameScene gameScene = new GameScene();
        //Szenen beim SceneManager registrieren
        //currentScene = 0
        sceneManager.addScene(mainMenuScene);
        //currentScene = 1
        sceneManager.addScene(gameScene);
    }
    
    /**
     * display.
     * OpenGL-Callback
     * SceneManager ermittelt die momentane Szene und diese
     * zeigt sich selbst an.
     * @param drawable
     */
    public void display(GLAutoDrawable drawable) {
        sceneManager.getCurrentScene().displayScene();
    }

    /**
     * reshape.
     * OpenGL-Callback
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void reshape (
        GLAutoDrawable drawable,
        int x,
        int y,
        int width,
        int height
    ) {
    }
    
    /**
     * displayChanged.
     * OpenGL-Callback
     * @param drawable
     * @param modeChanged
     * @param deviceChanged
     */
    public void displayChanged (
        GLAutoDrawable drawable,
        boolean modeChanged,
        boolean deviceChanged
    ) {
        
    }    
}