package chess.presentation;

import chess.presentation.scenes.*;
import chess.presentation.utilities.*;
import com.sun.opengl.util.FPSAnimator;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.*;

/**
 * Generierung der Grafik und Verarbeitung der Benutzereingaben.
 * Hier wird das Fenster und die darin enthaltende Zeichenfläche erzeugt und
 * dadurch, dass der GLEventListener implementiert wird auch direkt genutzt.
 */
public class Engine implements GLEventListener {
    /**
     * Anzahl Bilder pro Sekunde, die gerendert werden.
     */
    private int frameCount;

    /**
     * Aktualisierung des Fensters, entsprechend des frameCounts.
     */
    private FPSAnimator fpsAnimator;

    /**
     * Verwaltet die Szenen.
     */
    private SceneManager sceneManager;

    /**
     * Modus des Anti-Aliasing (Kantenglättung)
     */
    private int fsaamode;

    /**
     * Fenster-Auflösung
     */
    private Resolution resolution;

    /**
     * Die Zeichenflaeche
     */
    private static GLCanvas canvas;

    public static GLCanvas getCanvas(){
        return canvas;
    }
    /**
     * Erzeugung des Fensters (frame), FSAA (FullScreenAntiAliasing), fpsAnimator übernimmt
     * die aktualisierung des Zielfenster mit der vorgegebenen Framezahl
     * SceneManager wird erzeugt
     */
    public Engine(){
        fsaamode = 16;
        frameCount = 60;
        sceneManager = SceneManager.getInstance();
        resolution = Resolution.getInstance();

        //GLCapabilities
        GLCapabilities caps = new GLCapabilities();
        caps.setDoubleBuffered(true);
        caps.setHardwareAccelerated(true);
        // FSAA
        caps.setSampleBuffers(true);
        // FSAA-Mode
        caps.setNumSamples(fsaamode);

        //GLCanvas
        canvas = new GLCanvas(caps);
        canvas.addGLEventListener(this);
        canvas.setAutoSwapBufferMode(true);
        //Focus an die Zeichenflaeche geben moeglich
        //kein Klicken mehr noetig!
        canvas.setFocusable(true);
        canvas.setPreferredSize(
                new Dimension(
                    resolution.getX(),
                    resolution.getY()
                )
        );

        //Frame
        Frame frame = new Frame("Chess");
        //nötig um Auflösung zu ändern
        resolution.setFrame(frame);
        
        frame.add(canvas);

        //frame.setResizable(false);
        frame.setLocation(200, 50);
        frame.setSize(resolution.getX() + 9, resolution.getY() + 28);
        
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        frame.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        frame.setVisible(true);

        //FPSAnimator
        fpsAnimator = new FPSAnimator(canvas, this.frameCount);
        this.fpsAnimator.start();
    }
    
    /**
     * Initialisierung der Grafik.
     * OpenGL-Callback
     * OpenGLContext wird erzeugt und konfiguriert OpenGL
     * Szenen werden erzeugt und im SceneManager registriert
     * @param drawable
     */
    public void init(GLAutoDrawable drawable) {
        //GL Context erzeugen
        OpenGLContext oglContext = OpenGLContext.getInstance(drawable);
        //GL Context konfigurieren
        oglContext.configureOpenGL();

        //Szenen erstellen
        LoadMenu loadMenu = new LoadMenu();
        MainMenu mainMenu = new MainMenu();
        OptionsMenu optionsMenu = new OptionsMenu();
        StatisticsMenu statisticsMenu = new StatisticsMenu();
        GameTypeMenu gameTypeMenu = new GameTypeMenu();
        NetworkMenu networkMenu = new NetworkMenu();
        HotSeatMenu hotSeatMenu = new HotSeatMenu();
        GameScene gameScene = new GameScene();
        HostMenu hostMenu = new HostMenu();
        JoinMenu joinMenu = new JoinMenu();
        WaitingMenu waitingScene = new WaitingMenu();
        LoadgameMenu loadgameMenuHotSeat = new LoadgameMenu();
        LoadgameMenuNet loadgameMenuNet = new LoadgameMenuNet();

        //Szenen beim SceneManager registrieren
        sceneManager.addScene(loadMenu);
        sceneManager.addScene(mainMenu);
        sceneManager.addScene(optionsMenu);
        sceneManager.addScene(statisticsMenu);
        sceneManager.addScene(gameTypeMenu);
        sceneManager.addScene(networkMenu);
        sceneManager.addScene(hotSeatMenu);
        sceneManager.addScene(gameScene);
        sceneManager.addScene(hostMenu);
        sceneManager.addScene(joinMenu);
        sceneManager.addScene(waitingScene);
        sceneManager.addScene(loadgameMenuHotSeat);
        sceneManager.addScene(loadgameMenuNet);
    }
    
    /**
     * Anzeige der momentanen Szene.
     * OpenGL-Callback
     * SceneManager ermittelt die momentane Szene und diese
     * zeigt sich selbst an.
     * @param drawable
     */
    public void display(GLAutoDrawable drawable) {
        sceneManager.getCurrentScene().displayScene();
    }

    /**
     * Anzeige nach Fensteroperationen aktualisieren.
     * OpenGL-Callback
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void reshape (
        GLAutoDrawable drawable,
        int x,
        int y,
        int width,
        int height
    ) {
        resolution.setX(width);
        resolution.setY(height);
        sceneManager.reshapeAllScenes();
    }
    
    /**
     * Anzeige nach Veraenderung des Anzeigemodus aktualisieren
     * displayChanged.
     * OpenGL-Callback
     * @param drawable
     * @param modeChanged
     * @param deviceChanged
     */
    public void displayChanged (
        GLAutoDrawable drawable,
        boolean modeChanged,
        boolean deviceChanged
    ) {
        
    }    
}