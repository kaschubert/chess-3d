package chess.presentation;

/**
 * Speichert den Status des Schachspiels. 
 * Singleton-Klasse
 */
public class Status {

    /**
     * Singleton-Instanz. 
     */
    private static Status instance = null;

    /**
     * Schachmatt
     */
    private boolean checkmate = false;

    /**
     * Schach
     */
    private boolean check = false;

    /**
     * Remis
     */
    private boolean remis = false;

    /**
     * Aufgegeben
     */
    private boolean resign = false;

    /**
     * Gibt an, ob es sich um ein Netzwerkspiel handelt.
     */
     private boolean networkGame;
    /**
     * Liefert die Instanz des Singletons.
     * @return Instanz des Singletons
     */
    public static Status getInstance(){
        if(instance == null)
            instance = new Status();
        return instance;
    }

    /**
     * Getter Schach
     * @return Schach
     */
    public boolean getCheck() {
        return check;
    }

    /**
     * Getter Netzwerkspiel
     * @return Netzwerkspiel
     */
    public boolean isNetworkGame() {
        return networkGame;
    }

    /**
     * Setter Netzwerkspiel.
     * @param isNetworkGame
     */
    public void setNetworkGame(boolean isNetworkGame) {
        this.networkGame = isNetworkGame;
    }



    /**
     * Setter Schach
     * @param check Schach
     */
    public void setCheck(boolean check) {
        this.check = check;
    }

    /**
     * Getter Schachmatt
     * @return Schachmatt
     */
    public boolean getCheckmate() {
        return checkmate;
    }

    /**
     * Setter Schachmatt
     */
    public void setCheckmate() {
        this.checkmate = true;
    }

    /**
     * Getter Remis
     * @return Remis
     */
    public boolean getRemis() {
        return remis;
    }

    /**
     * Setter Remis
     */
    public void setRemis() {
        this.remis = true;
    }

    /**
     * Getter Resign
     * @return Resign
     */
    public boolean getResign() {
        return resign;
    }

    /**
     * Setter Resign
     */
    public void setResign() {
        this.resign = true;
    }

    /**
     * Setzt Schach, Schachmatt und Remis auf false. 
     */
    public void reset(){
        this.check = false;
        this.checkmate = false;
        this.remis = false;
        this.resign = false;
    }
}