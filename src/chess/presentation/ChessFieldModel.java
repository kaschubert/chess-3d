package chess.presentation;

import chess.presentation.drawables.figures.*;
import chess.presentation.utilities.*;

/**
 * Diese Klasse repraesentiert die Positionen der Figuren auf dem Schachfeld.
 * Singleton-Klasse
 */
public class ChessFieldModel {

    /**
     * Singleton-Instanz.
     */
    private static ChessFieldModel instance = null;

    /**
     * alle markierten Felder.
     */
    private boolean[][] markedFields;

    /**
     * alle selektierten Felder. 
     */
    private int[] selectedField;

    /**
     * alle Figuren.
     */
    private AFigure[][] figures;

    /**
     * Verwaltungsobjekt für Materialien.
     */
    private Materials materials;

    /**
     * Feld, auf dem eine Figur vor dem Zug stand. 
     */
    private Field source;

    /**
     * Erzeugt ein neues ChessFieldModel. 
     */
    private ChessFieldModel() {
        this.markedFields = new boolean[8][8];
        this.figures = new AFigure[8][8];
        this.materials = new Materials();

        selectedField = new int[2];

        reset();
    }

    /**
     * Liefert die Instanz des Singletons. 
     * @return Instanz des Singletons
     */
    public static ChessFieldModel getInstance() {
        if (instance == null) {
            instance = new ChessFieldModel();
        }
        return instance;
    }

    /**
     * Feld markieren.
     * @param x Position
     * @param y Position
     */
    public void markField(int x, int y) {
        markedFields[x][y] = true;
    }

    /**
     * abfragen, ob ein Feld markiert ist. 
     * @param x Position
     * @param y Position
     * @return Feld markiert oder nicht
     */
    public boolean isMarkedField(int x, int y) {
        return markedFields[x][y];
    }

    /**
     * Feld selektieren.
     * @param x Position
     * @param y Position
     */
    public void selectField(int x, int y) {
        selectedField[0] = x;
        selectedField[1] = y;
    }

    /**
     * abfragen, ob ein Feld selektiert ist.
     * @param x Position
     * @param y Position
     * @return Feld selektiert oder nicht
     */
    public boolean isSelectedField(int x, int y) {
        return selectedField[0] == x && selectedField[1] == y;
    }

    /**
     * Markierung aller Felder aufheben. 
     */
    public void clearMarkedFields() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                markedFields[i][j] = false;
            }
        }
    }

    /**
     * abfragen, ob Feld leer ist. 
     * @param x Position
     * @param y Position
     * @return Feld leer oder nicht
     */
    public boolean isFieldEmpty(int x, int y) {
        if (figures[x][y] == null) {
            return true;
        }
        return false;
    }

    /**
     * Figur vom Quellfeld auf das Zielfeld bewegen. 
     * @param source Quellfeld
     * @param destination Zielfeld
     */
    public void moveFigure(Field destination) {
        int x = destination.getX();
        int y = destination.getY();

        figures[x][y] = figures[source.getX()][source.getY()];

        figures[x][y].setPosition(new float[]{(1.0f + 2.0f * y), 0.0f, (1.0f + 2.0f * x)});

        figures[source.getX()][source.getY()] = null;
    }

    /**
     * Figur neu auf das Feld setzen. 
     * @param figureType Art der Figur
     * @param color Farbe der Figur
     * @param x Position
     * @param y Position
     */
    public void putFigure(String figureType, String color, int x, int y) {
        AFigure figure;
        Material material;

        material = materials.whitePlastic();

        figure = new Figure(figureType, material, color);

        figure.setPosition(new float[]{(1.0f + 2.0f * y), 0.0f, (1.0f + 2.0f * x)});
        this.figures[x][y] = figure;
    }

    /**
     * Figur entfernen. 
     * @param x Position
     * @param y Position
     */
    public void removeFigure(int x, int y){
        this.figures[x][y] = null;
    }

    /**
     * Alle Figuren auf ihre Startpositionen setzen. 
     */
    public void reset() {

        Status.getInstance().reset();

        for (int i = 0; i < this.figures.length; i++) {
            for (int j = 0; j < this.figures[i].length; j++) {
                figures[i][j] = null;
            }
        }

        //verschiedene Figuren in der hinteren Reihe weiss
        figures[0][0] = new Figure("rook", this.materials.whitePlastic(), "white");
        figures[1][0] = new Figure("knight", this.materials.whitePlastic(), "white");
        figures[2][0] = new Figure("bishop", this.materials.whitePlastic(), "white");
        figures[3][0] = new Figure("queen", this.materials.whitePlastic(), "white");
        figures[4][0] = new Figure("king", this.materials.whitePlastic(), "white");
        figures[5][0] = new Figure("bishop", this.materials.whitePlastic(), "white");
        figures[6][0] = new Figure("knight", this.materials.whitePlastic(), "white");
        figures[7][0] = new Figure("rook", this.materials.whitePlastic(), "white");

        //verschiedene Figuren in der hinteren Reihe schwarz
        figures[0][7] = new Figure("rook", this.materials.whitePlastic(), "black");
        figures[1][7] = new Figure("knight", this.materials.whitePlastic(), "black");
        figures[2][7] = new Figure("bishop", this.materials.whitePlastic(), "black");
        figures[3][7] = new Figure("queen", this.materials.whitePlastic(), "black");
        figures[4][7] = new Figure("king", this.materials.whitePlastic(), "black");
        figures[5][7] = new Figure("bishop", this.materials.whitePlastic(), "black");
        figures[6][7] = new Figure("knight", this.materials.whitePlastic(), "black");
        figures[7][7] = new Figure("rook", this.materials.whitePlastic(), "black");

        for (int i = 0; i < 8; i++) {
            //weisse Bauern
            figures[i][1] = new Figure("pawn", this.materials.whitePlastic(), "white");
            //schwarze Bauern
            figures[i][6] = new Figure("pawn", this.materials.whitePlastic(), "black");
        }

        //Figuren an Position setzen
        for (int i = 0; i < this.figures.length; i++) {
            for (int j = 0; j < this.figures[i].length; j++) {

                if (this.figures[i][j] != null) {
                    this.figures[i][j].setPosition(new float[]{(1.0f + 2.0f * j), 0.0f, (1.0f + 2.0f * i)});
                }
            }
        }
    }

    /**
     * Getter Figuren.
     * @return Figuren
     */
    public AFigure[][] getFigures() {
        return this.figures;
    }

    /**
     * Setter Quellfeld.
     * @param field Quellfeld
     */
    public void setSourceField(Field field) {
        this.source = field;
    }
}