package chess.presentation.utilities;

/**
 * Field.
 * Kapselung einer 2D-Position
 */
public class Field {
    private int x;
    private int y;

    /**
     * Konstruktor.
     * @param x int
     * @param y int
     */
    public Field(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * Getter fuer x
     * @return int
     */
    public int getX(){
        return this.x;
    }

    /**
     * Getter fuer y
     * @return int
     */
    public int getY(){
        return this.y;
    }
}