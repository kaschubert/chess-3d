package chess.presentation.utilities;

/**
 * Verwaltung aller zur Verfuegung stehenden Materialien
 */
public class Materials {

    //Alle zur Verfuegung stehenden Materialien

    private Material bronze = new Material (
        new float[] {0.25f, 0.14f, 0.06f, 1.0f},
        new float[] {0.4f, 0.23f, 0.1f, 1.0f},
        new float[] {0.77f, 0.45f, 0.2f, 1.0f},
        76.8f
    );

    private Material copper = new Material (
        new float[] {0.19f, 0.07f, 0.02f, 1.0f},
        new float[] {0.7f, 0.27f, 0.08f, 1.0f},
        new float[] {0.25f, 0.13f, 0.08f, 1.0f},
        12.8f
    );

    private Material gold = new Material (
        new float[] {0.24f, 0.19f, 0.07f, 1.0f},
        new float[] {0.75f, 0.6f, 0.22f, 1.0f},
        new float[] {0.75f, 0.6f, 0.22f, 1.0f},
        51.2f
    );

    private Material silver = new Material (
        new float[] {0.23f, 0.23f, 0.23f, 1.0f},
        new float[] {0.27f, 0.27f, 0.27f, 1.0f},
        new float[] {0.77f, 0.77f, 0.77f, 1.0f},
        89.6f
    );

    private Material obsidian = new Material (
        new float[] {0.05f, 0.05f, 0.06f, 0.8f},
        new float[] {0.18f, 0.17f, 0.22f, 0.8f},
        new float[] {0.33f, 0.32f, 0.34f, 0.8f},
        38.4f
    );

    private Material pearl = new Material (
        new float[] {0.25f, 0.2f, 0.2f, 0.9f},
        new float[] {1.0f, 0.82f, 0.82f, 0.9f},
        new float[] {0.29f, 0.29f, 0.29f, 0.9f},
        11.3f
    );

    private Material redPlastic = new Material (
        new float[] {0.2f, 0.0f, 0.0f, 1.0f},
        new float[] {0.7f, 0.0f, 0.0f, 1.0f},
        new float[] {0.5f, 0.5f, 0.5f, 1.0f},
        32f
    );

    private Material yellowPlastic = new Material (
        new float[] {0.2f, 0.0f, 0.0f, 1.0f},
        new float[] {1.0f, 1.0f, 0.5f, 1.0f},
        new float[] {0.5f, 0.5f, 0.5f, 1.0f},
        32f
    );

    private Material greenPlastic = new Material (
        new float[] {0.2f, 0.0f, 0.0f, 1.0f},
        new float[] {0.25f, 1.0f, 0.0f, 1.0f},
        new float[] {0.5f, 0.5f, 0.5f, 1.0f},
        32f
    );

    private Material bluePlastic = new Material (
        new float[] {0.1f, 0.1f, 0.1f, 1.0f},
        new float[] {0.0f, 0.0f, 0.7f, 1.0f},
        new float[] {0.5f, 0.5f, 0.5f, 1.0f},
        32f
    );

    private Material blackPlastic = new Material (
        new float[] {0.0f, 0.0f, 0.0f, 1.0f},
        new float[] {0.01f, 0.01f, 0.01f, 1.0f},
        new float[] {0.5f, 0.5f, 0.5f, 1.0f},
        32f
    );
    
    private Material whitePlastic = new Material (
        new float[] {0.2f, 0.2f, 0.2f, 1.0f},
        new float[] {0.7f, 0.7f, 0.7f, 1.0f},
        new float[] {1.0f, 1.0f, 1.0f, 1.0f},
        32f
    );

    /**
     * Getter fuer Bronze
     * @return
     */
    public Material bronze() {
        return bronze;
    }

    /**
     * Getter fuer Kupfer
     * @return
     */
    public Material copper() {
        return copper;
    }

    /**
     * Getter fuer Gold
     * @return
     */
    public Material gold() {
        return gold;
    }

    /**
     * Getter fuer Silber
     * @return
     */
    public Material silver() {
        return silver;
    }

    /**
     * Getter fuer Obsidian
     * @return
     */
    public Material obsidian() {
        return obsidian;
    }

    /**
     * Getter fuer Perle
     * @return
     */
    public Material pearl() {
        return pearl;
    }

    /**
     * Getter fuer rotes Plastik
     * @return
     */
    public Material redPlastic() {
        return redPlastic;
    }

    /**
     * Getter fuer gelbes Plastik
     * @return
     */
    public Material yellowPlastic() {
        return yellowPlastic;
    }

    /**
     * Getter fuer gruenes Plastik
     * @return
     */
    public Material greenPlastic() {
        return greenPlastic;
    }

    /**
     * Getter fuer blaues Plastik
     * @return
     */
    public Material bluePlastic() {
        return bluePlastic;
    }

    /**
     * Getter fuer schwarzes Plastik
     * @return
     */
    public Material blackPlastic() {
        return blackPlastic;
    }

    /**
     * Getter fuer weisses Plastik
     * @return
     */
    public Material whitePlastic() {
        return whitePlastic;
    }
}