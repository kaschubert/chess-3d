package chess.presentation.utilities;

/**
 * Singleton fuer Spielernamen und ServerIP
 */
public class Players {
    private static Players instance = null;
    private String player1 = "Spieler 1";
    private String player2 = "Spieler 2";
    private String ip = "127.0.0.1";

    /**
     * Getter IP
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * Setter IP
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Getter fuer Singletoninstanz
     * @return instanz
     */
    public static Players getInstance(){
        if(instance == null)
            instance = new Players();
        return instance;
    }

    /**
     * Getter Spieler 1
     * @return
     */
    public String getPlayer1() {
        return player1;
    }

    /**
     * Setter Spieler 1
     * @param player1
     */
    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    /**
     * Getter Spieler 2
     * @return
     */
    public String getPlayer2() {
        return player2;
    }

    /**
     * Setter Spieler 2
     * @param player2
     */
    public void setPlayer2(String player2) {
        this.player2 = player2;
    }
}