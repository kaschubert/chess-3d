package chess.presentation.utilities;

import javax.media.opengl.GL;

/**
 * Kapselung eines Materials
 */
public class Material {
    private GL gl;
    private float[] ambient;
    private float[] diffuse;
    private float[] specular;
    private float shininess;

    /**
     * Lichtarten und Oberflaechenleuchten werden initialisiert
     * OpenGLKontext wird genutzt, um Objektvariable zu initialisieren
     * @param ambient float[]
     * @param diffuse float[]
     * @param specular float[]
     * @param shininess float
     */
    public Material(float[] ambient, float[] diffuse, float[] specular, float shininess) {
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
        this.shininess = shininess;
        this.gl = OpenGLContext.getInstance(null).getGL();
    }    

    /**
     * setzen dieses Materials
     */
    public void setThisMaterial() {
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT, ambient , 0);
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_DIFFUSE, diffuse, 0);
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_SPECULAR, specular, 0);
        gl.glMaterialf(GL.GL_FRONT_AND_BACK, GL.GL_SHININESS, shininess);
    }
}