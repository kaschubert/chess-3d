package chess.presentation.utilities;

import com.sun.opengl.util.BufferUtil;

import java.nio.FloatBuffer;
import javax.media.opengl.GL;

/**
 * Repraesentiert das Model einer Figur.
 */
public class Model {

    /**
     * Alle Vertices des Models.
     */
    private final Vertex[] vertices;
    /**
     * Alle Normalen des Modells.
     */
    private final Normal[] normals;
    /**
     * Alle Faces des Modells.
     */
    private final Face[] faces;
    /**
     * Alle Texturkoodinaten des Modells.
     */
    private final TexCoord[] texCoords;

    private int endIndex;
    private GL gl;
    private FloatBuffer tmpVerticesBuf;
    private FloatBuffer tmpNormalsBuf;
    private FloatBuffer tmpTexCoordsBuf;

    /**
     * Erzeugt ein neues Model.
     *
     * @param vertices Alle Vertices.
     * @param normals Alle Normalen.
     * @param faces Alle Faces.
     * @param texCoords Alle Texturkordinaten.
     */
    public Model(Vertex[] vertices, Normal[] normals, Face[] faces, TexCoord[] texCoords) {
        this.vertices = vertices;
        this.normals = normals;
        this.faces = faces;
        this.texCoords = texCoords;
        this.gl = OpenGLContext.getInstance().getGL();

        endIndex = faces.length * 3 - 1;

        tmpVerticesBuf = BufferUtil.newFloatBuffer(9 * faces.length);
        tmpNormalsBuf = BufferUtil.newFloatBuffer(9 * faces.length);
        tmpTexCoordsBuf = BufferUtil.newFloatBuffer(9 * faces.length);

        for (Face face : faces) {
            Vertex v1 = vertices[face.getV1() - 1];
            Vertex v2 = vertices[face.getV2() - 1];
            Vertex v3 = vertices[face.getV3() - 1];

            tmpVerticesBuf.put(v1.getX());
            tmpVerticesBuf.put(v1.getY());
            tmpVerticesBuf.put(v1.getZ());

            tmpVerticesBuf.put(v2.getX());
            tmpVerticesBuf.put(v2.getY());
            tmpVerticesBuf.put(v2.getZ());

            tmpVerticesBuf.put(v3.getX());
            tmpVerticesBuf.put(v3.getY());
            tmpVerticesBuf.put(v3.getZ());

            Normal n1 = normals[face.getVn1() - 1];
            Normal n2 = normals[face.getVn2() - 1];
            Normal n3 = normals[face.getVn3() - 1];

            tmpNormalsBuf.put(n1.getX());
            tmpNormalsBuf.put(n1.getY());
            tmpNormalsBuf.put(n1.getZ());

            tmpNormalsBuf.put(n2.getX());
            tmpNormalsBuf.put(n2.getY());
            tmpNormalsBuf.put(n2.getZ());

            tmpNormalsBuf.put(n3.getX());
            tmpNormalsBuf.put(n3.getY());
            tmpNormalsBuf.put(n3.getZ());

            TexCoord t1 = texCoords[face.getVt1() - 1];
            TexCoord t2 = texCoords[face.getVt2() - 1];
            TexCoord t3 = texCoords[face.getVt3() - 1];

            tmpTexCoordsBuf.put(t1.getU());
            tmpTexCoordsBuf.put(t1.getV());
            tmpTexCoordsBuf.put(t1.getW());

            tmpTexCoordsBuf.put(t2.getU());
            tmpTexCoordsBuf.put(t2.getV());
            tmpTexCoordsBuf.put(t2.getW());

            tmpTexCoordsBuf.put(t3.getU());
            tmpTexCoordsBuf.put(t3.getV());
            tmpTexCoordsBuf.put(t3.getW());
        }

        tmpVerticesBuf.rewind();
        tmpNormalsBuf.rewind();
        tmpTexCoordsBuf.rewind();
    }

    public void pushToVertexArray() {
        gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL.GL_NORMAL_ARRAY);
        gl.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY);

        gl.glVertexPointer(3, GL.GL_FLOAT, 0, tmpVerticesBuf);
        gl.glNormalPointer(GL.GL_FLOAT, 0, tmpNormalsBuf);
        gl.glTexCoordPointer(3, GL.GL_FLOAT, 0, tmpTexCoordsBuf);
    }

    public Face[] getFaces() {
        return faces;
    }

    public Normal[] getNormals() {
        return normals;
    }

    public Vertex[] getVertices() {
        return vertices;
    }

    public TexCoord[] getTexCoords() {
        return texCoords;
    }

    public int getEndIndex() {
        return endIndex;
    }
}
