package chess.presentation.utilities;

/**
 * Verwaltung verschiedener Farben
 */
public class Colors {
    //Farben
    private static float[] white = {1, 1, 1, 1};
    private static float[] black = {0, 0, 0, 1};
    private static float[] blue = {0, 0, 1, 1};
    private static float[] red = {1, 0, 0, 1};
    private static float[] green = {0, 1, 0, 1};
    private static float[] yellow = {1, 1, 0, 1};
    private static float[] brown = {0.6f, 0.4f, 0.3f, 1};
    private static float[] grey = {0.4f, 0.4f, 0.4f, 1};
    private static float[] darkGrey = {0.2f, 0.2f, 0.2f, 1};
    private static float[] purple = {0.6f, 0.0f, 0.8f, 1};

    /**
     * Getter fuer Weiss
     * @return
     */
    public static float[] white() {
        return white;
    }

    /**
     * Getter fuer Schwarz
     * @return
     */
    public static float[] black() {
        return black;
    }

    /**
     * Getter fuer Blau
     * @return
     */
    public static float[] blue() {
        return blue;
    }

    /**
     * Getter fuer Rot
     * @return
     */
    public static float[] red() {
        return red;
    }

    /**
     * Getter fuer Gruen
     * @return
     */
    public static float[] green() {
        return green;
    }

    /**
     * Getter fuer Gelb
     * @return
     */
    public static float[] yellow() {
        return yellow;
    }

    /**
     * Getter fuer Braun
     * @return
     */
    public static float[] brown() {
        return brown;
    }

    /**
     * Getter fuer Grau
     * @return
     */
    public static float[] grey() {
        return grey;
    }

    /**
     * Getter fuer dunkles Grau
     * @return
     */
    public static float[] darkGrey() {
        return darkGrey;
    }

    /**
     * Getter fuer Lila
     * @return
     */
    public static float[] purple() {
        return purple;
    }
}