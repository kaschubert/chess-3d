package chess.presentation.utilities;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;

/**
 * Kontext in dem OGL-Operationen ausgefuehrt werden. 
 * Singleton zur Verwaltung der OpenGL-Kontext-Objekte:
 * GL, GLU, GLAutoDrawable (Zeichenflaeche)
 * Objekt dieser Klasse wird statisch in dieser Klasse gespeichert
 */
public class OpenGLContext {
    private GLAutoDrawable autoDrawable;
    private GL gl;
    private GLU glu;
    private static OpenGLContext instance = null;

    /**
     * Initialisierung der Objektvariablen
     * @param autoDrawable GLAutoDrawable
     */
    private  OpenGLContext(GLAutoDrawable autoDrawable){
        this.autoDrawable = autoDrawable;
        this.gl = autoDrawable.getGL();
        this.glu = new GLU();
    }

    /**
     * Kernmethode des Singleton:
     * Je nachdem ob bereits eine Instanz vorhanden ist oder nicht
     * wird entweder direkt die Instanz zurueckgegeben oder zunaechst
     * eine Instanz erzeugt und anschliessend zurueckgegeben
     * @param drawable
     * @return 
     */
    public static OpenGLContext getInstance(GLAutoDrawable drawable){
        if(instance == null)
            instance = new OpenGLContext(drawable);
        return instance;
    }

    /**
     * Getter der Singletoninstanz
     * @return
     */
    public static OpenGLContext getInstance(){
        return instance;
    }

    /**
     * OpenGL-Konfiguration, die global gültig ist. 
     */
    public void configureOpenGL(){
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL.GL_SMOOTH);
        gl.glEnable(GL.GL_NORMALIZE);
        gl.glShadeModel(GL.GL_SMOOTH);
    }

    /**
     * Getter gür GL-Objekt
     * @return GL
     */
    public GL getGL(){
        return this.gl;
    }

    /**
     * Getter fuer GLU-Objekt
     * @return GLU
     */
    public GLU getGLU(){
        return this.glu;
    }

    /**
     * Getter für Zeichenflaeche
     * @return GLAutoDrawable
     */
    public GLAutoDrawable getAutoDrawable(){
        return this.autoDrawable;
    }
}