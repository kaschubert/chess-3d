package chess.presentation.utilities;

/**
 * Camera.
 * Kameraverwaltung
 */
public class Camera {

    /**
     * Augenposition
     */
    private float[] eyePosition;

    /**
     * Kameraposition
     */
    private float[] centerPosition;

    /**
     * Kamerawinkel x
     */
    private float phiX;

    /**
     * Kamerawinkel y
     */
    private float phiY;

    /**
     * Kamerawinkel z
     */
    private float phiZ;

    /**
     * Achsen die rotiert werden sollen
     */
    private boolean[] rotatingAxis;

    /**
     * Winkelschritte für die Achsen
     */
    private int[] angleStep;

    /**
     * maximaler Kamerawinkel der x-Achse
     */
    private float maxphiX;

    /**
     * Objektvariablen werden initialisiert
     * @param getEyePosition
     * @param getCenterPosition
     */
    public Camera(float[] eyePosition, float[] centerPosition) {
        this.eyePosition = eyePosition;
        this.centerPosition = centerPosition;
        phiX = 35.0f;
        phiY = 90.0f;
        maxphiX = 60.0f;
        phiZ = 0.0f;
        rotatingAxis = new boolean[3];
        angleStep = new int[3];
    }

    /**
     * Getter fuer die Augenposition der Kamera
     * @return
     */
    public float[] getEyePosition() {
        return eyePosition;
    }

    /**
     * Getter fuer die Position der Kamera
     * @return
     */
    public float[] getCenterPosition() {
        return centerPosition;
    }

    /**
     * Getter Kamerawinkel x
     * @return Kamerawinkel x
     */
    public float getPhiX(){
        return this.phiX;
    }

    /**
     * Getter Kamerawinkel y
     * @return Kamerawinkel y
     */
    public float getPhiY(){
        return this.phiY;
    }

    /**
     * Getter Kamerawinkel z
     * @return Kamerawinkel z
     */
    public float getPhiZ(){
        return this.phiZ;
    }

    /**
     * Augenposition und Position der Kamera werden
     * je nach Benutzereingabe veraendert
     * @param key
     */
    public void move(char key){
        switch(key){
            case 'f':
                
                centerPosition[2] ++;
                eyePosition[2] ++;
                break;
            case 'b':
                centerPosition[2] --;
                eyePosition[2] --;
                break;
        }/*
            case 'l':
                centerPosition[0] ++;
                eyePosition[0] ++;
                break;
            case 'r':
                centerPosition[0] --;
                eyePosition[0] --;
                break;
            case 'u':
                centerPosition[1] --;
                eyePosition[1] --;
                break;
            case 'd':
                centerPosition[1] ++;
                eyePosition[1] ++;
                break;
        }
        */
    }

    /**
     * Rotation starten
     * Wird vom Listener aufgerufen
     * @param axis
     * @param step
     */
    public void startRotation(char axis, int step){
        if (step > 20)
            step = 1;
        else if (step < -20)
            step = -1;
        else
            step = 0;

        if (axis == 'x') {
            rotatingAxis[0] = true;
            angleStep[0] = step;
        }
        else if (axis == 'y') {
            rotatingAxis[1] = true;
            angleStep[1] = step;
        }
        else if (axis == 'z') {
            rotatingAxis[2] = true;
            angleStep[2] = step;
        }
    }

    /**
     * Rotation stoppen
     * Wird vom Listener aufgerufen
     * @param axis
     */
    public void stopRotation(char axis){
        if (axis == 'x') {
            rotatingAxis[0] = false;
            angleStep[0] = 0;
        }
        else if (axis == 'y') {
            rotatingAxis[1] = false;
            angleStep[1] = 0;
        }
        else if (axis == 'z') {
            rotatingAxis[2] = false;
            angleStep[2] = 0;
        }
    }

    /**
     * Kamera rotieren
     * Wird von der Szene aufgerufen
     */
    public void rotate(){
        if (rotatingAxis[0]) {
            if(angleStep[0] > 0) {
                if(phiX < maxphiX && phiX >= 0.0f){
                    phiX = (phiX + angleStep[0]) % 360;
                }
            }
            else if (angleStep[0] < 0) {
                if(phiX <= maxphiX && phiX > 0.0f){
                    phiX = (phiX + angleStep[0]) % 360;
                }
            }
        }
        if (rotatingAxis[1]) {
            phiY = (phiY + angleStep[1]) % 360;
        }
        if (rotatingAxis[2]) {
            if (angleStep[2] > 0) {
                if(phiZ < maxphiX && phiZ >= 0.0f){
                    phiZ = (phiZ + angleStep[2]) % 360;
                }
            }
            else if (angleStep[2] < 0) {
                if(phiZ <= -maxphiX && phiZ > 0.0f){
                    phiZ = (phiZ + angleStep[2]) % 360;
                }
            }
        }
    }
}