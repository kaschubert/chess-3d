package chess.presentation.utilities;

/**
 * Diese Klasse repraesentiert einen Punkt im dreidimensionalen Raum.
 */
public class Vertex {

    private final float x;
    private final float y;
    private final float z;

   /**
    * Erzeugt einen neuen Vertex.
    *
    * @param x Die x-Koordinate
    * @param y Die y-Koordinate
    * @param z Die z-Koordinate
    */
    public Vertex(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }
}
