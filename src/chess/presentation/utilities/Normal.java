package chess.presentation.utilities;

/**
 * Diese Klasse repraesentiert eine Normale.
 */
public class Normal {

    /**
     * Einheiten in X-Richtung.
     */
    private final float x;
    /**
     * Einheiten in Y-Richtung.
     */
    private final float y;
    /**
     * Einheiten in Z-Richtung.
     */
    private final float z;

    /**
     * Erzeugt eine neue Normale.
     * @param x Einheiten in X-Richtung.
     * @param y Einheiten in Y-Richtung.
     * @param z Einheiten in Z-Richtung.
     */
    public Normal(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }
}
