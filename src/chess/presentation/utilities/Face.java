package chess.presentation.utilities;

/**
 * Speichert Daten ueber eine Flaeche des Models.
 */
public class Face {

    // Die Indizes der Vertices
    private final int v1;
    private final int v2;
    private final int v3;
    // Die Indizes der Texturkoordinaten
    private final int vt1;
    private final int vt2;
    private final int vt3;
    // Die Indizes der Normalen
    private final int vn1;
    private final int vn2;
    private final int vn3;

    /**
     * Erstellt ein neues Face.
     * @param v1 Index des ersten Punktes.
     * @param v2 Index des zweiten Punktes.
     * @param v3 Index des dritten Punktes.
     * @param vt1 Index der ersten Texturkoordinate.
     * @param vt2 Index der zweiten Texturkoordinate.
     * @param vt3 Index der dritten Texturkoordinate.
     * @param vn1 Index der ersten Normale.
     * @param vn2 Index der zweiten Normale.
     * @param vn3 Index der dritten Normale.
     */
    public Face(int v1, int v2, int v3, int vt1, int vt2, int vt3, int vn1, int vn2, int vn3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        this.vt1 = vt1;
        this.vt2 = vt2;
        this.vt3 = vt3;
        this.vn1 = vn1;
        this.vn2 = vn2;
        this.vn3 = vn3;
    }

    public int getV1() {
        return v1;
    }

    public int getV2() {
        return v2;
    }

    public int getV3() {
        return v3;
    }

    public int getVn1() {
        return vn1;
    }

    public int getVn2() {
        return vn2;
    }

    public int getVn3() {
        return vn3;
    }

    public int getVt1() {
        return vt1;
    }

    public int getVt2() {
        return vt2;
    }

    public int getVt3() {
        return vt3;
    }
}
