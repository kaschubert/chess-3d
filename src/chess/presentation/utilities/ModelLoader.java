package chess.presentation.utilities;

import chess.presentation.scenes.LoadMenu;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Laedt die Modelle der Spielfiguren.
 */
public class ModelLoader {
    /**
     * Laedt ein Model einer bestimmten Figur.
     * Die Datei muss sich im Ordner 'models' befinden, in .obj-Format vorliegen
     * (siehe Doku: Blender Hinweise.pfd) und genauso heißen wie die Figur.
     *
     * @param name Der Name der Spielfigur.
     * @return Das geladene Model.
     */
    public static Model loadModel(String name, LoadMenu loadMenu) {

        String fileName = "models/" + name + ".obj";
        File file = new File(fileName);
        
        ArrayList<Vertex> verticesTemp = new ArrayList<Vertex>();
        ArrayList<Face> facesTemp = new ArrayList<Face>();
        ArrayList<Normal> normalsTemp = new ArrayList<Normal>();
        ArrayList<TexCoord> texTemp = new ArrayList<TexCoord>();

        String line;
        String[] lineSplit;
        Scanner sc = null, lc = null;

        int lines = 0, workline = 0;

        try {
            sc = new Scanner(file);
            lc = new Scanner(file);
        } catch (FileNotFoundException ex) {
            System.err.println("The Model " + fileName + " doesn't exist");
            Logger.getLogger(ModelLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (lc.hasNextLine()) {
            lc.nextLine();
            lines++;
        }

        while (sc.hasNextLine()) {
            loadMenu.setModelProgress((float)workline++/(float)lines);

            line = sc.nextLine();
            lineSplit = line.split(" ");
            if (lineSplit.length > 0) {
                if (lineSplit[0].equals("v")) {
                    // Vertices
                    float x = Float.parseFloat(lineSplit[2]);
                    float y = Float.parseFloat(lineSplit[3]);
                    float z = Float.parseFloat(lineSplit[4]);
                    verticesTemp.add(new Vertex(x, y, z));
                } else if (lineSplit[0].equals("f")) {
                    // Faces
                    String[] facesSplit;
                    int v1, v2, v3;
                    int vt1, vt2, vt3;
                    int vn1, vn2, vn3;

                    facesSplit = lineSplit[1].split("/");
                    v1 = Integer.parseInt(facesSplit[0]);
                    vt1 = Integer.parseInt(facesSplit[1]);
                    vn1 = Integer.parseInt(facesSplit[2]);

                    facesSplit = lineSplit[2].split("/");
                    v2 = Integer.parseInt(facesSplit[0]);
                    vt2 = Integer.parseInt(facesSplit[1]);
                    vn2 = Integer.parseInt(facesSplit[2]);

                    facesSplit = lineSplit[3].split("/");
                    v3 = Integer.parseInt(facesSplit[0]);
                    vt3 = Integer.parseInt(facesSplit[1]);
                    vn3 = Integer.parseInt(facesSplit[2]);

                    facesTemp.add(new Face(v1, v2, v3, vt1, vt2, vt3, vn1, vn2, vn3));

                } else if (lineSplit[0].equals("vn")) {
                    // Normalen
                    float x = Float.parseFloat(lineSplit[1]);
                    float y = Float.parseFloat(lineSplit[2]);
                    float z = Float.parseFloat(lineSplit[3]);
                    normalsTemp.add(new Normal(x, y, z));
                } else if (lineSplit[0].equals("vt")) {
                    // Texturkoordinaten
                    float u = Float.parseFloat(lineSplit[1]);
                    float v = Float.parseFloat(lineSplit[2]);
                    float w = Float.parseFloat(lineSplit[3]);
                    texTemp.add(new TexCoord(u, v, w));
                }

            }
        }
        Vertex[] vertices = new Vertex[verticesTemp.size()];
        vertices = verticesTemp.toArray(vertices);

        Face[] faces = new Face[facesTemp.size()];
        faces = facesTemp.toArray(faces);

        Normal[] normals = new Normal[normalsTemp.size()];
        normals = normalsTemp.toArray(normals);

        TexCoord[] texCoords = new TexCoord[texTemp.size()];
        texCoords = texTemp.toArray(texCoords);

        Model model = new Model(vertices, normals, faces, texCoords);

        return model;
    }
}