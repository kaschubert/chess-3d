package chess.presentation.utilities;

import chess.presentation.scenes.*;
import java.util.*;

/**
 * Laderthread zum asynchronen Modelladen.
 */
public class Loader implements Runnable {
    private ArrayList<String> models;
    private LoadMenu loadMenu;

    /**
     * Objektvariablen werden initialisiert
     * @param loadMenu LadeMenue mit OpenGL Kontext
     * @param models Liste aller zu ladenden Models
     */
    public Loader(LoadMenu loadMenu, ArrayList<String> models) {
        this.loadMenu = loadMenu;
        this.models = models;
    }

    /**
     * Runmethode des Threads.
     */
    public void run() {
        int counter = 1;

        for(String model : models) {
            ModelManager.setModel(ModelLoader.loadModel(model, loadMenu), model);
            loadMenu.setModelProgress(0.0f);
            loadMenu.setProgress((float)counter++/(float)models.size());
        }
    }
}
