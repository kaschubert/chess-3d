package chess.presentation.utilities;

import javax.media.opengl.GL;

/**
 * Verwaltet bis zu acht Lichter
 */
public class LightManager {
    private Light[] lights;
    private int lightsLeft;

    /**
     * OpenGLKontext wird genutzt, um Objektvariable zu initialisieren
     * OGL-Lighting wird aktiviert
     */
    public LightManager() {
        GL gl = OpenGLContext.getInstance(null).getGL();
        this.lights = new Light[8];
        gl.glEnable(GL.GL_LIGHTING);
        lightsLeft = 8;
    }

    /**
     * zu Testzwecken.
     * Getter fuer das 0-te Light
     * @return Light
     */
    public Light getLight(){
        return this.lights[0];
    }

    /**
     * generiert solange Lichter, bis acht Lichter in Benutzung sind
     * @param lightPosition float[]
     * @param lightColorAmbient float[]
     * @param lightColorDiffuse float[]
     * @param lightColorSpecular float[]
     * @param lightSpotDirection float[]
     * @param spotAngle int
     */
    public void generateNewLight (
            float[] lightPosition,
            float[] lightColorAmbient,
            float[] lightColorDiffuse,
            float[] lightColorSpecular,
            float[] lightSpotDirection,
            int spotAngle
    ) {
        switch(lightsLeft) {
            case 8: 
                lights[0] = new Light (
                            lightPosition,
                            lightColorAmbient,
                            lightColorDiffuse,
                            lightColorSpecular,
                            lightSpotDirection,
                            spotAngle,
                            0
                        );
                lights[0].init();
                lightsLeft--;
                break;
            case 7: 
                lights[1] = new Light (
                            lightPosition,
                            lightColorAmbient,
                            lightColorDiffuse,
                            lightColorSpecular,
                            lightSpotDirection,
                            spotAngle,
                            1
                        );
                lights[1].init();
                lightsLeft--;
                break;
            case 6: 
                lights[2] = new Light (
                            lightPosition,
                            lightColorAmbient,
                            lightColorDiffuse,
                            lightColorSpecular,
                            lightSpotDirection,
                            spotAngle,
                            2
                        );
                lights[2].init();
                lightsLeft--;
                break;
            case 5:
                lights[3] = new Light (
                            lightPosition,
                            lightColorAmbient,
                            lightColorDiffuse,
                            lightColorSpecular,
                            lightSpotDirection,
                            spotAngle,
                            3
                        );
                lights[3].init();
                lightsLeft--;
                break;
            case 4:
                lights[4] = new Light (
                            lightPosition,
                            lightColorAmbient,
                            lightColorDiffuse,
                            lightColorSpecular,
                            lightSpotDirection,
                            spotAngle,
                            4
                        );
                lights[4].init();
                lightsLeft--;
                break;
            case 3:
                lights[5] = new Light (
                            lightPosition,
                            lightColorAmbient,
                            lightColorDiffuse,
                            lightColorSpecular,
                            lightSpotDirection,
                            spotAngle,
                            5
                        );
                lights[5].init();
                lightsLeft--;
                break;
            case 2:
                lights[6] = new Light (
                            lightPosition,
                            lightColorAmbient,
                            lightColorDiffuse,
                            lightColorSpecular,
                            lightSpotDirection,
                            spotAngle,
                            6
                        );
                lights[6].init();
                lightsLeft--;
                break;
            case 1:
                lights[7] = new Light (
                            lightPosition,
                            lightColorAmbient,
                            lightColorDiffuse,
                            lightColorSpecular,
                            lightSpotDirection,
                            spotAngle,
                            7
                        );
                lights[7].init();
                lightsLeft--;
                break;
            case 0:
                System.err.println("No more Lights possible.");
                break;
        }
    }
}