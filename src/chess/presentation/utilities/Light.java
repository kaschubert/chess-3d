package chess.presentation.utilities;

import javax.media.opengl.GL;

/**
 * Verwaltung eines einzelnen Lichtes.
 */
public class Light{
    private float[] lightPosition;
    private float[] lightColorAmbient;
    private float[] lightColorDiffuse;
    private float[] lightColorSpecular;
    private float[] lightSpotDirection;
    private int spotAngle;
    private int lightNumber;
    private GL gl;

    /**
     * Position, drei Lichtarten, Spotlightwinkel
     * und -richtung werden initialisiert
     * @param lightPosition float[]
     * @param lightColorAmbient float[]
     * @param lightColorDiffuse float[]
     * @param lightColorSpecular float[]
     * @param lightSpotDirection float[]
     * @param spotAngle int
     * @param lightNumber int
     */
    public Light (
        float[] lightPosition,
        float[] lightColorAmbient,
        float[] lightColorDiffuse,
        float[] lightColorSpecular,
        float[] lightSpotDirection,
        int spotAngle,
        int lightNumber
    ) {
        this.lightPosition = lightPosition;
        this.lightColorAmbient = lightColorAmbient;
        this.lightColorDiffuse = lightColorDiffuse;
        this.lightColorSpecular = lightColorSpecular;
        this.lightSpotDirection = lightSpotDirection;
        this.spotAngle = spotAngle;
        this.lightNumber = lightNumber;
        this.gl = OpenGLContext.getInstance(null).getGL();
    }

    /**
     * zu Testzwecken
     * Bewegung eines Lichtes
     * @param key char 
     */
    public void move(char key){
        switch(key){
            case 'f':
                this.lightPosition[2] --;
                break;
            case 'b':
                this.lightPosition[2] ++;
                break;
            case 'l':
                this.lightPosition[0] --;
                break;
            case 'r':
                this.lightPosition[0] ++;
                break;
            case 'u':
                this.lightPosition[1] ++;
                break;
            case 'd':
                this.lightPosition[1] --;
                break;
        }
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, lightPosition , 0);
    }

    /**
     * Mapping vom Lichtobjekt auf das OpenGL-Licht
     * je nach Lichtobjekt werden die entsprechenden OGL-Aufrufe getaetigt
     */
    public void init() {
        switch(this.lightNumber) {
            
            case 0:                
                if((lightSpotDirection != null) && (spotAngle != 0)) {
                    gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPOT_DIRECTION, lightSpotDirection, 0);
                    gl.glLightf(GL.GL_LIGHT0, GL.GL_SPOT_CUTOFF, spotAngle);
                }

                gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, lightPosition , 0);
                gl.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, lightColorAmbient, 0);
                gl.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, lightColorDiffuse, 0);
                gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, lightColorSpecular, 0);
                gl.glEnable(GL.GL_LIGHT0);
                break;

            case 1:
                if((lightSpotDirection != null) && (spotAngle != 0)) {
                    gl.glLightfv(GL.GL_LIGHT1, GL.GL_SPOT_DIRECTION, lightSpotDirection, 0);
                    gl.glLightf(GL.GL_LIGHT1, GL.GL_SPOT_CUTOFF, spotAngle);
                }
                
                gl.glLightfv(GL.GL_LIGHT1, GL.GL_POSITION, lightPosition, 0);
                gl.glLightfv(GL.GL_LIGHT1, GL.GL_AMBIENT, lightColorAmbient, 0);
                gl.glLightfv(GL.GL_LIGHT1, GL.GL_DIFFUSE, lightColorDiffuse , 0);
                gl.glLightfv(GL.GL_LIGHT1, GL.GL_SPECULAR, lightColorSpecular, 0);
                gl.glEnable(GL.GL_LIGHT1);
                break;

            case 2:
                if((lightSpotDirection != null) && (spotAngle != 0)) {
                    gl.glLightfv(GL.GL_LIGHT2, GL.GL_SPOT_DIRECTION, lightSpotDirection, 0);
                    gl.glLightf(GL.GL_LIGHT2, GL.GL_SPOT_CUTOFF, spotAngle);
                }
                
                gl.glLightfv(GL.GL_LIGHT2, GL.GL_POSITION, lightPosition, 0);
                gl.glLightfv(GL.GL_LIGHT2, GL.GL_AMBIENT, lightColorAmbient, 0);
                gl.glLightfv(GL.GL_LIGHT2, GL.GL_DIFFUSE, lightColorDiffuse, 0);
                gl.glLightfv(GL.GL_LIGHT2, GL.GL_SPECULAR, lightColorSpecular, 0);
                gl.glEnable(GL.GL_LIGHT2);
                break;

            case 3:
                if((lightSpotDirection != null) && (spotAngle != 0)) {
                    gl.glLightfv(GL.GL_LIGHT3, GL.GL_SPOT_DIRECTION, lightSpotDirection, 0);
                    gl.glLightf(GL.GL_LIGHT3, GL.GL_SPOT_CUTOFF, spotAngle);
                }
                
                gl.glLightfv(GL.GL_LIGHT3, GL.GL_POSITION, lightPosition, 0);
                gl.glLightfv(GL.GL_LIGHT3, GL.GL_AMBIENT, lightColorAmbient, 0);
                gl.glLightfv(GL.GL_LIGHT3, GL.GL_DIFFUSE, lightColorDiffuse, 0);
                gl.glLightfv(GL.GL_LIGHT3, GL.GL_SPECULAR, lightColorSpecular, 0);
                gl.glEnable(GL.GL_LIGHT3);
                break;

            case 4:
                if((lightSpotDirection != null) && (spotAngle != 0)) {
                    gl.glLightfv(GL.GL_LIGHT4, GL.GL_SPOT_DIRECTION, lightSpotDirection, 0);
                    gl.glLightf(GL.GL_LIGHT4, GL.GL_SPOT_CUTOFF, spotAngle);
                }
                
                gl.glLightfv(GL.GL_LIGHT4, GL.GL_POSITION, lightPosition, 0);
                gl.glLightfv(GL.GL_LIGHT4, GL.GL_AMBIENT, lightColorAmbient, 0);
                gl.glLightfv(GL.GL_LIGHT4, GL.GL_DIFFUSE, lightColorDiffuse, 0);
                gl.glLightfv(GL.GL_LIGHT4, GL.GL_SPECULAR, lightColorSpecular, 0);
                gl.glEnable(GL.GL_LIGHT4);
                break;

            case 5:
                if((lightSpotDirection != null) && (spotAngle != 0)) {
                    gl.glLightfv(GL.GL_LIGHT5, GL.GL_SPOT_DIRECTION, lightSpotDirection, 0);
                    gl.glLightf(GL.GL_LIGHT5, GL.GL_SPOT_CUTOFF, spotAngle);
                }
                
                gl.glLightfv(GL.GL_LIGHT5, GL.GL_POSITION, lightPosition, 0);
                gl.glLightfv(GL.GL_LIGHT5, GL.GL_AMBIENT, lightColorAmbient, 0);
                gl.glLightfv(GL.GL_LIGHT5, GL.GL_DIFFUSE, lightColorDiffuse, 0);
                gl.glLightfv(GL.GL_LIGHT5, GL.GL_SPECULAR, lightColorSpecular, 0);
                gl.glEnable(GL.GL_LIGHT5);
                break;

            case 6:
                if((lightSpotDirection != null) && (spotAngle != 0)) {
                    gl.glLightfv(GL.GL_LIGHT6, GL.GL_SPOT_DIRECTION, lightSpotDirection, 0);
                    gl.glLightf(GL.GL_LIGHT6, GL.GL_SPOT_CUTOFF, spotAngle);
                }
                
                gl.glLightfv(GL.GL_LIGHT6, GL.GL_POSITION, lightPosition, 0);
                gl.glLightfv(GL.GL_LIGHT6, GL.GL_AMBIENT, lightColorAmbient, 0);
                gl.glLightfv(GL.GL_LIGHT6, GL.GL_DIFFUSE, lightColorDiffuse, 0);
                gl.glLightfv(GL.GL_LIGHT6, GL.GL_SPECULAR, lightColorSpecular, 0);
                gl.glEnable(GL.GL_LIGHT6);
                break;
                
            case 7:
                if((lightSpotDirection != null) && (spotAngle != 0)) {
                    gl.glLightfv(GL.GL_LIGHT7, GL.GL_SPOT_DIRECTION, lightSpotDirection, 0);
                    gl.glLightf(GL.GL_LIGHT7, GL.GL_SPOT_CUTOFF, spotAngle);
                }
                
                gl.glLightfv(GL.GL_LIGHT7, GL.GL_POSITION, lightPosition, 0);
                gl.glLightfv(GL.GL_LIGHT7, GL.GL_AMBIENT, lightColorAmbient, 0);
                gl.glLightfv(GL.GL_LIGHT7, GL.GL_DIFFUSE, lightColorDiffuse, 0);
                gl.glLightfv(GL.GL_LIGHT7, GL.GL_SPECULAR, lightColorSpecular, 0);
                gl.glEnable(GL.GL_LIGHT7);
                break;
        }
    }

    /**
     * Veraenderung des Abschwaechungsfaktors durch
     * konstanten, linearen und quadratischen Faktor
     * @param constant float
     * @param linear float
     * @param quadratic float
     */
    public void changeAttenuationFactor(float constant, float linear, float quadratic) {
        switch(this.lightNumber) {
            case 0:
                gl.glLightf(GL.GL_LIGHT0, GL.GL_CONSTANT_ATTENUATION, constant);
                gl.glLightf(GL.GL_LIGHT0, GL.GL_LINEAR_ATTENUATION, linear);
                gl.glLightf(GL.GL_LIGHT0, GL.GL_QUADRATIC_ATTENUATION, quadratic);
                break;
            case 1:
                gl.glLightf(GL.GL_LIGHT1, GL.GL_CONSTANT_ATTENUATION, constant);
                gl.glLightf(GL.GL_LIGHT1, GL.GL_LINEAR_ATTENUATION, linear);
                gl.glLightf(GL.GL_LIGHT1, GL.GL_QUADRATIC_ATTENUATION, quadratic);
                break;
            case 2:
                gl.glLightf(GL.GL_LIGHT2, GL.GL_CONSTANT_ATTENUATION, constant);
                gl.glLightf(GL.GL_LIGHT2, GL.GL_LINEAR_ATTENUATION, linear);
                gl.glLightf(GL.GL_LIGHT2, GL.GL_QUADRATIC_ATTENUATION, quadratic);
                break;
            case 3:
                gl.glLightf(GL.GL_LIGHT3, GL.GL_CONSTANT_ATTENUATION, constant);
                gl.glLightf(GL.GL_LIGHT3, GL.GL_LINEAR_ATTENUATION, linear);
                gl.glLightf(GL.GL_LIGHT3, GL.GL_QUADRATIC_ATTENUATION, quadratic);
                break;
            case 4:
                gl.glLightf(GL.GL_LIGHT4, GL.GL_CONSTANT_ATTENUATION, constant);
                gl.glLightf(GL.GL_LIGHT4, GL.GL_LINEAR_ATTENUATION, linear);
                gl.glLightf(GL.GL_LIGHT4, GL.GL_QUADRATIC_ATTENUATION, quadratic);
                break;
            case 5:
                gl.glLightf(GL.GL_LIGHT5, GL.GL_CONSTANT_ATTENUATION, constant);
                gl.glLightf(GL.GL_LIGHT5, GL.GL_LINEAR_ATTENUATION, linear);
                gl.glLightf(GL.GL_LIGHT5, GL.GL_QUADRATIC_ATTENUATION, quadratic);
                break;
            case 6:
                gl.glLightf(GL.GL_LIGHT6, GL.GL_CONSTANT_ATTENUATION, constant);
                gl.glLightf(GL.GL_LIGHT6, GL.GL_LINEAR_ATTENUATION, linear);
                gl.glLightf(GL.GL_LIGHT6, GL.GL_QUADRATIC_ATTENUATION, quadratic);
                break;
            case 7:
                gl.glLightf(GL.GL_LIGHT7, GL.GL_CONSTANT_ATTENUATION, constant);
                gl.glLightf(GL.GL_LIGHT7, GL.GL_LINEAR_ATTENUATION, linear);
                gl.glLightf(GL.GL_LIGHT7, GL.GL_QUADRATIC_ATTENUATION, quadratic);
                break;
        }
    }
}