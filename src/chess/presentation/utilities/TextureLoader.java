package chess.presentation.utilities;

import javax.media.opengl.*;
import com.sun.opengl.util.texture.*;
import java.io.*;

public class TextureLoader{

    /**
    * TextureLoader laedt eine Textur aus einer Datei
    *
    * @param fileName relativer Dateiname
    * @return eine an den OpenGL Context gebundene Textur
    */
    public static Texture load(String name) {
            Texture texture = null;
            try {
                    texture = TextureIO.newTexture(new File("textures/" + name + ".jpg"), false);
                    texture.setTexParameteri(GL.GL_TEXTURE_MAG_FILTER, GL.GL_NICEST);
                    texture.setTexParameteri(GL.GL_TEXTURE_MIN_FILTER, GL.GL_NICEST);
                    texture.setTexParameteri(GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT);
                    texture.setTexParameteri(GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT);
            }
            catch(Exception e) {
                    System.out.println(e.getMessage());
                    System.out.println("Error loading texture " + name);
            }
            return texture;
    }
}