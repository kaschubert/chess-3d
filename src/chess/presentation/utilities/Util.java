package chess.presentation.utilities;

/**
 * Mathmatische Funktionen
 */
public class Util {

    /**
     * Rechnet einen Winkel im Gradmass ins Bogenmass um
     * @param phi float
     * @return float
     */
    public static float radToArc(float phi){
	return (float) (phi * Math.PI / 180.0);
    }
}