package chess.presentation.utilities;

import com.sun.opengl.util.j2d.TextureRenderer;
import com.sun.opengl.util.texture.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.RenderingHints;
import java.io.File;

public class TextureManager {

    private static Image img;
    private static Texture textTexture;
    private static TextureRenderer textureRenderer;
    private static Graphics2D g2D;

    private static Texture egypt_border;
    private static Texture egypt_border2;

    private static Texture egypt_tile_black;
    private static Texture egypt_tile_white;

    private static Texture egypt_tile_black_selected;
    private static Texture egypt_tile_white_selected;

    private static Texture egypt_tile_black_marked;
    private static Texture egypt_tile_white_marked;

    private static Texture egypt_tile_black_marked_selected;
    private static Texture egypt_tile_white_marked_selected;

    private static Texture egypt_tile_black_attack;
    private static Texture egypt_tile_white_attack;

    private static Texture egypt_tile_black_attack_selected;
    private static Texture egypt_tile_white_attack_selected;

    private static Texture egypt_corner;

    private static Texture egypt_mat_black;
    private static Texture egypt_mat_white;

    private static Texture knight_black;
    private static Texture knight_white;


    public static void setTexture(Texture texture, String name) {    
        if (name.equals("egypt_border"))
            egypt_border = texture;

        else if (name.equals("egypt_border2"))
            egypt_border2 = texture;

        else if (name.equals("egypt_tile_black"))
            egypt_tile_black = texture;

        else if (name.equals("egypt_tile_white"))
            egypt_tile_white = texture;

        else if (name.equals("egypt_tile_black_selected"))
            egypt_tile_black_selected = texture;

        else if (name.equals("egypt_tile_white_selected"))
            egypt_tile_white_selected = texture;

        else if (name.equals("egypt_tile_black_marked"))
            egypt_tile_black_marked = texture;

        else if (name.equals("egypt_tile_white_marked"))
            egypt_tile_white_marked = texture;

        else if (name.equals("egypt_tile_black_marked_selected"))
            egypt_tile_black_marked_selected = texture;

        else if (name.equals("egypt_tile_white_marked_selected"))
            egypt_tile_white_marked_selected = texture;

        else if (name.equals("egypt_tile_black_attack"))
            egypt_tile_black_attack = texture;

        else if (name.equals("egypt_tile_white_attack"))
            egypt_tile_white_attack = texture;

        else if (name.equals("egypt_tile_black_attack_selected"))
            egypt_tile_black_attack_selected = texture;

        else if (name.equals("egypt_tile_white_attack_selected"))
            egypt_tile_white_attack_selected = texture;

        else if (name.equals("egypt_corner"))
            egypt_corner = texture;

        else if (name.equals("egypt_mat_black"))
            egypt_mat_black = texture;

        else if (name.equals("egypt_mat_white"))
            egypt_mat_white = texture;

        else if (name.equals("knight_black"))
            knight_black = texture;

        else if (name.equals("knight_white"))
            knight_white = texture;
    }

    public static Texture egypt_border() {
        return egypt_border;
    }

    public static Texture egypt_border2() {
        return egypt_border2;
    }

    public static Texture egypt_tile_black() {
        return egypt_tile_black;
    }

    public static Texture egypt_tile_white() {
        return egypt_tile_white;
    }

    public static Texture egypt_tile_black_selected() {
        return egypt_tile_black_selected;
    }

    public static Texture egypt_tile_white_selected() {
        return egypt_tile_white_selected;
    }

    public static Texture egypt_tile_black_marked() {
        return egypt_tile_black_marked;
    }

    public static Texture egypt_tile_white_marked() {
        return egypt_tile_white_marked;
    }

    public static Texture egypt_tile_black_marked_selected() {
        return egypt_tile_black_marked_selected;
    }

    public static Texture egypt_tile_white_marked_selected() {
        return egypt_tile_white_marked_selected;
    }

    public static Texture egypt_tile_black_attack() {
        return egypt_tile_black_attack;
    }

    public static Texture egypt_tile_white_attack() {
        return egypt_tile_white_attack;
    }

    public static Texture egypt_tile_black_attack_selected() {
        return egypt_tile_black_attack_selected;
    }

    public static Texture egypt_tile_white_attack_selected() {
        return egypt_tile_white_attack_selected;
    }

    public static Texture egypt_corner() {
        return egypt_corner;
    }

    public static Texture egypt_mat_black() {
        return egypt_mat_black;
    }

    public static Texture egypt_mat_white() {
        return egypt_mat_white;
    }

    public static Texture knight_black() {
        return knight_black;
    }

    public static Texture knight_white() {
        return knight_white;
    }


    public static Texture generateArrowTexture(int xLength, int yLength, boolean mouseIsOver, boolean alpha){
        textureRenderer = new TextureRenderer(xLength, yLength, alpha);
        g2D = textureRenderer.createGraphics();

        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2D.setColor(Color.BLACK);

        g2D.fillRect(0, 0, xLength, yLength);

        if (mouseIsOver) {
            g2D.setColor(Color.DARK_GRAY);
        } else {
            g2D.setColor(Color.WHITE);
        }

        int[] xcoords = {5, xLength-5, xLength-5};
        int[] ycoords = {yLength/2, 10, yLength-10};

        g2D.fillPolygon (xcoords, ycoords, xcoords.length);

        g2D.dispose();
        g2D = null;

        textTexture = textureRenderer.getTexture();
        textureRenderer = null;
        return textTexture;
    }

    /**
     * Texte fuer alle Menueintraege generieren
     */
    public static Texture generateMenuTextTexture(
                                        String text,
                                        int xLength,
                                        int yLength,
                                        boolean mouseIsOver,
                                        boolean alpha,
                                        int textSize,
                                        boolean inverse
    ) {
        textureRenderer = new TextureRenderer(xLength, yLength, alpha);
        g2D = textureRenderer.createGraphics();

        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        try {
            if (mouseIsOver)
                img = ImageIO.read(new File("textures/button1.jpg"));
            else
                if(inverse)
                    img = ImageIO.read(new File("textures/button_inverted.jpg"));
                else
                    img = ImageIO.read(new File("textures/button2.jpg"));
                    
            g2D.drawImage(img.getScaledInstance(xLength, yLength, img.SCALE_SMOOTH), 0, 0, xLength, yLength, null);
        } catch (IOException ex) {
            Logger.getLogger(TextureManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        //if(!inverse)
            g2D.setColor(Color.WHITE);
        /*else
            g2D.setColor(Color.BLACK);*/

        g2D.setFont(new Font("Arial", Font.BOLD, textSize));

        g2D.drawString(text, 10, yLength - textSize /2);

        g2D.dispose();
        
        textTexture = textureRenderer.getTexture();
        return textTexture;
    }
}
