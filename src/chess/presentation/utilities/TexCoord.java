package chess.presentation.utilities;

public class TexCoord {

    private final float u;
    private final float v;
    private final float w;

    public TexCoord(float u, float v, float w) {
        this.u = u;
        this.v = v;
        this.w = w;
    }

    public float getU() {
        return u;
    }

    public float getV() {
        return v;
    }

    public float getW() {
        return w;
    }
}