package chess.presentation.utilities;

import java.awt.Frame;

/**
 * Aufloesung. 
 * Kapselung der Aufloesung in einem Singleton
 * fuer globale Verfügbarkeit
 */
public class Resolution {
    private int xResolution;
    private int yResolution;
    private Frame frame;
    private static Resolution instance = null;

    /**
     * @param xResolution
     * @param yResolution
     */
    private  Resolution(int xResolution, int yResolution){
        this.xResolution = xResolution;
        this.yResolution = yResolution;
        this.frame = null;
    }

    /**
     * Kernmethode des Singleton:
     * Je nachdem ob bereits eine Instanz vorhanden ist oder nicht
     * wird entweder direkt die Instanz zurueckgegeben oder zunaechst
     * eine Instanz erzeugt und anschliessend zurueckgegeben
     * Ueberladung fuer alle weiteren Benutzungen
     * mit Defaultaufloesung 800*600
     * @return Resolution
     */
    public static Resolution getInstance(){
        if(instance == null)
            instance = new Resolution(800, 600);
        return instance;
    }

    /**
     * Getter fuer x
     * @return int
     */
    public int getX(){
        return this.xResolution;
    }

    /**
     * Getter fuer y
     * @return int
     */
    public int getY(){
        return this.yResolution;
    }

    /**
     * Setter x
     * @param x
     */
    public void setX(int x) {
        this.xResolution = x;
    }

    /**
     * Setter y
     * @param y
     */
    public void setY(int y) {
        this.yResolution = y;
    }

    /**
     * Getter Frame
     * @return
     */
    public Frame getFrame() {
        return frame;
    }

    /**
     * Setter Frame
     * @param frame
     */
    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    /**
     * Aufloesung aendern
     * @param x
     * @param y
     * @return Erfolg
     */
    public boolean changeResolution(int x, int y){
        boolean success = false;

        if(frame != null){
            frame.setSize(x + 9, y + 28);
            this.xResolution = x;
            this.yResolution = y;
            success = true;
        }

        return success;
    }
}