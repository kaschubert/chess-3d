package chess.presentation.utilities;

/**
 * Diese Klasse speichert die geladenen Modelle.
 */
public class ModelManager {

    private static Model rookModel;
    private static Model pawnModel;
    private static Model bishopModel;
    private static Model queenModel;
    private static Model kingModel;
    private static Model knightModel;

    public static void setModel(Model model, String name) {
        if (name.equals("pawn"))
            pawnModel = model;

        else if (name.equals("rook"))
            rookModel = model;
        
        else if (name.equals("bishop"))
            bishopModel = model;

        else if (name.equals("queen"))
            queenModel = model;

        else if (name.equals("king"))
            kingModel = model;

        else if (name.equals("knight"))
            knightModel = model;
    }

    public static Model pawnModel() {
        return pawnModel;
    }

    public static Model rookModel() {
        return rookModel;
    }

    public static Model bishopModel() {
        return bishopModel;
    }

    public static Model queenModel() {
        return queenModel;
    }

    public static Model kingModel() {
        return kingModel;
    }

    public static Model knightModel() {
        return knightModel;
    }
}
