package chess.presentation.listener;

import chess.net.NetController;
import chess.presentation.scenes.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.Resolution;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Verarbeitung der Mauseingaben in der WaitingMenu.
 */
public class WaitingMenuMouseListener implements MouseListener, MouseMotionListener{

    /**
     * Szene, für die der Listener arbeitet
     */
    private WaitingMenu waitingMenu;

    /**
     * Szenenmanager
     */
    private SceneManager sceneManager;

    /**
     * Liste der GUI-Komponenten
     */
    private ArrayList<AGuiComponent> gui;

    /**
     * Temporaerer Speicher eines gedrueckten Buttons
     */
    private Button pressedButton;

    /**
     * Aufloesung
     */
    private Resolution resolution;

    /**
     * zum Listener gehörende Scene und SceneManager werden initialisiert
     * @param WaitingMenu
     */
    public WaitingMenuMouseListener(WaitingMenu waitingMenu){
        this.waitingMenu = waitingMenu;
        sceneManager = SceneManager.getInstance();
        gui = this.waitingMenu.getGUI();
        resolution = Resolution.getInstance();
    }

    /**
     * @param e
     */
    public void mouseClicked(MouseEvent e) {

    }
    
    /**
     * @param e
     */
    public void mousePressed(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Press in ihrer Fläche lag
        for(AGuiComponent component: this.gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Button")){
                Button button = (Button) component;
                if(button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                    this.pressedButton = button;
                }
            }
        }
    }

    /**
     * @param e
     */
    public void mouseReleased(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Release in ihrer Fläche lag
        if(this.pressedButton != null){
            if(pressedButton.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                //verschiedene Buttons verarbeiten
                if(pressedButton.getAction().equals("Zurück")){
                    NetController.getInstance().stopServer();
                }
            }
        }
    }

    /**
     * @param e
     */
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * @param e
     */
    public void mouseExited(MouseEvent e) {

    }

    /**
     * @param e
     */
    public synchronized void mouseDragged(MouseEvent e) {

    }

    /**
     * Maus wird ungedrueckt bewegt
     * @param e
     */
    public synchronized void mouseMoved(MouseEvent e) {
        //System.err.println(e.getX() + ";" + e.getY());

        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Cursor in ihrer Fläche liegt
        for(AGuiComponent component: gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Button")){
                Button button = (Button) component;
                if(button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                    button.setMouseIsOver(true);
                }
                else{
                    button.setMouseIsOver(false);
                }
            }
        }
    }
}