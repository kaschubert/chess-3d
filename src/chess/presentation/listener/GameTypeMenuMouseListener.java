package chess.presentation.listener;

import chess.presentation.scenes.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.Resolution;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Verarbeitung der Mauseingaben in der GameTypeMenu.
 */
public class GameTypeMenuMouseListener implements MouseListener, MouseMotionListener{

    /**
     * Szene, für die der Listener arbeitet
     */
    private GameTypeMenu gameTypeMenuScene;

    /**
     * Szenenmanager
     */
    private SceneManager sceneManager;

    /**
     * Liste der GUI-Komponenten
     */
    private ArrayList<AGuiComponent> gui;

    /**
     * Temporaerer Speicher eines gedrueckten Buttons
     */
    private Button pressedButton;

    /**
     * zum Listener gehörende Scene und SceneManager werden initialisiert
     * @param GameTypeMenu
     */
    public GameTypeMenuMouseListener(GameTypeMenu gameTypeMenuScene){
        this.gameTypeMenuScene = gameTypeMenuScene;
        this.sceneManager = SceneManager.getInstance();
        this.gui = this.gameTypeMenuScene.getGUI();
    }

    /**
     * @param e 
     */
    public void mouseClicked(MouseEvent e) {
        
    }

    /**
     * erster Teil der Verarbeitung der verschiedenen Mauseingaben
     * @param e 
     */
    public void mousePressed(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Press in ihrer Fläche lag
        for(AGuiComponent component: this.gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Button")){
                Button button = (Button) component;
                if(button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                    this.pressedButton = button;
                }
            }
        }
    }

    /**
     * zweiter Teil der Verarbeitung der verschiedenen Mauseingaben
     * @param e 
     */
    public void mouseReleased(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Release in ihrer Fläche lag

        if(this.pressedButton != null){
            if(pressedButton.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                //verschiedene Buttons verarbeiten
                if(pressedButton.getAction().equals("Zurück")){
                    this.sceneManager.goToScene("Main");
                }
                else if(pressedButton.getAction().equals("Netzwerk")){
                    this.sceneManager.goToScene("Network");
                }
                else if(pressedButton.getAction().equals("Hot Seat")){
                    this.sceneManager.goToScene("HotSeat");
                }
            }
        }
    }

    /**
     * @param e 
     */
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * @param e 
     */
    public void mouseExited(MouseEvent e) {

    }

    /**
     * @param e
     */
    public synchronized void mouseDragged(MouseEvent e) {

    }

    /**
     * Maus wird ungedrueckt bewegt.
     * @param e
     */
    public synchronized void mouseMoved(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Cursor in ihrer Fläche liegt
        for(AGuiComponent component: gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Button")){
                Button button = (Button) component;
                if(button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                    button.setMouseIsOver(true);
                }
                else{
                    button.setMouseIsOver(false);
                }
            }
        }
    }
}