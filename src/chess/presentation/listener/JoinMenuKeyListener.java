package chess.presentation.listener;

import chess.presentation.scenes.JoinMenu;
import chess.presentation.scenes.gui.AGuiComponent;
import chess.presentation.scenes.gui.Textfield;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Verarbeitung der Tastatureingaben im JoinMenu.
 */
public class JoinMenuKeyListener implements KeyListener{

    /**
     * Szene, für die der Listener arbeitet
     */
    private JoinMenu joinMenu;

    /**
     * Liste der GUI-Komponenten
     */
    private ArrayList<AGuiComponent> gui;

    /**
     * zum Listener gehörende Scene und SceneManager werden initialisiert
     * @param joinMenu
     */
    public JoinMenuKeyListener(JoinMenu joinMenu){
        this.joinMenu = joinMenu;
        this.gui = this.joinMenu.getGUI();
    }

    /**
     * Verarbeitung der Tastatureingaben.
     * @param e
     */
    public void keyTyped(KeyEvent e) {
        for(AGuiComponent component: gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Textfield")){
                Textfield textfield = (Textfield) component;
                if(textfield.getInputActive()){
                    //Spielernamenfeld
                    if(textfield.getStartText().equals("Spieler 2")){
                        if(validCharNumber(e.getKeyChar()) && textfield.getUserInputTextLength() <= 15){
                            textfield.setInputText(e.getKeyChar());
                        }
                        else if(e.getKeyChar() == KeyEvent.VK_BACK_SPACE){
                            textfield.deleteLastInput();
                        }
                    }
                    //Server IP
                    if(textfield.getStartText().equals("ServerIP")){
                        if(validIPChar(e.getKeyChar()) && textfield.getUserInputTextLength() <= 16){
                            textfield.setInputText(e.getKeyChar());
                        }
                        else if(e.getKeyChar() == KeyEvent.VK_BACK_SPACE){
                            textfield.deleteLastInput();
                        }
                    }
                }
            }
        }
    }

    /**
     * Ueberpruefung der Eingabe.
     * @param toCheck zu ueberpruefendes Zeichen
     * @return erlaubt oder nicht erlaubt
     */
    private boolean validCharNumber(char toCheck){
        boolean valid = false;
        char[] validValues = new char[]
            {
                '1','2','3','4','5','6','7','8','9','0',
                'q','w','e','r','t','z','u','i','o','p',
                'ü','a','s','d','f','g','h','j','k','l',
                'ö','ä','y','x','c','v','b','n','m',
                'Q','W','E','R','T','Z','U','I','O','P',
                'Ü','A','S','D','F','G','H','J','K','L',
                'Ö','Ä','Y','X','C','V','B','N','M'
            };

        for(int i = 0; i < validValues.length; i++){
            if(validValues[i] == toCheck)
                valid = true;
        }

        return valid;
    }

    /**
     * Ueberpruefung der Eingabe bei IP.
     * @param toCheck zu ueberpruefendes Zeichen
     * @return erlaubt oder nicht erlaubt
     */
    private boolean validIPChar(char toCheck){
        boolean valid = false;
        char[] validValues = new char[]{'1','2','3','4','5','6','7','8','9','0','.'};

        for(int i = 0; i < validValues.length; i++){
            if(validValues[i] == toCheck)
                valid = true;
        }

        return valid;
    }

    /**
     * @param e
     */
    public void keyPressed(KeyEvent e) {

    }

    /**
     * @param e
     */
    public void keyReleased(KeyEvent e) {

    }
}