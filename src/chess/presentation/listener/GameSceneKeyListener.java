package chess.presentation.listener;

import chess.presentation.scenes.GameScene;
import chess.presentation.scenes.SceneManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Verarbeitung der Tastatureingaben in der GameScene.
 */
public class GameSceneKeyListener implements KeyListener{

    /**
     * Szene, für die der Listener arbeitet
     */
    private GameScene gameScene;

    /**
     * Szenenmanager
     */
    private SceneManager sceneManager;

    /**
     * Neuer GameSceneKeyListener wird erstellt. 
     * zum Listener gehörende Scene und SceneManager werden initialisiert
     * @param gameScene
     */
    public GameSceneKeyListener(GameScene gameScene){
        this.gameScene = gameScene;
        this.sceneManager = SceneManager.getInstance();
    }

    /**
     * @param e 
     */
    public void keyTyped(KeyEvent e) {

    }

    /**
     * Verarbeitung der verschiedenen Tasteneingaben
     * @param e 
     */
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            sceneManager.goToScene("Main");
        }
        //FORWARD
        if(e.getKeyCode() == KeyEvent.VK_UP){
            this.gameScene.getCamera().move('f');
        }
        //BACKWARD
        if(e.getKeyCode() == KeyEvent.VK_DOWN){
            gameScene.getCamera().move('b');
        }
        //LEFT
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            gameScene.getCamera().move('l');
        }
        //RIGHT
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            gameScene.getCamera().move('r');
        }
        //UP
        if(e.getKeyCode() == KeyEvent.VK_PAGE_UP){
            gameScene.getCamera().move('u');
        }
        //DOWN
        if(e.getKeyCode() == KeyEvent.VK_PAGE_DOWN){
            gameScene.getCamera().move('d');
        }
    }

    /**
     * @param e 
     */
    public void keyReleased(KeyEvent e) {

    }
}