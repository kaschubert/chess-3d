package chess.presentation.listener;

import chess.Chess;
import chess.logic.Player;
import chess.net.NetController;
import chess.presentation.Engine;
import chess.presentation.Status;
import chess.presentation.scenes.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.Players;
import chess.presentation.utilities.Resolution;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * Verarbeitung der Mauseingaben in der GameScene.
 */
public class GameSceneMouseListener implements MouseListener, MouseMotionListener {

    /**
     * Szene, für die der Listener arbeitet
     */
    private GameScene gameScene;
    /**
     * Szenenmanager
     */
    private SceneManager sceneManager;
    /**
     * Liste der GUI-Komponenten
     */
    private ArrayList<AGuiComponent> gui;
    /**
     * Temporaerer Speicher eines gedrueckten Buttons
     */
    private Button pressedButton;
    /**
     * Temporaerer Speicher eines gedrueckten Arrows
     */
    private Arrow pressedArrow;
    /**
     * Aufloesung
     */
    private Resolution resolution;
    /**
     * rechte Maustaste gedrueckt
     */
    private boolean rightDown;
    /**
     * letzte Mausposition
     */
    private int oldX, oldY;

    /**
     * zum Listener gehörende Scene und SceneManager werden initialisiert
     * @param gameScene
     */
    public GameSceneMouseListener(GameScene gameScene) {
        this.gameScene = gameScene;
        this.sceneManager = SceneManager.getInstance();
        this.gui = this.gameScene.getGUI();
        this.resolution = Resolution.getInstance();
        rightDown = false;
        oldX = oldY = 0;
    }

    /**
     * @param e 
     */
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * erster Teil der Verarbeitung der verschiedenen Mauseingaben
     * @param e 
     */
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == e.BUTTON3) {
            oldX = e.getX();
            oldY = e.getY();

            rightDown = true;
        }

        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Press in ihrer Fläche lag
        for (AGuiComponent component : this.gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;

                if (button.checkMousePosition(e.getX(), resolution.getY() - e.getY())) {
                    this.pressedButton = button;
                }
            }
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Arrow")) {
                Arrow arrow = (Arrow) component;

                if (arrow.checkMousePosition(e.getX(), resolution.getY() - e.getY())) {
                    this.pressedArrow = arrow;
                    //verschiedene Arrows verarbeiten
                    if (pressedArrow.getDirection().equals("left")) {
                        gameScene.getCamera().startRotation('y', 21);
                    }
                    if (pressedArrow.getDirection().equals("up")) {
                        gameScene.getCamera().startRotation('x', 21);
                    }
                    if (pressedArrow.getDirection().equals("right")) {
                        gameScene.getCamera().startRotation('y', -21);
                    }
                    if (pressedArrow.getDirection().equals("down")) {
                        gameScene.getCamera().startRotation('x', -21);
                    }
                }
            }
        }
        sceneManager.getCurrentScene().giveMousePosClick(e.getX(), e.getY());
    }

    /**
     * zweiter Teil der Verarbeitung der verschiedenen Mauseingaben
     * @param e
     */
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == e.BUTTON3) {
            rightDown = false;
            gameScene.getCamera().stopRotation('x');
            gameScene.getCamera().stopRotation('y');
        }

        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Release in ihrer Fläche lag

        if (this.pressedButton != null) {
            if (pressedButton.checkMousePosition(e.getX(), resolution.getY() - e.getY())) {
                //verschiedene Buttons verarbeiten
                if (pressedButton.getAction().equals("Menü")) {
                    this.sceneManager.goToScene("Main");
                }
                if(
                    !Status.getInstance().getCheckmate() &&
                    !Status.getInstance().getRemis() &&
                    !Status.getInstance().getResign()
                ){
                    if (pressedButton.getAction().equals("Spiel speichern")) {
                        Chess.getPersistance().presentationInputSave();
                    } else if (pressedButton.getAction().equals("Remis anbieten")) {
                        if (Status.getInstance().isNetworkGame()) {
                            NetController.getInstance().sendRemisOutput("offer");
                        } else {
                            String[] options = {"Ja", "Nein"};
                            int choice = JOptionPane.showOptionDialog(
                                    Engine.getCanvas(),
                                    "Soll das Spiel wirklich Remis ausgehen?",
                                    "Remis",
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE,
                                    null, options, options[0]);

                            if (choice == JOptionPane.YES_OPTION) {
                                Chess.getPresentation().logicInputCheck(false);
                                Chess.getPresentation().logicInputRemis();
                            } else if (choice == JOptionPane.NO_OPTION) {
                            }
                        }
                    } else if (pressedButton.getAction().equals("Aufgeben")) {
                        String[] options = {"Ja", "Nein"};
                        int choice = JOptionPane.showOptionDialog(
                                Engine.getCanvas(),
                                "Möchten Sie wirklich aufgeben?",
                                "Aufgeben",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE,
                                null, options, options[0]);
                        if (choice == JOptionPane.YES_OPTION) {
                            Status.getInstance().setResign();
                            if (Status.getInstance().isNetworkGame()) {
                                NetController.getInstance().sendResignOutput();
                            }
                            JOptionPane.showMessageDialog(Engine.getCanvas(), "Sie haben verloren");
                            String onTurnColor = Chess.getLogic().getOnTurnColor();
                            if (onTurnColor.toLowerCase().equals("white")) {
                                // Der andere Spieler gewinnt
                                Chess.getPersistance().presentationInputEndgame(Players.getInstance().getPlayer2());
                            } else {
                                // Der andere Spieler gewinnt
                                Chess.getPersistance().presentationInputEndgame(Players.getInstance().getPlayer1());
                            }
                        }
                    }
                }
            }
        }
        if (this.pressedArrow != null) {
            if (pressedArrow.checkMousePosition(e.getX(), resolution.getY() - e.getY())) {
                //verschiedene Arrows verarbeiten
                gameScene.getCamera().stopRotation('x');
                gameScene.getCamera().stopRotation('y');
            }
        }
    }

    /**
     * @param e
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * @param e
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Maus bleibt gedrueckt. 
     * @param e
     */
    public void mouseDragged(MouseEvent e) {
        if (rightDown) {
            gameScene.getCamera().startRotation('y', oldX - e.getX());
            gameScene.getCamera().startRotation('x', oldY - e.getY());
        }
    }

    /**
     * Maus wird ungedrueckt bewegt.
     * @param e
     */
    public void mouseMoved(MouseEvent e) {
        sceneManager.getCurrentScene().giveMousePosMove(e.getX(), e.getY());

        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Cursor in ihrer Fläche liegt
        for (AGuiComponent component : gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;

                if (button.checkMousePosition(e.getX(), resolution.getY() - e.getY())) {
                    button.setMouseIsOver(true);
                } else {
                    button.setMouseIsOver(false);
                }
            }
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Arrow")) {
                Arrow arrow = (Arrow) component;

                if (arrow.checkMousePosition(e.getX(), resolution.getY() - e.getY())) {
                    arrow.setMouseIsOver(true);
                } else {
                    arrow.setMouseIsOver(false);
                }
            }
        }
    }
}
