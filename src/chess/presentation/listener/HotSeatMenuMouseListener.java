package chess.presentation.listener;

import chess.presentation.scenes.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.Resolution;
import java.awt.event.*;
import java.util.ArrayList;
import chess.Chess;
import chess.presentation.ChessFieldModel;
import chess.presentation.Status;
import chess.presentation.utilities.Players;

/**
 * Verarbeitung der Mauseingaben im HotSeatMenu.
 */
public class HotSeatMenuMouseListener implements MouseListener, MouseMotionListener {

    /**
     * Szene, für die der Listener arbeitet
     */
    private HotSeatMenu hotSeatMenu;
    /**
     * Szenenmanager
     */
    private SceneManager sceneManager;
    /**
     * Liste der GUI-Komponenten
     */
    private ArrayList<AGuiComponent> gui;
    /**
     * Temporaerer Speicher eines gedrueckten Buttons
     */
    private Button pressedButton;
    /**
     * Temporaerer Speicher eines gedrueckten Textfields
     */
    private Textfield pressedTextfield;
    /**
     * Temporaerer Speicher des letzten Textfields
     */
    private Textfield lastTextfield;

    /**
     * zum Listener gehörende Scene und SceneManager werden initialisiert
     * @param HotSeatMenu
     */
    public HotSeatMenuMouseListener(HotSeatMenu hotSeatMenuScene) {
        this.hotSeatMenu = hotSeatMenuScene;
        this.sceneManager = SceneManager.getInstance();
        this.gui = this.hotSeatMenu.getGUI();
        this.lastTextfield = null;
    }

    /**
     * @param e
     */
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * erster Teil der Verarbeitung der verschiedenen Mauseingaben
     * @param e
     */
    public void mousePressed(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Press in ihrer Fläche lag
        for (AGuiComponent component : this.gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;
                if (button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                    this.pressedButton = button;
                }
            }
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Textfield")) {
                Textfield textfield = (Textfield) component;
                if (textfield.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                    this.pressedTextfield = textfield;
                }
            }
        }
    }

    /**
     * zweiter Teil der Verarbeitung der verschiedenen Mauseingaben
     * @param e
     */
    public void mouseReleased(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Release in ihrer Fläche lag

        if (this.pressedButton != null) {
            if (this.pressedButton.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                //verschiedene Buttons verarbeiten
                if (this.pressedButton.getAction().equals("Zurück")) {
                    this.sceneManager.goToScene("GameType");
                } else if (this.pressedButton.getAction().equals("Start")) {
                    String player1 = Players.getInstance().getPlayer1();
                    String player2 = Players.getInstance().getPlayer2();
                    Chess.getLogic().newGame(
                            player1,
                            player2,
                            "white",
                            false,
                            null);
                    ChessFieldModel.getInstance().reset();
                    Status.getInstance().setNetworkGame(false);
                    Chess.getPersistance().presentationInputStartgame(player1, player2);
                    this.sceneManager.goToScene("Game");
                }
                if (pressedButton.getAction().equals("Spiel laden")) {
                    this.sceneManager.goToScene("Loadgame");
                }
            }
        }
        if (this.pressedTextfield != null) {
            if (this.pressedTextfield.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                //letztes Textfeld aus dem Fokus nehmen
                if (this.lastTextfield != null) {
                    this.lastTextfield.setInputActive(false);
                }
                //Fokus auf aktuelles Textfeld legen
                this.pressedTextfield.setInputActive(true);

                //aktuelles Textfeld speichern
                this.lastTextfield = this.pressedTextfield;
            }
        }
    }

    /**
     * @param e
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * @param e
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * @param e
     */
    public synchronized void mouseDragged(MouseEvent e) {
    }

    /**
     * Maus wird ungedrueckt bewegt
     * @param e
     */
    public synchronized void mouseMoved(MouseEvent e) {

        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Cursor in ihrer Fläche liegt
        for (AGuiComponent component : gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;
                if (button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                    button.setMouseIsOver(true);
                } else {
                    button.setMouseIsOver(false);
                }
            } else if (component.getClass().getName().equals("chess.presentation.scenes.gui.Textfield")) {
                Textfield textfield = (Textfield) component;
                if (textfield.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                    textfield.setMouseIsOver(true);
                } else {
                    textfield.setMouseIsOver(false);
                }
            }
        }
    }
}
