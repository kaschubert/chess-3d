package chess.presentation.listener;

import chess.Chess;
import chess.logic.Gameboard;
import chess.net.NetController;
import chess.presentation.ChessFieldModel;
import chess.presentation.Status;
import chess.presentation.scenes.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Verarbeitung der Mauseingaben in der HostMenu.
 */
public class HostMenuMouseListener implements MouseListener, MouseMotionListener {

    /**
     * Szene, für die der Listener arbeitet
     */
    private HostMenu hostMenu;
    /**
     * Szenenmanager
     */
    private SceneManager sceneManager;
    /**
     * Liste der GUI-Komponenten
     */
    private ArrayList<AGuiComponent> gui;
    /**
     * Temporaerer Speicher eines gedrueckten Buttons
     */
    private Button pressedButton;
    /**
     * Temporaerer Speicher eines gedrueckten Textfields
     */
    private Textfield pressedTextfield;
    /**
     * Temporaerer Speicher des letzten Textfields
     */
    private Textfield lastTextfield;

    /**
     * zum Listener gehoerende Scene und SceneManager werden initialisiert
     * @param hostMenu
     */
    public HostMenuMouseListener(HostMenu hostMenu) {
        this.hostMenu = hostMenu;
        this.sceneManager = SceneManager.getInstance();
        this.gui = this.hostMenu.getGUI();
        this.lastTextfield = null;
    }

    /**
     * @param e
     */
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * erster Teil der Verarbeitung der verschiedenen Mauseingaben
     * @param e
     */
    public void mousePressed(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Press in ihrer Fläche lag
        for (AGuiComponent component : this.gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;
                if (button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                    this.pressedButton = button;
                }
            }
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Textfield")) {
                Textfield textfield = (Textfield) component;
                if (textfield.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                    this.pressedTextfield = textfield;
                }
            }
        }
    }

    /**
     * zweiter Teil der Verarbeitung der verschiedenen Mauseingaben
     * @param e 
     */
    public void mouseReleased(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Release in ihrer Fläche lag

        if (this.pressedButton != null) {
            if (pressedButton.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                //verschiedene Buttons verarbeiten
                if (pressedButton.getAction().equals("Zurück")) {
                    this.sceneManager.goToScene("Network");
                } else if (pressedButton.getAction().equals("Starten")) {

                    // Setzt das Spielbrett auf die Grundstellung.
                    ChessFieldModel.getInstance().reset();
                    Gameboard.getInstance().reset();
                    Status.getInstance().setNetworkGame(true);

                    NetController.getInstance().startServerRoutine(Players.getInstance().getPlayer1());
                    Chess.getPersistance().presentationInputStartgame(Players.getInstance().getPlayer1(), Players.getInstance().getPlayer2());
                    this.sceneManager.goToScene("Waiting");
                }
            }
        }
        if (this.pressedTextfield != null) {
            if (this.pressedTextfield.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                //letztes Textfeld aus dem Fokus nehmen
                if (this.lastTextfield != null) {
                    this.lastTextfield.setInputActive(false);
                }
                //Fokus auf aktuelles Textfeld legen
                this.pressedTextfield.setInputActive(true);

                //aktuelles Textfeld speichern
                this.lastTextfield = this.pressedTextfield;
            }
        }
    }

    /**
     * @param e
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * @param e
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * @param e
     */
    public synchronized void mouseDragged(MouseEvent e) {
    }

    /**
     * Maus wird ungedrueckt bewegt. 
     * @param e
     */
    public synchronized void mouseMoved(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Cursor in ihrer Fläche liegt
        for (AGuiComponent component : gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;
                if (button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                    button.setMouseIsOver(true);
                } else {
                    button.setMouseIsOver(false);
                }
            } else if (component.getClass().getName().equals("chess.presentation.scenes.gui.Textfield")) {
                Textfield textfield = (Textfield) component;
                if (textfield.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())) {
                    textfield.setMouseIsOver(true);
                } else {
                    textfield.setMouseIsOver(false);
                }
            }
        }
    }
}
