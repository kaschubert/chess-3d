package chess.presentation.listener;

import chess.presentation.scenes.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.Resolution;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Verarbeitung der Mauseingaben in der MainMenu.
 */
public class OptionsMenuMouseListener implements MouseListener, MouseMotionListener{

    /**
     * Szene, für die der Listener arbeitet
     */
    private OptionsMenu optionsMenu;

    /**
     * Szenenmanager
     */
    private SceneManager sceneManager;

    /**
     * Liste der GUI-Komponenten
     */
    private ArrayList<AGuiComponent> gui;

    /**
     * Temporaerer Speicher eines gedrueckten Buttons
     */
    private Button pressedButton;

    /**
     * Aufloesung
     */
    private Resolution resolution;

    /**
     * zum Listener gehörende Scene und SceneManager werden initialisiert
     * @param OptionsMenu
     */
    public OptionsMenuMouseListener(OptionsMenu optionsMenuScene){
        this.optionsMenu = optionsMenuScene;
        sceneManager = SceneManager.getInstance();
        gui = this.optionsMenu.getGUI();
        resolution = Resolution.getInstance();
    }

    /**
     * @param e
     */
    public void mouseClicked(MouseEvent e) {

    }
    
    /**
     * @param e
     */
    public void mousePressed(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Press in ihrer Fläche lag
        for(AGuiComponent component: this.gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Button")){
                Button button = (Button) component;
                if(button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                    this.pressedButton = button;
                }
            }
        }
    }

    /**
     * @param e
     */
    public void mouseReleased(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Release in ihrer Fläche lag
        if(this.pressedButton != null){
            if(pressedButton.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                //verschiedene Buttons verarbeiten
                if(pressedButton.getAction().equals("Zurück")){
                    this.sceneManager.goToScene("Main");
                }
                else if(pressedButton.getAction().equals("800 * 600")){
                    resolution.changeResolution(800, 600);
                }
                else if(pressedButton.getAction().equals("1024 * 768")){
                    resolution.changeResolution(1024, 768);
                }
            }
        }
    }

    /**
     * @param e
     */
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * @param e
     */
    public void mouseExited(MouseEvent e) {

    }

    /**
     * @param e
     */
    public synchronized void mouseDragged(MouseEvent e) {

    }

    /**
     * Maus wird ungedrueckt bewegt
     * @param e
     */
    public synchronized void mouseMoved(MouseEvent e) {
        //GUI-Komponenten durchgehen und bei allen Buttons prüfen,
        //ob der Cursor in ihrer Fläche liegt
        for(AGuiComponent component: gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Button")){
                Button button = (Button) component;
                if(button.checkMousePosition(e.getX(), Resolution.getInstance().getY() - e.getY())){
                    button.setMouseIsOver(true);
                }
                else{
                    button.setMouseIsOver(false);
                }
            }
        }
    }
}