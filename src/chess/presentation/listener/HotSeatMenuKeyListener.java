package chess.presentation.listener;

import chess.presentation.scenes.HotSeatMenu;
import chess.presentation.scenes.gui.AGuiComponent;
import chess.presentation.scenes.gui.Textfield;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Verarbeitung der Tastatureingaben im HotSeatMenu. 
 */
public class HotSeatMenuKeyListener implements KeyListener{

    /**
     * Szene, für die der Listener arbeitet
     */
    private HotSeatMenu hotSeatMenu;

    /**
     * Liste der GUI-Komponenten
     */
    private ArrayList<AGuiComponent> gui;

    /**
     * zum Listener gehörende Scene und GUI werden initialisiert
     * @param hotSeatMenuScene
     */
    public HotSeatMenuKeyListener(HotSeatMenu hotSeatMenuScene){
        this.hotSeatMenu = hotSeatMenuScene;
        this.gui = this.hotSeatMenu.getGUI();
    }

    /**
     * Verarbeitung der Tastatureingaben.
     * @param e
     */
    public void keyTyped(KeyEvent e) {
        for(AGuiComponent component: gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Textfield")){
                Textfield textfield = (Textfield) component;
                if(textfield.getInputActive()){
                    if(validCharNumber(e.getKeyChar()) && textfield.getUserInputTextLength() <= 15){
                        textfield.setInputText(e.getKeyChar());
                    }
                    else if(e.getKeyChar() == KeyEvent.VK_BACK_SPACE){
                        textfield.deleteLastInput();
                    }
                }
            }
        }
    }

    /**
     * Ueberpruefung der Eingabe.
     * @param toCheck zu ueberpruefendes Zeichen
     * @return erlaubt oder nicht erlaubt
     */
    private boolean validCharNumber(char toCheck){
        boolean valid = false;
        char[] validValues = new char[]
            {
                '1','2','3','4','5','6','7','8','9','0',
                'q','w','e','r','t','z','u','i','o','p',
                'ü','a','s','d','f','g','h','j','k','l',
                'ö','ä','y','x','c','v','b','n','m',
                'Q','W','E','R','T','Z','U','I','O','P',
                'Ü','A','S','D','F','G','H','J','K','L',
                'Ö','Ä','Y','X','C','V','B','N','M'
            };

        for(int i = 0; i < validValues.length; i++){
            if(validValues[i] == toCheck)
                valid = true;
        }

        return valid;
    }

    /**
     *
     * @param e
     */
    public void keyPressed(KeyEvent e) {
        
    }

    /**
     *
     * @param e
     */
    public void keyReleased(KeyEvent e) {
        
    }
}