package chess.presentation;

/**
 * Schnittstelle der Praesentation.
 */
public interface IPresentation {

    /**
     * Markierungseingaben aus der Logik.
     * @param x Position
     * @param y Position
     */
    public void logicInput(int x, int y);

    /**
     * Demarkierungseingaben aus der Logik.
     */
    public void logicInput();

    /**
     * Bewegungseingaben aus der Logik.
     * @param x Position
     * @param y Position
     */
    public void logicMoveInput(int x, int y);

    /**
     * Eingabe aus der Logik, die eine Figur setzt.
     * Falls auf diesem Feld bereits eine Figur steht, wird diese entfernt.
     * @param type Der Typ der Figur
     * @param color Die Farbe der Figur
     * @param x Die x-Koordinate des Feldes.
     * @param y Die y-Koordinate des Feldes.
     */
    public void logicInputPutFigure(String type, String color, int x, int y);

    /**
     * Eingabe aus der Logik, die eine Figur loescht.
     * @param x Die x-Koordinate des Feldes.
     * @param y Die y-Koordinate des Feldes.
     */
    public void logicInputRemoveFigure(int x, int y);

    /**
     * Eingabe aus der Persistenz.
     */
    public void persistenceInput();

    /**
     * Eingabe aus der Logik, die den Status Schachmatt setzt.
     */
    public void logicInputCheckMate();

    /**
     * Eingabe aus der Logik, die den Status Schach setzt.
     * @param check Status auf den Check gesetzt werden soll
     */
    public void logicInputCheck(boolean check);

    /**
     * Eingabe aus der Logik, die den Status Remis setzt.
     */
    public void logicInputRemis();

    /**
     * Eingabe aus dem Modul Net, die beide Spielernamen setzt.
     * @param nameWhite Name des weißen Spielers
     * @param nameBlack Name des schwarzen Spielers
     */
    public void netInputPlayer(String nameWhite, String nameBlack);
}
