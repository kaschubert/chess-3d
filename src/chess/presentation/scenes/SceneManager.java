package chess.presentation.scenes;

import java.util.ArrayList;
/**
 * Szenenverwaltung. 
 * Singleton
 * Verwaltet alle Szenen und ermöglicht es nach belieben
 * zwischen allen Szenen zu wechseln
 */
public class SceneManager {

    /**
     * aktuelle Szene
     */
    private String currentScene;

    /**
     * Listen aller Szenen
     */
    private ArrayList<AScene> scenes;

    /**
     * Instanz des Singleton
     */
    private static SceneManager instance = null;

    /**
     * Objektvariablen werden initialisiert
     */
    private SceneManager(){
        this.currentScene = "Load";
        this.scenes = new ArrayList<AScene>();
    }

    /**
     * Methode zur Konstruktion und zum Zugriff auf den Singleton
     * @return SceneManager
     */
    public static SceneManager getInstance(){
        if(instance == null)
            instance = new SceneManager();
        return instance;
    }

    /**
     * Hinzufügen einer neuen Szene
     * @param newScene neue Szene
     */
    public void addScene(AScene newScene){
        scenes.add(newScene);
    }

    /**
     * zu einer bestimmten Szene wechseln
     */
    public void goToScene(String targetScene){
        //Reset-Deregistrierung der alten Szene
        for(AScene scene: this.scenes) {
            if(scene.getSceneName().equals(this.currentScene)){
                scene.unSetListener();
                scene.setNoMouseOver();
            }
        }

        //Neue Szene finden
        for(AScene scene: this.scenes) {
            if(scene.getSceneName().equals(targetScene)){
                this.currentScene = targetScene;
                scene.setListener();
            }
        }
    }

    /**
     * Liefert die aktuelle Szene
     * @return aktuelle Szene
     */
    public AScene getCurrentScene(){
        AScene current = null;

        for(AScene scene: this.scenes) {
            if(scene.getSceneName().equals(this.currentScene)){
                current = scene;
            }
        }
        return current;
    }

    /**
     * Alle Szenen an neue Aufloesung anpassen
     */
    public void reshapeAllScenes(){
        for(AScene scene: this.scenes) {
            scene.reshapeScene();
        }
    }
}