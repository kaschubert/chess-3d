package chess.presentation.scenes;

import chess.Chess;
import chess.presentation.listener.LoadgameMenuNetMouseListener;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.Camera;
import chess.presentation.utilities.Colors;
import chess.presentation.utilities.OpenGLContext;
import javax.media.opengl.GL;

public class LoadgameMenuNet extends AScene {

    /**
     * Der zugehoerige Maus-Listener.
     */
    private LoadgameMenuNetMouseListener mouseListener;
    /**
     * Die Anzahl der Savegames, die auf einer Seite angezeigt werden sollen.
     */
    private final int savesToShow = 8;
    /**
     * Die aktuell angezeigte Seite.
     */
    private int page = 0;
    /**
     * Die Anzahl an Seiten, die benoetigt werden.
     */
    private int pagesCount;
    /**
     * Flag dient dazu, nur die Save-Buttons nur beim Wechsel zur Szene zu laden.
     */
    private boolean flag;
    /**
     * Alle Buttons fuer die Savegames.
     */
    private Button[][] buttons;
    /**
     * Der "Hoch" Button
     */
    private Button scrollUp;
    /**
     * Der "Runter"-Button
     */
    private Button scrollDown;
    /**
     * Der Zurueck-Button.
     */
    private Button back;

    public LoadgameMenuNet() {
        super();
        mouseListener = new LoadgameMenuNetMouseListener(this);
        camera = new Camera(new float[]{2.0f, 1.0f, 14.0f}, new float[]{0.0f, 0.0f, 0.0f});
        SceneName = "LoadgameNet";

        scrollDown = new Button("        Runter", new float[]{4.0f, 12.0f}, 2.0f, 14.0f, 1, 10);
        scrollUp = new Button("        Hoch", new float[]{4.0f, 12.0f}, 2.0f, 14.0f, 1, 1);
        back = new Button("Zurück", new float[]{32.0f, 32.0f}, 7.0f, 14.0f, 1, 29);
        flag = true;
        initScene();
    }

    /**
     * Szene initialisieren.
     */
    @Override
    public void initScene() {
        gl.glLoadIdentity();

        this.setPerspective();

        gl.glMatrixMode(GL.GL_MODELVIEW);
    }

    /**
     * Perspektive setzen.
     */
    private void setPerspective() {
        gl.glViewport(0, 0, resolution.getX(), resolution.getY());
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, (float) resolution.getX() / (float) resolution.getY(), 0.1f, 500.0f);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Szene anzeigen.
     */
    @Override
    public void displayScene() {
        gl.glClearColor(Colors.white()[0],
                Colors.white()[1],
                Colors.white()[2],
                Colors.white()[3]);

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();

        glu.gluLookAt(
                camera.getEyePosition()[0], camera.getEyePosition()[1], camera.getEyePosition()[2],
                camera.getCenterPosition()[0], camera.getCenterPosition()[1], camera.getCenterPosition()[2],
                0.0f, 1.0f, 0.0f);

        this.viewOrtho();

        if (flag) {
            refreshSaveButtons();
            setDrawStuff();
            flag = false;
        }

        //GUI zeichnen
        for (AGuiComponent button : gui) {
            button.draw();
        }

        this.viewPerspective();
    }

    /**
     *
     */
    @Override
    public void setListener() {
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseMotionListener(this.mouseListener);
    }

    /**
     *
     */
    @Override
    public void unSetListener() {
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseMotionListener(this.mouseListener);
        flag = true;
    }

    /**
     *
     */
    @Override
    public void setNoMouseOver() {
        for (AGuiComponent component : gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;
                button.setMouseIsOver(false);
            }
        }
    }

    /**
     * Wird aufgerufen, wenn der "Runter" Button gedrueckt wird.
     */
    public void down() {
        page++;
        setDrawStuff();
    }

    /**
     * Wird aufgerufen, wenn der "Hoch" Button gedrueckt wird.
     */
    public void up() {
        page--;
        setDrawStuff();
    }

    /**
     * Muss aufgerufen werden, bevor zu dieser Szene gewechselt wird.
     * Diese Methode erstellt einen Button fuer jedes Savegame.
     */
    private void refreshSaveButtons() {
        String[] savenames = Chess.getPersistance().getSavenames();
        pagesCount = (savenames.length / savesToShow) + 1;
        buttons = new Button[pagesCount][savesToShow];

        // Fuer jedes Save einen Button erzeugen

        for (int i = 0; i < savenames.length; i++) {
            Button b = new Button(
                    savenames[i],
                    new float[]{4.0f, 12.0f},
                    2.0f,
                    14.0f,
                    1,
                    2 + (i % savesToShow));

            buttons[i / savesToShow][i % savesToShow] = b;
        }
    }

    /**
     * Diese Methode managed die anzuzeigenden Gui-Komponenten.
     */
    private synchronized void setDrawStuff() {
        gui.clear();

        gui.add(back);

        if (page != 0) {
            gui.add(scrollUp);
        }


        for (int i = 0; i < buttons[page].length; i++) {
            if (buttons[page][i] != null) {
                gui.add(buttons[page][i]);
            }
        }
        if (page != pagesCount - 1) {
            gui.add(scrollDown);
        }
    }
}
