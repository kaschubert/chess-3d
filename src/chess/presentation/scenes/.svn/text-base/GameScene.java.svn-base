package chess.presentation.scenes;

import chess.presentation.Status;
import chess.Chess;
import chess.presentation.ChessFieldModel;
import chess.presentation.drawables.*;
import chess.presentation.listener.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.*;
import java.util.ArrayList;
import javax.media.opengl.GL;

import java.nio.*;

/**
 * Szene, in der der eigentliche Spielbildschirm angezeigt wird. 
 */
public class GameScene extends AScene {

    /**
     * Mauslistener der Szene
     */
    private GameSceneMouseListener mouseListener;

    /**
     * KeyListener der Szene
     */
    private GameSceneKeyListener keyListener;

    /**
     * Status des Schachspiels
     */
    private Status status;

    /**
     * SchachmattLabel
     */
    private Label checkmate;

    /**
     * SchachLabel
     */
    private Label check;

    /**
     * RemisLabel
     */
    private Label remis;

    /**
     * Player1Label
     */
    private Label player1;

    /**
     * Player2Label
     */
    private Label player2;

    /**
     * Initialisierung der Liste der Szenenteile und der GUI,
     * der Kamera, der Materialien, des Lichtes und der Hintergrundfarbe
     */
    public GameScene() {
        this.mouseListener = new GameSceneMouseListener(this);
        this.keyListener = new GameSceneKeyListener(this);
        this.status = Status.getInstance();
        this.camera = new Camera(new float[]{0.0f, 0.0f, -30.0f}, new float[]{0.0f, 0.0f, 0.0f});
        this.backgroundColor = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
        this.SceneName = "Game";
        this.initScene();
    }

    /**
     * Erzeugen aller Szenenteile und einfügen in die Liste der Szenenteile
     * Erzeugen aller GUI-Elemente und einfügen in die Liste der GUI-Teile
     * Erzeugen der Lichter und initialisieren der Perspektive
     */
    @Override
    public void initScene() {
        sceneParts.add(new ChessField(2.0f, 2.0f, materials.whitePlastic()));
        //sceneParts.add(new CoordinatePlane(20, 20, 20));

        sceneParts.get(0).setPosition(new float[]{0.0f, 0.0f, 0.0f});
        //sceneParts.get(1).setPosition(new float[] {0.0f, 0.2f, 0.0f});

        checkmate = new Label(
                "Schachmatt",
                new float[]{10.0f, 24.0f},
                4.7f,
                13.3f,
                4,
                2,
                26,
                true);

        check = new Label(
                "Schach",
                new float[]{10.0f, 24.0f},
                7.2f,
                13.3f,
                4,
                2,
                26,
                true);

        remis = new Label(
                "Remis",
                new float[]{10.0f, 24.0f},
                7.2f,
                13.3f,
                4,
                2,
                26,
                true);

        player1 = new Label(
                "",
                new float[]{32.0f, 24.0f},
                3.2f,
                24.0f,
                1,
                1,
                16,
                true);

        gui.add(player1);

        player2 = new Label(
                "",
                new float[]{32.0f, 24.0f},
                3.2f,
                24.0f,
                1,
                2,
                16,
                true);

        gui.add(player2);

        //GUI
        Button b1 = new Button(
                "Menü",
                new float[]{32.0f, 32.0f},
                7.3f,
                14.0f,
                1,
                29);

        gui.add(b1);

        Button save = new Button(
                "Spiel speichern",
                new float[]{5.0f, 32.0f},
                3.7f,
                14.0f,
                1,
                29);

        gui.add(save);

        Button remisOffer = new Button(
                "Remis anbieten",
                new float[]{2.0f, 32.0f},
                3.7f,
                14.0f,
                1,
                29);

        gui.add(remisOffer);

         Button resign = new Button(
                "Aufgeben",
                new float[]{1.25f, 32.0f},
                6.0f,
                14.0f,
                1,
                29);

        gui.add(resign);

        Arrow a1 = new Arrow(
                "left",
                new float[]{42.0f, 42.0f},
                18.0f,
                20.0f,
                39,
                37);
        Arrow a3 = new Arrow(
                "right",
                new float[]{42.0f, 42.0f},
                18.0f,
                20.0f,
                39,
                37);
        Arrow a2 = new Arrow(
                "up",
                new float[]{42.0f, 42.0f},
                18.0f,
                20.0f,
                39,
                38);
        Arrow a4 = new Arrow(
                "down",
                new float[]{42.0f, 42.0f},
                18.0f,
                20.0f,
                39,
                36);

        // gui.add(a1);
        // gui.add(a2);
        // gui.add(a3);
        // gui.add(a4);

        setLights();
        setPerspective();

        gl.glMatrixMode(GL.GL_MODELVIEW);
    }

    /**
     * Lichter setzen.
     * Submethode zur Generierung von bis zu acht Lichtern
     */
    private void setLights() {
        lightManager.generateNewLight(
                new float[]{0.0f, 0.0f, 0.0f, 1.0f}, //Position
                Colors.white(), //Ambient
                Colors.white(), //Diffuse
                new float[]{1.0f, 1.0f, 1.0f, 1.0f}, //Specular
                null,//lightSpotDirection
                0 //spotAngle
                );
        /*lightManager.generateNewLight (
        new float[] {8.0f, 2.0f, 5.0f, 1.0f}, //Position
        Colors.red(), //Ambient
        Colors.red(), //Diffuse
        Colors.white(), //Specular
        null,//lightSpotDirection
        0 //spotAngle
        );*/
    }

    /**
     * Perspektive setzen.
     * Submethode zur Spezifikation der Perspektive.
     */
    private void setPerspective() {
        /*
        gl.glMatrixMode(GL.GL_PROJECTION);
        
        gl.glLoadIdentity();

        glu.gluPerspective(75.0f, this.resolution.getX() / this.resolution.getY(), 0.1f, 500.0f);
         */

        gl.glViewport(0, 0, resolution.getX(), resolution.getY());
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, (float) resolution.getX() / (float) resolution.getY(), 0.1f, 500.0f);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Szene anzeigen.
     * Es werden falls nötig die Input-Listener registriert,
     * die Kameraposition wird spezifiziert und die Hintergrundfarbe wird variiert. 
     * Alle Elemente der Szene und der GUI werden gezeichnet. 
     */
    @Override
    public void displayScene() {
        gl.glClearColor(
                backgroundColor[0],
                backgroundColor[1],
                backgroundColor[2],
                backgroundColor[3]);

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();

        camera.rotate();

        player1.setText(Players.getInstance().getPlayer1());
        player2.setText(Players.getInstance().getPlayer2());

        gl.glTranslatef(camera.getEyePosition()[0], camera.getEyePosition()[1], camera.getEyePosition()[2]);
        gl.glRotatef(camera.getPhiX(), 1.0f, 0.0f, 0.0f);
        gl.glRotatef(camera.getPhiY(), 0.0f, 1.0f, 0.0f);
        gl.glRotatef(camera.getPhiZ(), 0.0f, 0.0f, 1.0f);


        if (mouseclicked || mousemoved) {
            int[] viewport = new int[4];
            double[] modelview = new double[16];
            double[] projection = new double[16];

            gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
            gl.glGetDoublev(GL.GL_MODELVIEW_MATRIX, modelview, 0);
            gl.glGetDoublev(GL.GL_PROJECTION_MATRIX, projection, 0);

            /*
            System.out.println(viewport[0] + "|" + viewport[1] + "|" + viewport[2] + "|" + viewport[3]);
            System.out.println(modelview[0] + "|" + modelview[1] + "|" + modelview[2] + "|" + modelview[3]
            + "|" + modelview[4] + "|" + modelview[5] + "|" + modelview[6] + "|" + modelview[7]
            + "|" + modelview[8] + "|" + modelview[9] + "|" + modelview[10] + "|" + modelview[11]
            + "|" + modelview[12] + "|" + modelview[13] + "|" + modelview[14] + "|" + modelview[15]);
            System.out.println(projection[0] + "|" + projection[1] + "|" + projection[2] + "|" + projection[3]
            + "|" + projection[4] + "|" + projection[5] + "|" + projection[6] + "|" + projection[7]
            + "|" + projection[8] + "|" + projection[9] + "|" + projection[10] + "|" + projection[11]
            + "|" + projection[12] + "|" + projection[13] + "|" + projection[14] + "|" + projection[15]);
             */

            int x = mousex;
            int y = viewport[3] - mousey;

            FloatBuffer z = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asFloatBuffer();
            gl.glReadPixels(x, y, 1, 1, GL.GL_DEPTH_COMPONENT, GL.GL_FLOAT, z);
            z.rewind();

            double[] worldfar = new double[4];
            glu.gluUnProject((double) x, (double) y, (double) z.get(0), modelview, 0, projection, 0, viewport, 0, worldfar, 0);
            //System.out.println(x + "|" + y + "|" + (double) z.get(0) + ": " + worldfar[0] + "|" + worldfar[1] + "|" + worldfar[2]);

            double[] worldnear = new double[4];
            glu.gluUnProject((double) x, (double) y, 0.0, modelview, 0, projection, 0, viewport, 0, worldnear, 0);
            //System.out.println(x + "|" + y + "|" + 0.0 + ": " + worldnear[0] + "|" + worldnear[1] + "|" + worldnear[2]);

            /* ACHTUNG: Mathe! (Rayerzeugung)

            worldnear[0] + t * (worldfar[0] - worldnear[0]) = v * 1 + w * 0
            worldnear[1] + t * (worldfar[1] - worldnear[1]) = v * 0 + w * 0
            worldnear[2] + t * (worldfar[2] - worldnear[2]) = v * 0 + w * 1
            _______________________________________________________________

            t = -worldnear[1]/(worldfar[1] - worldnear[1])
            v = worldnear[0] + t * (worldfar[0] - worldnear[0])
            w = worldnear[2] + t * (worldfar[2] - worldnear[2])

             */

            double v = worldnear[0] + (-worldnear[1] / (worldfar[1] - worldnear[1])) * (worldfar[0] - worldnear[0]);
            double w = worldnear[2] + (-worldnear[1] / (worldfar[1] - worldnear[1])) * (worldfar[2] - worldnear[2]);

            //v und w anpassen
            v = (v + 8.0) / 2.0;
            w = (w + 8.0) / 2.0;

            //System.out.println((int)v + "|" + (int)w);

            if (mouseclicked) {
                Chess.getLogic().presentationInput((int) w, (int) v);
                mouseclicked = false;
            }

            if (mousemoved) {
                ChessFieldModel.getInstance().selectField((int) w, (int) v);
                mousemoved = false;
            }
        }

        //alle Elemente der Szene zeichnen
        for (ADrawable mydrawable : sceneParts) {
            mydrawable.draw();
        }

        //In 2D Zeichenmodus wechseln
        this.viewOrtho();

        //Schachmatt-Label zur gui-liste hinzufuegen
        if (status.getCheckmate()) {
            checkmate.draw();
        }
        //Schach-Label zur gui-Liste hinzufügen
        if (status.getCheck()) {
            check.draw();
        }

        //Remis-Label zur giu-Liste hinzufügen
        if (status.getRemis()) {
            remis.draw();
        }


        //Spieler selektieren
        if (Chess.getLogic().getOnTurnColor().equals("white")) {
            player1.setSelected(true);
            player2.setSelected(false);
        } else {
            player1.setSelected(false);
            player2.setSelected(true);
        }

        //alle Elemente der GUI zeichnen
        for (AGuiComponent button : gui) {
            button.draw();
        }

        //In 3D Zeichenmodus wechseln
        this.viewPerspective();
    }

    /**
     * Getter für die Kamera
     * @return Kamera
     */
    public Camera getCamera() {
        return this.camera;
    }

    /**
     * zu Testzwecken
     * Getter für das 0-te Licht
     * @return 0-te Licht
     */
    public Light getLight() {
        return this.lightManager.getLight();
    }

    /**
     * Getter Szenenteile
     * @return Szenenteile
     */
    public ArrayList<ADrawable> getSceneParts() {
        return sceneParts;
    }

    /**
     * Listener dieser Szene aktivieren
     */
    @Override
    public void setListener() {
        OpenGLContext.getInstance(null).getAutoDrawable().addKeyListener(this.keyListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseMotionListener(this.mouseListener);
    }

    /**
     * Listener dieser Szene deaktivieren
     */
    @Override
    public void unSetListener() {
        OpenGLContext.getInstance(null).getAutoDrawable().removeKeyListener(this.keyListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseMotionListener(this.mouseListener);
    }

    /**
     * Button-Highlight durch Maus wieder zuruecksetzen
     */
    @Override
    public void setNoMouseOver() {
        for (AGuiComponent component : gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;
                button.setMouseIsOver(false);
            }
        }
    }

    /**
     * Getter SchachLabel
     * @return SchachLabel
     */
    public Label getCheck() {
        return check;
    }

    /**
     * Getter SchachmattLabel
     * @return SchachmattLabel
     */
    public Label getCheckmate() {
        return checkmate;
    }

    /**
     * Getter RemisLabel
     * @return RemisLabel
     */
    public Label getRemis() {
        return remis;
    }

    /**
     * Getter Player1Label
     * @return Player1Label
     */
    public Label getPlayer1() {
        return player1;
    }

    /**
     * Getter Player2Label
     * @return Player2Label
     */
    public Label getPlayer2() {
        return player2;
    }
}