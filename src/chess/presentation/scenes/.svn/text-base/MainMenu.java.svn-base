package chess.presentation.scenes;

import chess.presentation.listener.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.*;
import javax.media.opengl.GL;

/**
 * Szene, in der das HauptMenue gezeichnet wird. 
 */
public class MainMenu extends AScene {

    /**
     * MausListener
     */
    private MainMenuMouseListener mouseListener;

    /**
     * Liste der Szenenteile, die GUI und Kamera werden initialisiert.
     */
    public MainMenu() {
        super();
        this.mouseListener = new MainMenuMouseListener(this);
        this.camera = new Camera(new float[]{2.0f, 1.0f, 14.0f}, new float[]{0.0f, 0.0f, 0.0f});
        this.SceneName = "Main";
        this.initScene();
    }

    /**
     * Szene initialisieren.
     * GUI wird erzeugt.
     */
    @Override
    public void initScene() {
        gl.glLoadIdentity();

        Label title = new Label(
                "Schach",
                new float[]{4.0f, 12.0f},
                2.0f,
                14.0f,
                1,
                2,
                26,
                false);

        gui.add(title);

        String[] actions = new String[]{"Neues Spiel", "Statistiken", "Optionen", "Beenden"};

        for (int i = 0; i < actions.length; i++) {
            Button b = new Button(
                    actions[i],
                    new float[]{4.0f, 12.0f},
                    2.0f,
                    14.0f,
                    1,
                    i + 4);
            gui.add(b);
        }

        this.setPerspective();

        gl.glMatrixMode(GL.GL_MODELVIEW);
    }

    /**
     * Perspektive setzen.
     */
    private void setPerspective() {
        gl.glMatrixMode(GL.GL_PROJECTION);

        gl.glLoadIdentity();

        glu.gluPerspective(75.0f, this.resolution.getX() / this.resolution.getY(), 0.1f, 500.0f);
    }

    /**
     * Szene anzeigen.
     */
    @Override
    public void displayScene() {
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();

        glu.gluLookAt(
                camera.getEyePosition()[0], camera.getEyePosition()[1], camera.getEyePosition()[2],
                camera.getCenterPosition()[0], camera.getCenterPosition()[1], camera.getCenterPosition()[2],
                0.0f, 1.0f, 0.0f);

        this.viewOrtho();

        //GUI zeichnen
        for (AGuiComponent button : gui) {
            button.draw();
        }

        this.viewPerspective();
    }

    /**
     * Listener dieser Szene aktivieren
     */
    @Override
    public void setListener() {
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseMotionListener(this.mouseListener);
    }

    /**
     * Listener dieser Szene deaktivieren
     */
    @Override
    public void unSetListener() {
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseMotionListener(this.mouseListener);
    }

    /**
     * Button-Highlight durch Maus wieder zuruecksetzen
     */
    @Override
    public void setNoMouseOver() {
        for (AGuiComponent component : gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;
                button.setMouseIsOver(false);
            }
        }
    }
}