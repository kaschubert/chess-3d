package chess.presentation.scenes;

import chess.presentation.utilities.*;
import com.sun.opengl.util.j2d.*;
import java.awt.*;
import java.util.*;
import javax.media.opengl.GL;

/**
 * LoadMenu.
 * Szene, in der das LoadMenue gezeichnet wird.
 */
public class LoadMenu extends AScene{

    /**
     *
     */
    private boolean once;

    /**
     *
     */
    private Thread loader;

    /**
     *
     */
    private float progress, modelprogress, progressfade, loadfade;

    /**
     *
     */
    private ArrayList<String> models, textures;

    /**
     * 
     */
    private int stopper;

    /**
     *
     */
    public LoadMenu(){
        super();

        models = new ArrayList<String>();
        textures = new ArrayList<String>();

        models.add("pawn");
        models.add("rook");
        models.add("bishop");
        models.add("queen");
        models.add("king");
        models.add("knight");
        
        textures.add("egypt_border");
        textures.add("egypt_border2");
        textures.add("egypt_tile_black");
        textures.add("egypt_tile_white");
        textures.add("egypt_tile_black_selected");
        textures.add("egypt_tile_white_selected");
        textures.add("egypt_tile_black_marked");
        textures.add("egypt_tile_white_marked");
        textures.add("egypt_tile_black_marked_selected");
        textures.add("egypt_tile_white_marked_selected");
        textures.add("egypt_tile_black_attack");
        textures.add("egypt_tile_white_attack");
        textures.add("egypt_tile_black_attack_selected");
        textures.add("egypt_tile_white_attack_selected");
        textures.add("egypt_corner");
        textures.add("egypt_mat_black");
        textures.add("egypt_mat_white");
        textures.add("knight_black");
        textures.add("knight_white");

        once = true;
        stopper = 50;
        
        loader = new Thread(new Loader(this, models));
        progress = modelprogress = progressfade = loadfade = 0.0f;

        camera = new Camera(new float[] {2.0f, 1.0f, 14.0f}, new float[] {0.0f, 0.0f, 0.0f});
        SceneName = "Load";

        gl.glLoadIdentity();
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(75.0f, this.resolution.getX() / this.resolution.getY(), 0.1f, 500.0f);
        gl.glMatrixMode(GL.GL_MODELVIEW);
    }

    /**
     * Szene anzeigen.
     */
    @Override
    public void displayScene() {
        if (once) {
            once = false;
            gl.glClearColor(Colors.white()[0], Colors.white()[1],  Colors.white()[2], Colors.white()[3]);

            for(String texture : textures)
                TextureManager.setTexture(TextureLoader.load(texture), texture);
        }

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();

        glu.gluLookAt(
                camera.getEyePosition()[0], camera.getEyePosition()[1], camera.getEyePosition()[2],
                camera.getCenterPosition()[0], camera.getCenterPosition()[1], camera.getCenterPosition()[2],
                0.0f, 1.0f, 0.05f
        );

        viewOrtho();
        
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glDisable(GL.GL_LIGHTING);

        progressfade = progress + (1.0f/(float)models.size()) * modelprogress;

        //Ladebalken
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f - loadfade, 1.0f - loadfade, 1.0f - loadfade);
        gl.glVertex3f(resolution.getX()/2.0f - 300.0f, resolution.getY()/10.0f + 1.0f, 0.0f);
        gl.glVertex3f(resolution.getX()/2.0f - 300.0f, resolution.getY()/10.0f - 1.0f, 0.0f);
        gl.glVertex3f(resolution.getX()/2.0f - 300.0f + progressfade*600.0f, resolution.getY()/10.0f - 1.0f, 0.0f);
        gl.glVertex3f(resolution.getX()/2.0f - 300.0f + progressfade*600.0f, resolution.getY()/10.0f + 1.0f, 0.0f);
        gl.glEnd();

        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE);

        if (stopper >= 0) {
            stopper--;

            loadfade += 0.025f;
            if (loadfade > 1.0f)
                loadfade = 1.0f;

        }
        gl.glColor4f(1.0f, 1.0f, 1.0f, loadfade);

        
        //Ladeprozent
        TextureRenderer textureRenderer = new TextureRenderer(30, 20, true);
        Graphics2D g2D = textureRenderer.createGraphics();
        g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, loadfade));
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //g2D.setColor(Color.WHITE);
        //g2D.fillRect(0, 0, 30, 20);
        g2D.setColor(Color.BLACK);
        g2D.setFont(new Font("Arial", Font.PLAIN, 11));
        g2D.drawString((int)(progressfade*100) + "%", 0, 15);

        textureRenderer.getTexture().bind();

        gl.glBegin(GL.GL_QUADS);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(resolution.getX()/2.0f - 15.0f, resolution.getY()/10.0f + 22.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(resolution.getX()/2.0f - 15.0f, resolution.getY()/10.0f + 2.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(resolution.getX()/2.0f + 15.0f, resolution.getY()/10.0f + 2.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(resolution.getX()/2.0f + 15.0f, resolution.getY()/10.0f + 22.0f, 0.0f);
        gl.glEnd();
        

        if (progress < 1.0f) {
            //Ladefigur weiß -> statisch
            TextureManager.knight_white().bind();

            gl.glBegin(GL.GL_QUADS);
            gl.glTexCoord2f(0.0f, 0.0f);
            gl.glVertex3f(resolution.getX()/2.0f + 64.0f, resolution.getY()/2.0f + 128.0f, 0.0f);

            gl.glTexCoord2f(0.0f, 1.0f);
            gl.glVertex3f(resolution.getX()/2.0f + 64.0f, resolution.getY()/2.0f - 128.0f, 0.0f);

            gl.glTexCoord2f(1.0f, 1.0f);
            gl.glVertex3f(resolution.getX()/2.0f - 64.0f, resolution.getY()/2.0f - 128.0f, 0.0f);

            gl.glTexCoord2f(1.0f, 0.0f);
            gl.glVertex3f(resolution.getX()/2.0f - 64.0f, resolution.getY()/2.0f + 128.0f, 0.0f);
            gl.glEnd();
        }

        if (stopper == 0) {
            loader.start();
            stopper = -50;
        }
        else if (stopper < 0) {
            //Ladefigur schwarz -> dynamisch
            TextureManager.knight_black().bind();

            gl.glBegin(GL.GL_QUADS);
            gl.glTexCoord2f(0.0f, 1.0f - progressfade);
            gl.glVertex3f(resolution.getX()/2.0f + 64.0f, resolution.getY()/2.0f - 128.0f + 256.0f*progressfade, 0.01f);

            gl.glTexCoord2f(0.0f, 1.0f);
            gl.glVertex3f(resolution.getX()/2.0f + 64.0f, resolution.getY()/2.0f - 128.0f, 0.01f);

            gl.glTexCoord2f(1.0f, 1.0f);
            gl.glVertex3f(resolution.getX()/2.0f - 64.0f, resolution.getY()/2.0f - 128.0f, 0.01f);

            gl.glTexCoord2f(1.0f, 1.0f - progressfade);
            gl.glVertex3f(resolution.getX()/2.0f - 64.0f, resolution.getY()/2.0f - 128.0f + 256.0f*progressfade, 0.01f);
            gl.glEnd();
        }

        gl.glDisable(GL.GL_TEXTURE_2D);
        gl.glEnable(GL.GL_LIGHTING);

        viewPerspective();

        if (progress >= 1.0f) {
            loadfade -= 0.025f;
            if (loadfade < 0.0f)
                loadfade = 0.0f;

            if (++stopper == 0)
                SceneManager.getInstance().goToScene("Main");
        }
    }

    /**
     * Uebergabe des Ladevortschritts.
     * @param progress Ladefortschritt
     */
    public void setProgress(float progress) {
        this.progress = progress;
    }

    /**
     * @param modelprogress
     */
    public void setModelProgress(float modelprogress) {
        this.modelprogress = modelprogress;
    }

    /**
     * Zur Abfrage des Laderstatuses.
     * @return true falls der Lader noch lauft, sonst false
     */
    public boolean isLoading() {
        return loader.isAlive();
    }
}