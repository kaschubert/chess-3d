package chess.presentation.scenes;

import chess.Chess;
import chess.persistance.Statistics;
import chess.presentation.listener.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.*;
import com.sun.opengl.util.j2d.TextureRenderer;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.util.Hashtable;
import javax.media.opengl.GL;

/**
 * Szene, in der das StatisticsMenue gezeichnet wird.
 */
public class StatisticsMenu extends AScene {

    /**
     * MausListener
     */
    private StatisticsMenuMouseListener mouseListener;
    private Hashtable<String, Boolean> possibleStats;
    private Hashtable<String, Statistics> statistics;

    /**
     * KeyListener nötig?
     */
    //private StatisticsMenuKeyListener keyListener;
    /**
     * Liste der Szenenteile, die GUI und Kamera werden initialisiert.
     */
    public StatisticsMenu() {
        super();
        this.mouseListener = new StatisticsMenuMouseListener(this);
        //this.keyListener = new StatisticsMenuKeyListener(this);
        this.camera = new Camera(new float[]{2.0f, 1.0f, 14.0f}, new float[]{0.0f, 0.0f, 0.0f});
        this.SceneName = "Statistics";
        this.initScene();
    }

    /**
     * Szene initialisieren.
     * GUI wird erzeugt.
     */
    @Override
    public void initScene() {
        possibleStats = Chess.getPersistance().getPossibleStats();
        statistics = Chess.getPersistance().getStatistics();

        //Example
        /*
        Chess.getPersistance().presentationInputStartgame("mmmmmmmmmmm", "wwwwwwwwwwww");
        Chess.getPersistance().logicInput("mmmmmmmmmmm", "Spieldauer");
        Chess.getPersistance().logicInput("wwwwwwwwwwww", "Anzahl Zuege", 200);
        Chess.getPersistance().presentationInputEndgame("mmmmmmmmmmm");
         */

        gl.glLoadIdentity();

        Button b1 = new Button(
                "Zurück",
                new float[]{32.0f, 32.0f},
                7.0f,
                14.0f,
                1,
                29);

        gui.add(b1);

        Label title = new Label(
                "Statistiken",
                new float[]{4.0f, 12.0f},
                2.0f,
                14.0f,
                1,
                2,
                26,
                false);

        gui.add(title);

        this.setPerspective();

        gl.glMatrixMode(GL.GL_MODELVIEW);
    }

    /**
     * Perspektive setzen.
     */
    private void setPerspective() {
        gl.glMatrixMode(GL.GL_PROJECTION);

        gl.glLoadIdentity();

        glu.gluPerspective(75.0f, this.resolution.getX() / this.resolution.getY(), 0.1f, 500.0f);
    }

    /**
     * Szene anzeigen.
     */
    @Override
    public void displayScene() {
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();

        glu.gluLookAt(
                camera.getEyePosition()[0], camera.getEyePosition()[1], camera.getEyePosition()[2],
                camera.getCenterPosition()[0], camera.getCenterPosition()[1], camera.getCenterPosition()[2],
                0.0f, 1.0f, 0.0f);

        this.viewOrtho();

        gl.glDisable(GL.GL_LIGHTING);
        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE);

        float resModY = 0.325f, resModX = 0.1f;

        for (String stat : possibleStats.keySet()) {
            if (!possibleStats.get(stat) && !stat.equals("momentane Siegesreihe") && !stat.equals("laengste Siegesreihe")) {
                TextureRenderer textureRendererStats = new TextureRenderer(160, 20, true);
                Graphics2D g2DStats = textureRendererStats.createGraphics();
                g2DStats.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
                g2DStats.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                //g2DStats.setColor(Color.WHITE);
                //g2DStats.fillRect(0, 0, 30, 20);
                g2DStats.setColor(Color.BLACK);
                g2DStats.setFont(new Font("Arial", Font.PLAIN, 11));
                g2DStats.drawString(stat, 0, 15);

                textureRendererStats.getTexture().bind();

                gl.glBegin(GL.GL_QUADS);
                gl.glTexCoord2f(0.0f, 0.0f);
                gl.glVertex3f(resolution.getX() * resModX - 80.0f, resolution.getY() - resolution.getY() * resModY + 20.0f, 0.0f);

                gl.glTexCoord2f(0.0f, 1.0f);
                gl.glVertex3f(resolution.getX() * resModX - 80.0f, resolution.getY() - resolution.getY() * resModY, 0.0f);

                gl.glTexCoord2f(1.0f, 1.0f);
                gl.glVertex3f(resolution.getX() * resModX + 80.0f, resolution.getY() - resolution.getY() * resModY, 0.0f);

                gl.glTexCoord2f(1.0f, 0.0f);
                gl.glVertex3f(resolution.getX() * resModX + 80.0f, resolution.getY() - resolution.getY() * resModY + 20.0f, 0.0f);
                gl.glEnd();

                resModY += 0.025f;
            }
        }

        resModY = 0.3f;
        resModX = 0.25f;

        for (String name : statistics.keySet()) {
            TextureRenderer textureRendererName = new TextureRenderer(100, 20, true);
            Graphics2D g2DName = textureRendererName.createGraphics();
            g2DName.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
            g2DName.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //g2DName.setColor(Color.WHITE);
            //g2DName.fillRect(0, 0, 30, 20);
            g2DName.setColor(Color.BLACK);
            g2DName.setFont(new Font("Arial", Font.PLAIN, 11));
            g2DName.drawString(name, 0, 15);

            textureRendererName.getTexture().bind();

            gl.glBegin(GL.GL_QUADS);
            gl.glTexCoord2f(0.0f, 0.0f);
            gl.glVertex3f(resolution.getX() * resModX - 50.0f, resolution.getY() - resolution.getY() * resModY + 20.0f, 0.0f);

            gl.glTexCoord2f(0.0f, 1.0f);
            gl.glVertex3f(resolution.getX() * resModX - 50.0f, resolution.getY() - resolution.getY() * resModY, 0.0f);

            gl.glTexCoord2f(1.0f, 1.0f);
            gl.glVertex3f(resolution.getX() * resModX + 50.0f, resolution.getY() - resolution.getY() * resModY, 0.0f);

            gl.glTexCoord2f(1.0f, 0.0f);
            gl.glVertex3f(resolution.getX() * resModX + 50.0f, resolution.getY() - resolution.getY() * resModY + 20.0f, 0.0f);
            gl.glEnd();

            resModY += 0.025f;
            for (String stat : possibleStats.keySet()) {
                if (!possibleStats.get(stat) && !stat.equals("momentane Siegesreihe") && !stat.equals("laengste Siegesreihe")) {
                    TextureRenderer textureRendererStats = new TextureRenderer(30, 20, true);
                    Graphics2D g2DStats = textureRendererStats.createGraphics();
                    g2DStats.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
                    g2DStats.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    //g2DStats.setColor(Color.WHITE);
                    //g2DStats.fillRect(0, 0, 30, 20);
                    g2DStats.setColor(Color.BLACK);
                    g2DStats.setFont(new Font("Arial", Font.PLAIN, 11));
                    g2DStats.drawString("" + statistics.get(name).getStat(stat), 0, 15);

                    textureRendererStats.getTexture().bind();

                    gl.glBegin(GL.GL_QUADS);
                    gl.glTexCoord2f(0.0f, 0.0f);
                    gl.glVertex3f(resolution.getX() * resModX - 15.0f, resolution.getY() - resolution.getY() * resModY + 20.0f, 0.0f);

                    gl.glTexCoord2f(0.0f, 1.0f);
                    gl.glVertex3f(resolution.getX() * resModX - 15.0f, resolution.getY() - resolution.getY() * resModY, 0.0f);

                    gl.glTexCoord2f(1.0f, 1.0f);
                    gl.glVertex3f(resolution.getX() * resModX + 15.0f, resolution.getY() - resolution.getY() * resModY, 0.0f);

                    gl.glTexCoord2f(1.0f, 0.0f);
                    gl.glVertex3f(resolution.getX() * resModX + 15.0f, resolution.getY() - resolution.getY() * resModY + 20.0f, 0.0f);
                    gl.glEnd();

                    resModY += 0.025f;
                }
            }
            resModY = 0.3f;
            resModX += 0.15f;
        }

        //GUI zeichnen
        for (AGuiComponent button : gui) {
            button.draw();
        }

        this.viewPerspective();
    }

    /**
     * Listener dieser Szene aktivieren
     */
    @Override
    public void setListener() {
        //OpenGLContext.getInstance(null).getAutoDrawable().addKeyListener(this.keyListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseMotionListener(this.mouseListener);
    }

    /**
     * Listener dieser Szene deaktivieren
     */
    @Override
    public void unSetListener() {
        //OpenGLContext.getInstance(null).getAutoDrawable().removeKeyListener(this.keyListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseMotionListener(this.mouseListener);
    }

    /**
     * Button-Highlight durch Maus wieder zuruecksetzen
     */
    @Override
    public void setNoMouseOver() {
        for (AGuiComponent component : gui) {
            if (component.getClass().getName().equals("chess.presentation.scenes.gui.Button")) {
                Button button = (Button) component;
                button.setMouseIsOver(false);
            }
        }
    }
}
