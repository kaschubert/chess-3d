package chess.presentation.scenes;

import chess.presentation.listener.WaitingMenuMouseListener;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.*;
import javax.media.opengl.GL;

/**
 * Szene, in der das WaitingMenue gezeichnet wird.
 */
public class WaitingMenu extends AScene {

    /**
     * MausListener
     */
    private WaitingMenuMouseListener mouseListener;

    /**
     * Liste der Szenenteile, die GUI und Kamera werden initialisiert.
     */
    public WaitingMenu() {
        super();
        mouseListener = new WaitingMenuMouseListener(this);
        camera = new Camera(new float[]{2.0f, 1.0f, 14.0f}, new float[]{0.0f, 0.0f, 0.0f});
        SceneName = "Waiting";
        initScene();
    }

    /**
     * Szene initialisieren.
     * GUI wird erzeugt.
     */
    @Override
    public void initScene() {
        gl.glLoadIdentity();

        Button b1 = new Button(
            "Zurück",
            new float[]{32.0f, 32.0f},
            7.0f,
            14.0f,
            1,
            29
        );

        Label text = new Label(
            "Warte auf Mitspieler...",
            new float[]{9.0f, 12.0f},
            2.6f,
            14.0f,
            3,
            8,
            26,
            true
        );
	
        LoadCircle circle = new LoadCircle(
            new float[]{4.0f,  1.5f},
            4.0f,
            3.0f,
            1,
            1
        );

        gui.add(b1);
        gui.add(circle);
        gui.add(text);

        this.setPerspective();
        
        gl.glMatrixMode(GL.GL_MODELVIEW);
    }

    /**
     * Perspektive setzen.
     */
    private void setPerspective() {
        gl.glViewport(0, 0, resolution.getX(), resolution.getY());
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, (float) resolution.getX() / (float) resolution.getY(), 0.1f, 500.0f);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Szene anzeigen.
     */
    @Override
    public void displayScene() {
        gl.glClearColor(Colors.white()[0],
                Colors.white()[1],
                Colors.white()[2],
                Colors.white()[3]);

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();

        glu.gluLookAt(
                camera.getEyePosition()[0], camera.getEyePosition()[1], camera.getEyePosition()[2],
                camera.getCenterPosition()[0], camera.getCenterPosition()[1], camera.getCenterPosition()[2],
                0.0f, 1.0f, 0.0f);

        this.viewOrtho();

        //GUI zeichnen
        for (AGuiComponent button : gui) {
            button.draw();
        }

        this.viewPerspective();
    }

    /**
     * Listener dieser Szene aktivieren
     */
    @Override
    public void setListener(){
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseMotionListener(this.mouseListener);
    }

    /**
     * Listener dieser Szene deaktivieren
     */
    @Override
    public void unSetListener(){
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseMotionListener(this.mouseListener);
    }

    /**
     * Button-Highlight durch Maus wieder zuruecksetzen
     */
    @Override
    public void setNoMouseOver(){
        for(AGuiComponent component: gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Button")){
                Button button = (Button) component;
                button.setMouseIsOver(false);
            }
        }
    }
}