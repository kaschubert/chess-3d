package chess.presentation.scenes.gui;

import chess.presentation.drawables.ADrawable;
import chess.presentation.utilities.Resolution;
import javax.media.opengl.GL;

/**
 * Alle GUI-Klassen erben von dieser Klasse.
 */
public abstract class AGuiComponent extends ADrawable{

    /**
     * Aufloesung x
     */
    protected int xResolution;

    /**
     * Aufloesung y
     */
    protected int yResolution;

    /**
     * Laenge x
     */
    protected int xLength;

    /**
     * Laenge y
     */
    protected int yLength;

    /**
     * Anteil der Laenge an der Aufloesung x
     */
    private float xPart;

    /**
     * Anteil der Laenge an der Aufloesung y
     */
    private float yPart;

    /**
     * Anteil der Position an der Aufloesung x,y,z
     */
    private float[] positionParts;

    /**
     * wird mit xPart multipliziert
     */
    private int xFactor;

    /**
     * wird mit yPart multipliziert
     */
    private int yFactor;

    /**
     * Berechung der Laenge und Position dynamisch zur Aufloesung
     * @param xPart
     * @param yPart
     * @param positionParts
     * @param xFactor
     * @param yFactor
     */
    public AGuiComponent(float xPart, float yPart, float[] positionParts, int xFactor, int yFactor){
        super();
        
        this.xPart = xPart;
        this.yPart = yPart;

        this.positionParts = positionParts;

        this.xFactor = xFactor;
        this.yFactor = yFactor;

        Resolution resolution = Resolution.getInstance();

        xResolution = resolution.getX();
        yResolution = resolution.getY();
        
        xLength = (int) (xResolution / xPart);
        yLength = (int) (yResolution / yPart);
        
        position[0] = (xResolution / positionParts[0]) * this.xFactor;
        position[1] = yResolution - ((yResolution / positionParts[1]) * this.yFactor);
    }

    /**
     * OGL-Operationen, die immer vor dem eigentlichen Zeichnen erfolgen sollen
     */
    @Override
    protected void beforeDraw() {
        gl.glPushMatrix();
        gl.glDisable(GL.GL_LIGHTING);
        // Texturierung einschalten
        gl.glEnable(GL.GL_TEXTURE_2D);

        // Ersetze die Farben des Quads mit der Textur
        gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE);
        
        gl.glTranslatef(this.position[0], this.position[1], 0.0f);
    }
    
    /**
     * Ueberpruefung der Mausposition
     * @param x Mausposition
     * @param y Mausposition
     * @return Maus ueber Komponente oder nicht
     */
    protected boolean checkMousePosition(int x, int y){
        return true;
    }

    /**
     * Komponenten an neue Aufloesung anpassen.
     */
    public void reshapeComponent(){
        if(positionParts != null){
            Resolution resolution = Resolution.getInstance();

            xResolution = resolution.getX();
            yResolution = resolution.getY();

            xLength = (int) (xResolution / xPart);
            yLength = (int) (yResolution / yPart);

            position[0] = (xResolution / positionParts[0]) * this.xFactor;
            position[1] = yResolution - ((yResolution / positionParts[1]) * this.yFactor);
        }
    }
}