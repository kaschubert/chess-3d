package chess.presentation.scenes.gui;

import javax.media.opengl.GL;

/**
 * Eine Ladeanimation. 
 */
public class LoadCircle extends AGuiComponent {

    /**
     * Rotation der Kette. 
     */
    float rotation = 0.0f;
    /**
     * Der Radius der Kreise.
     */
    float circleRadius = 8.0f;
    /**
     * Der Abstand zwischen zwei Kreisen.
     */
    float circleDistance = 35.0f;
    /**
     * Regelt die Drehgeschwindigkeit
     */
    float delta = 7.0f;
    /**
     * Anzahl Kreise.
     */
    int circleCount = 10;

    /**
     * Position und Laenge werden an Superklasse weiter gegeben
     * @param positionParts
     * @param xPart
     * @param yPart
     * @param xFactor
     * @param yFactor
     */
    public LoadCircle(float[] positionParts, float xPart, float yPart, int xFactor, int yFactor) {
        super(xPart, yPart, positionParts, xFactor, yFactor);
    }

    /**
     * LoadCircle wird gezeichnet. 
     */
    @Override
    protected void mainDraw() {
        gl.glDisable(GL.GL_TEXTURE_2D);
        int i;
        float colorValue = 1.0f;
        float[] colors = new float[3];

        // Abstand der Mittelpunkte der Kreise vom Ursprungspunkt
        float radius = 50.0f;

        // Kreise zeichnen
        for (i = 0; i < circleCount; i++) {
            float mX = radius * (float) Math.cos(gradToRad(rotation + i * circleDistance)) + position[0];
            float mY = radius * (float) Math.sin(gradToRad(rotation + i * circleDistance)) + position[1];

            colors[0] = colorValue;
            colors[1] = colorValue;
            colors[2] = colorValue;

            gl.glColor3fv(colors, 0);
            drawCircle(mX, mY, circleRadius);
            colorValue -= (1.0f / (float) circleCount);
        }
        rotation += delta;
        gl.glEnable(GL.GL_TEXTURE_2D);
    }

    /**
     * Zeichnet einen Kreis.
     * @param mX X-Koordinate des Mittelpunktes des Kreises.
     * @param mY Y-Koordinate des Mittelpunktes des Kreises.
     * @param radius Radius des Kreises.
     */
    private void drawCircle(float mX, float mY, float radius) {
        int i;
        int edges = 30;
        float angle = (float) (360.0 / edges);

        gl.glBegin(gl.GL_TRIANGLE_FAN);

        for (i = 0; i < edges; i++) {
            gl.glVertex3f(radius * (float) Math.cos(gradToRad(i * angle)) + mX, radius * (float) Math.sin(gradToRad(i * angle)) + mY, 0.0f);
        }
        gl.glEnd();
    }

    /**
     * Grad in Bogenmaß umrechnen
     * @param grad Wert in Grad.
     * @return Wert in RAD.
     */
    private float gradToRad(float grad) {
        return grad * (float) Math.PI / 180.0f;
    }
}
