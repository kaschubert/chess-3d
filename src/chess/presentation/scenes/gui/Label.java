package chess.presentation.scenes.gui;

import chess.presentation.utilities.TextureManager;
import com.sun.opengl.util.texture.Texture;
import javax.media.opengl.GL;

/**
 * Ein Label.
 */
public class Label extends AGuiComponent{

    /**
     * Text auf dem Label
     */
    private String text;

    /**
     * Textur des Textes. 
     */
    private Texture textTexture;

    /**
     * Groesse des Textes
     */
    private int textSize;

    /**
     * Textur wird farblich invertiert
     */
    private boolean inverse;

    /**
     * Objektvariablen werden initialisiert
     * @param text
     * @param positionParts
     * @param xPart
     * @param yPart
     * @param xFactor
     * @param yFactor
     * @param textSize
     * @param inverse
     */
    public Label(String text, float[] positionParts, float xPart, float yPart, int xFactor, int yFactor, int textSize, boolean inverse){
        super(xPart, yPart, positionParts, xFactor, yFactor);
        this.text = text;
        this.textSize = textSize;
        this.inverse = inverse;
        this.textTexture = TextureManager.
                generateMenuTextTexture(this.text, xLength, yLength, false, true, textSize, inverse);
    }

    /**
     * Setter Text
     * Generierung der Textur
     * @param text
     */
    public void setText(String text){
       this.text = text;
       this.textTexture = TextureManager.
                generateMenuTextTexture(this.text, xLength, yLength, false, true, textSize, inverse);
    }

    /**
     * Maus-Highlight
     * @param selected
     */
    public void setSelected(boolean selected){
        inverse = !selected;
        this.textTexture = TextureManager.
                generateMenuTextTexture(this.text, xLength, yLength, false, true, textSize, inverse);
    }

    /**
     * Label wird gezeichnet.
     */
    @Override
    protected void mainDraw() {
        this.textTexture.bind();

        //Button linke obere Ecke im Ursprung
        gl.glBegin(GL.GL_QUADS);
            gl.glTexCoord2f(0.0f, 0.0f);
            gl.glVertex3f(0.0f, 0.0f, 0.0f);

            gl.glTexCoord2f(0.0f, 1.0f);
            gl.glVertex3f(0.0f, -yLength, 0.0f);

            gl.glTexCoord2f(1.0f, 1.0f);
            gl.glVertex3f(xLength, -yLength, 0.0f);

            gl.glTexCoord2f(1.0f, 0.0f);
            gl.glVertex3f(xLength, 0.0f, 0.0f);
        gl.glEnd();
    }
}