package chess.presentation.scenes.gui;

import chess.presentation.utilities.TextureManager;
import com.sun.opengl.util.texture.Texture;
import javax.media.opengl.GL;

/**
 * Ein Button.
 */
public class Button extends AGuiComponent{

    /**
     * Aktion die ausgeloest wird
     */
    private String action;

    /**
     * Maus-Highlight
     */
    private boolean mouseIsOver;

    /**
     * Textur ohne Maus-Highlight
     */
    private Texture textTextureNormal;

    /**
     * Textur mit Maus-Highlight
     */
    private Texture textTextureMouseOver;
    
    /**
     * Objektvariablen werden initialisiert
     * @param action
     * @param positionParts
     * @param xPart
     * @param yPart
     * @param xFactor
     * @param yFactor
     */
    public Button(String action, float[] positionParts, float xPart, float yPart, int xFactor, int yFactor){
        super(xPart, yPart, positionParts, xFactor, yFactor);
        this.action = action;
        this.mouseIsOver = false;
        this.textTextureNormal = TextureManager.
                generateMenuTextTexture(this.action, xLength, yLength, false, true, 26, false);
        this.textTextureMouseOver = TextureManager.
                generateMenuTextTexture(this.action, xLength, yLength, true, true, 26, false);
    }
    
    /**
     * Button wird gezeichnet
     */
    @Override
    protected void mainDraw() {
        if(this.mouseIsOver)
            this.textTextureMouseOver.bind();
        else
            this.textTextureNormal.bind();

        //Button linke obere Ecke im Ursprung
        gl.glBegin(GL.GL_QUADS);
            gl.glTexCoord2f(0.0f, 0.0f);
            gl.glVertex3f(0.0f, 0.0f, 0.0f);

            gl.glTexCoord2f(0.0f, 1.0f);
            gl.glVertex3f(0.0f, -yLength, 0.0f);

            gl.glTexCoord2f(1.0f, 1.0f);
            gl.glVertex3f(xLength, -yLength, 0.0f);

            gl.glTexCoord2f(1.0f, 0.0f);
            gl.glVertex3f(xLength, 0.0f, 0.0f);
        gl.glEnd();
    }
    
    /**
     * Ueberpruefung der Mausposition
     * @param x Mausposition
     * @param y Mausposition
     * @return Maus ueber Komponente oder nicht
     */
    @Override
    public boolean checkMousePosition(int x, int y){
        boolean clicked = false;
        
        if((x > position[0]) && (x < (position[0] + xLength)) && (y < position[1]) && (y > (position[1] - yLength))){
            clicked = true;
        }
        
        return clicked;
    }

    /**
     * Getter action
     * @return action
     */
    public String getAction(){
        return this.action;
    }

    /**
     * Setter Maus-Highlight
     * @param mouseIsOver
     */
    public void setMouseIsOver(boolean mouseIsOver){

        this.mouseIsOver = mouseIsOver;
    }
}