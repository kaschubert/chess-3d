package chess.presentation.scenes.gui;

import chess.presentation.utilities.TextureManager;
import com.sun.opengl.util.texture.Texture;
import javax.media.opengl.GL;

/**
 * Ein Arrow.
 */
public class Arrow extends AGuiComponent{

    /**
     * Maus-Highlight
     */
    private boolean mouseIsOver;

    /**
     * Richtung des Pfeils
     */
    private String direction;

    /**
     * Textur mit Maus-Highlight
     */
    private Texture arrowTextureMouseOver;

    /**
     * Textur ohne Maus-Highlight
     */
    private Texture arrowTextureMouseOff;

    /**
     * Objektvariablen werden initialisiert
     * @param direction
     * @param positionParts
     * @param xPart
     * @param yPart
     * @param xFactor
     * @param yFactor
     */
    public Arrow(String direction, float[] positionParts, float xPart, float yPart, int xFactor, int yFactor){
        super(xPart, yPart, positionParts, xFactor, yFactor);
        this.direction = direction;
        this.mouseIsOver = false;
        this.arrowTextureMouseOver = TextureManager.generateArrowTexture(xLength, yLength, true, true);
        this.arrowTextureMouseOff = TextureManager.generateArrowTexture(xLength, yLength, false, true);
    }

    /**
     * Arrow wird gezeichnet
     */
    @Override
    protected void mainDraw() {
        if(direction.equals("up")){
            gl.glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
        }
        else if(direction.equals("right")){
            gl.glRotatef(-180.0f, 0.0f, 0.0f, 1.0f);
        }
        else if(direction.equals("down")){
            gl.glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
        }

        gl.glTranslatef(0.0f, -yLength/2, 0.0f);

        if(this.mouseIsOver)
            this.arrowTextureMouseOver.bind();
        else
            this.arrowTextureMouseOff.bind();

        gl.glBegin(GL.GL_QUADS);
            gl.glTexCoord2f(0.0f, 0.0f);
            gl.glVertex2i(0, 0);

            gl.glTexCoord2f(1.0f, 0.0f);
            gl.glVertex2i(this.xLength, 0);

            gl.glTexCoord2f(1.0f, 1.0f);
            gl.glVertex2i(this.xLength, this.yLength);

            gl.glTexCoord2f(0.0f, 1.0f);
            gl.glVertex2i(0, this.yLength);
        gl.glEnd();
   }

    /**
     * Ueberpruefung der Mausposition
     * @param x Mausposition
     * @param y Mausposition
     * @return Maus ueber Komponente oder nicht
     */
    @Override
    public boolean checkMousePosition(int x, int y){
        boolean clicked = false;

        if(direction.equals("left")){
            if(x >= this.position[0] && x <= this.position[0]+xLength){
                if(y >= this.position[1]-yLength/2 && y <= this.position[1]+yLength/2){
                    clicked = true;
                }
            }
        }
        else if(direction.equals("up")){
            if(x >= this.position[0]-yLength/2 && x <= this.position[0]+yLength/2){
                if(y >= this.position[1]-xLength && y <= this.position[1]){
                    clicked = true;
                }
            }
        }
        else if(direction.equals("right")){
            if(x >= this.position[0]-xLength && x <= this.position[0]){
                if(y >= this.position[1]-yLength/2 && y <= this.position[1]+yLength/2){
                    clicked = true;
                }
            }
        }
        else if(direction.equals("down")){
            if(x >= this.position[0]-yLength/2 && x <= this.position[0]+yLength/2){
                if(y >= this.position[1] && y <= this.position[1]+xLength){
                    clicked = true;
                }
            }
        }
        return clicked;
    }

    /**
     * Setter Maus-Highlight
     * @param mouseIsOver
     */
    public void setMouseIsOver(boolean mouseIsOver){
        this.mouseIsOver = mouseIsOver;
    }

    /**
     * Getter direction
     * @return direction
     */
    public String getDirection(){
        return this.direction;
    }
}