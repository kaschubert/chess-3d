package chess.presentation.scenes.gui;

import chess.presentation.utilities.*;
import com.sun.opengl.util.texture.Texture;
import javax.media.opengl.GL;

/**
 * Ein interaktives Textfeld. 
 */
public class Textfield extends AGuiComponent{

    /**
     * Defaulttext
     */
    private String startText;

    /**
     * Textur des aktuellen Textes
     */
    private Texture textTexture;

    /**
     * aktueller Text
     */
    private StringBuffer inputText;

    /**
     * Maus-Highlight
     */
    private boolean mouseIsOver;

    /**
     * neue Eingaben
     */
    private boolean newInput;

    /**
     * Textfeld zur Texteingabe aktiviert
     */
    private boolean inputActive;

    /**
     * Speicherung der Spielernamen
     */
    private Players players;

    /**
     * Objektvariablen werden initialisiert
     * @param text
     * @param positionParts
     * @param xPart
     * @param yPart
     * @param xFactor
     * @param yFactor
     * @param textSize
     */
    public Textfield(String text, float[] positionParts, float xPart, float yPart, int xFactor, int yFactor, int textSize){
        super(xPart, yPart, positionParts, xFactor, yFactor);
        this.startText = text;
        this.players = Players.getInstance();
        this.mouseIsOver = false;
        this.newInput = false;
        this.inputActive = false;
        this.inputText = new StringBuffer();
        this.textTexture = TextureManager.
                generateMenuTextTexture(this.startText, xLength, yLength, false, true, textSize, false);
    }
    
    /**
     * Textfield wird gezeichnet.
     */
    @Override
    protected void mainDraw() {
        //Standard Text mit grauem Hintergrund
        if(this.mouseIsOver && !this.newInput){
            this.textTexture = TextureManager.
                generateMenuTextTexture(this.startText, xLength, yLength, true, true, 26, false);
        }
        //Standard Text mit schwarzem Hintergrund
        else if(!this.mouseIsOver && !this.newInput){
            this.textTexture = TextureManager.
                generateMenuTextTexture(this.startText, xLength, yLength, false, true, 26, false);
        }
        //eingegebener Text mit grauem Hintergrund
        else if(this.mouseIsOver && this.newInput){
            this.textTexture = TextureManager.
                generateMenuTextTexture(this.inputText.toString(), xLength, yLength, true, true, 26, false);
        }
        //eingegebener Text mit schwarzem Hintergrund
        else if(!this.mouseIsOver && this.newInput){
            this.textTexture = TextureManager.
                generateMenuTextTexture(this.inputText.toString(), xLength, yLength, false, true, 26, false);
        }

        this.textTexture.bind();
        
        //Button linke obere Ecke im Ursprung
        gl.glBegin(GL.GL_QUADS);
            gl.glTexCoord2f(0.0f, 0.0f);
            gl.glVertex3f(0.0f, 0.0f, 0.0f);

            gl.glTexCoord2f(0.0f, 1.0f);
            gl.glVertex3f(0.0f, -yLength, 0.0f);

            gl.glTexCoord2f(1.0f, 1.0f);
            gl.glVertex3f(xLength, -yLength, 0.0f);

            gl.glTexCoord2f(1.0f, 0.0f);
            gl.glVertex3f(xLength, 0.0f, 0.0f);
        gl.glEnd();
    }

    /**
     * Ueberpruefung der Mausposition
     * @param x Mausposition
     * @param y Mausposition
     * @return Maus ueber Komponente oder nicht
     */
    @Override
    public boolean checkMousePosition(int x, int y){
        boolean clicked = false;

        if((x > position[0]) && (x < (position[0] + xLength)) && (y < position[1]) && (y > (position[1] - yLength))){
            clicked = true;
        }

        return clicked;
    }

    /**
     * Setter Maus-Highlight
     * @param mouseIsOver
     */
    public void setMouseIsOver(boolean mouseIsOver){

        this.mouseIsOver = mouseIsOver;
    }

    /**
     * neues Zeichen eingeben
     * @param input neues Zeichen
     */
    public void setInputText(char input){
        this.inputText.append(input);
        
        if(startText.equals("Spieler 1")){
            players.setPlayer1(inputText.toString());
        }
        else if(startText.equals("Spieler 2")){
            players.setPlayer2(inputText.toString());
        }
        else if(startText.equals("ServerIP")){
            players.setIp(inputText.toString());
        }
        this.newInput = true;
    }

    /**
     * letztes eingegebenes Zeichen loeschen
     */
    public void deleteLastInput(){
        if(inputText.length() > 0){
            this.inputText.deleteCharAt(inputText.length()-1);

            if(startText.equals("Spieler 1")){
                players.setPlayer1(inputText.toString());
            }
            else if(startText.equals("Spieler 2")){
                players.setPlayer2(inputText.toString());
            }
            else if(startText.equals("ServerIP")){
                players.setIp(inputText.toString());
            }
        }

    }

    /**
     * Getter Laenge des eingegebenen Textes
     * @return Laenge
     */
    public int getUserInputTextLength(){
        return this.inputText.length();
    }

    /**
     * Setter Eingabe aktiv
     * @param activ
     */
    public void setInputActive(boolean activ){
       this.inputActive = activ;
    }

    /**
     * Getter Eingabe aktiv
     * @return aktiv
     */
    public boolean getInputActive(){
       return this.inputActive;
    }

    /**
     * Getter xLength
     * @return xLength
     */
    public float getxLength(){
        return this.xLength;
    }

    /**
     * Getter yLength
     * @return yLength
     */
    public float getyLength(){
        return this.yLength;
    }

    /**
     * Getter StartText
     * @return StartText
     */
    public String getStartText() {
        return startText;
    }
}