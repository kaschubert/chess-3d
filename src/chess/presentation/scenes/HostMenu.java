package chess.presentation.scenes;

import chess.presentation.listener.*;
import chess.presentation.scenes.gui.*;
import chess.presentation.utilities.*;
import javax.media.opengl.GL;

/**
 * Szene, in der das HostMenu gezeichnet wird.
 */
public class HostMenu extends AScene{

    /**
     * MausListener
     */
    private HostMenuMouseListener mouseListener;

    /**
     * KeyListener
     */
    private HostMenuKeyListener keyListener;

    /**
     * Liste der Szenenteile, die GUI und Kamera werden initialisiert.
     */
    public HostMenu(){
        super();
        mouseListener = new HostMenuMouseListener(this);
        keyListener = new HostMenuKeyListener(this);
        camera = new Camera(new float[] {2.0f, 1.0f, 14.0f}, new float[] {0.0f, 0.0f, 0.0f});
        SceneName = "Host";
        initScene();
    }

    /**
     * Szene initialisieren.
     * GUI wird erzeugt.
     */
    @Override
    public void initScene() {
        gl.glLoadIdentity();

        Button b1 = new Button(
            "Zurück",
            new float[]{32.0f, 32.0f},
            7.0f,
            14.0f,
            1,
            29
        );

        gui.add(b1);

        Label title = new Label(
            "Server starten",
            new float[]{4.0f, 12.0f},
            2.0f,
            14.0f,
            1,
            2,
            26,
            false
        );

        gui.add(title);

        Textfield player1 = new Textfield(
            "Spieler 1",
            new float[]{4.0f, 12.0f},
            2.0f,
            14.0f,
            1,
            4,
            26
        );

        gui.add(player1);

        Button start = new Button(
            "Starten",
            new float[]{4.0f, 12.0f},
            2.0f,
            14.0f,
            1,
            5
        );
        gui.add(start);

        this.setPerspective();

        gl.glMatrixMode(GL.GL_MODELVIEW);
    }

    /**
     * Perspektive setzen.
     * Submethode zur Spezifikation der Perspektive.
     */
    private void setPerspective() {
        gl.glViewport(0, 0, resolution.getX(), resolution.getY());
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, (float) resolution.getX() / (float) resolution.getY(), 0.1f, 500.0f);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Szene anzeigen.
     */
    @Override
    public void displayScene() {
        gl.glClearColor(Colors.white()[0],
                    Colors.white()[1],
                    Colors.white()[2],
                    Colors.white()[3]
        );

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();

        glu.gluLookAt(
                camera.getEyePosition()[0], camera.getEyePosition()[1], camera.getEyePosition()[2],
                camera.getCenterPosition()[0], camera.getCenterPosition()[1], camera.getCenterPosition()[2],
                0.0f, 1.0f, 0.0f
        );

        this.viewOrtho();

        //GUI zeichnen
        for(AGuiComponent button: gui) {
            button.draw();
        }

        this.viewPerspective();
    }

    /**
     * Listener dieser Szene aktivieren
     */
    @Override
    public void setListener(){
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addMouseMotionListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().addKeyListener(this.keyListener);
    }

    /**
     * Listener dieser Szene deaktivieren
     */
    @Override
    public void unSetListener(){
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeMouseMotionListener(this.mouseListener);
        OpenGLContext.getInstance(null).getAutoDrawable().removeKeyListener(this.keyListener);
    }

    /**
     * Button-Highlight durch Maus wieder zuruecksetzen
     */
    @Override
    public void setNoMouseOver(){
        for(AGuiComponent component: gui) {
            if(component.getClass().getName().equals("chess.presentation.scenes.gui.Button")){
                Button button = (Button) component;
                button.setMouseIsOver(false);
            }
        }
    }
}