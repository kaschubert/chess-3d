package chess.presentation.scenes;

import chess.presentation.drawables.ADrawable;
import chess.presentation.scenes.gui.AGuiComponent;
import chess.presentation.utilities.*;
import java.util.ArrayList;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 * Alle Szenen-Klassen erben von dieser Klasse.
 */
public abstract class AScene {

    /**
     * Objekt für OGL Funktionen
     */
    protected GL gl;

    /**
     * Objekt für OGL-Utilities Funktionen
     */
    protected GLU glu;

    /**
     * Aufloesung
     */
    protected Resolution resolution;

    /**
     * Liste der Szenenteile
     */
    protected ArrayList<ADrawable> sceneParts;

    /**
     * Liste der GUI-Komponenten
     */
    protected ArrayList<AGuiComponent> gui;

    /**
     * Beleuchtungsmanager
     */
    protected LightManager lightManager;

    /**
     * Kamera
     */
    protected Camera camera;

    /**
     * Materialverwaltungsobjekt
     */
    protected Materials materials;

    /**
     * Hintergrundfarbe
     */
    protected float[] backgroundColor;

    /**
     * Szenenname
     */
    protected String SceneName;

    /**
     * Mauspositionen
     */
    protected int mousex, mousey;

    /**
     * Zustaende: Maus geklickt, bewegt
     */
    protected boolean mouseclicked, mousemoved;

    /**
     * OpenGLKontext wird genutzt, um Objektvariablen zu initialisieren
     * die Aufloesung wird ueber den Singleton Resolution ausgelesen
     */
    public AScene(){
        this.gl = OpenGLContext.getInstance(null).getGL();
        this.glu = OpenGLContext.getInstance(null).getGLU();
        //0,0 sobald eine Instanz erzeugt wurde
        this.resolution = Resolution.getInstance();
        this.sceneParts = new ArrayList<ADrawable>();
        this.gui = new ArrayList<AGuiComponent>();
        this.materials = new Materials();
        this.lightManager = new LightManager();

        mousex = mousey = 0;
        mouseclicked = mousemoved = false;
    }

    /**
     * Macht es moeglich aus dem 3D Zeichenmodus in den 2D Zeichenmodus
     * zu wechseln
     */
    public void viewOrtho(){
        gl.glMatrixMode(GL.GL_PROJECTION);  
        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glOrtho(0, this.resolution.getX(), 0, this.resolution.getY(), -1, 1);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();
        gl.glLoadIdentity();
    }

    /**
     * Macht es moeglich aus dem 2D Zeichenmodus in den 3D Zeichenmodus
     * zu wechseln
     */
    public void viewPerspective(){
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPopMatrix();
    }
    
    /**
     * Getter fuer die GUI
     * Wird von den Listenern benutzt
     * @return GUI
     */
    public ArrayList<AGuiComponent> getGUI(){
       return this.gui;
    }

    /**
     * Getter fuer den Szenennamen
     * @return
     */
    public String getSceneName(){
        return this.SceneName;
    }

    /**
     * Initialisierung der Szene
     * Wird von erbenden Klassen ueberschrieben
     */
    public void initScene(){};

    /**
     * Szene zeichnen
     * Wird von erbenden Klassen ueberschrieben
     */
    public void displayScene(){};

    /**
     * Listener dieser Szene aktivieren
     */
    public void setListener(){};

    /**
     * Listener dieser Szene deaktivieren
     */
    public void unSetListener(){};

    /**
     * Button-Highlight durch Maus wieder zuruecksetzen
     */
    public void setNoMouseOver(){};

    /**
     * Position eines Mausklicks speichern
     * @param x Mausposition
     * @param y Mausposition
     */
    public void giveMousePosClick(int x, int y) {
        mouseclicked = true;
        mousex = x;
        mousey = y;
    }

    /**
     * Position einer Mausbewegung speichern
     * @param x Mausposition
     * @param y Mausposition
     */
    public void giveMousePosMove(int x, int y) {
        mousemoved = true;
        mousex = x;
        mousey = y;
    }

    /**
     * Alle Elemente der Szene an neue Aufloesung anpassen. 
     */
    public void reshapeScene(){
        for(AGuiComponent guiElement: this.gui) {
            guiElement.reshapeComponent();
        }
        if(SceneName.equals("Game")){
            GameScene game = (GameScene) this;
            game.getCheck().reshapeComponent();
            game.getCheckmate().reshapeComponent();
            game.getRemis().reshapeComponent();
            game.getResign().reshapeComponent();
        }
    }
}