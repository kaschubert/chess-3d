package chess.presentation.drawables;

import chess.presentation.utilities.Material;
import javax.media.opengl.GL;

/**
 * Fügt der ADrawable ein Material hinzu.
 */
public abstract class ADrawableMaterial extends ADrawable{

    /**
     * Material des Objektes
     */
    protected Material material;

    /**
     * gewünschtes Material wird in der Klasse gespeichert
     * @param material
     */
    public ADrawableMaterial(Material material) {
        super();
        this.material = material;
    }

    /**
     * um das gewünschte Material garantiert zu setzen
     * wird diese Methode spezialisiert
     */
    @Override
    protected void beforeDraw() {
        gl.glPushMatrix();

        gl.glDisable(GL.GL_TEXTURE_2D);

        material.setThisMaterial();

        //Primitive/Lighting Color nicht mehr mit Texturfarben multiplizieren
        gl.glTexEnvf(gl.GL_TEXTURE_ENV, gl.GL_TEXTURE_ENV_MODE, gl.GL_REPLACE);

        gl.glTranslatef(this.position[0], this.position[1], this.position[2]);
    }
}