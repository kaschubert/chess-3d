package chess.presentation.drawables.figures;

import chess.presentation.drawables.ADrawableTexture;
import chess.presentation.utilities.Material;

/**
 * Alle Spielfiguren erben von dieser Klasse.
 */
public abstract class AFigure extends ADrawableTexture{

    /**
     * Zwischenklasse für mögliche Spezialisierungen der Figuren
     * bisher faktisch nicht genutzt
     * @param material
     */
    public AFigure(Material material){
        super(material);
    }
}