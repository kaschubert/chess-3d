package chess.presentation.drawables.figures;

import chess.presentation.utilities.*;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

/**
 * TestFigure.
 * Mensch-ägere-dich nicht Testfigur.
 */
public class TestFigure extends AFigure {

    /**
     * Konfiguration der GLU-Primitiven
     */
    GLUquadric qobj;

    /**
     * gluQuadric erzeugen und konfigurieren
     * @param material
     */
    public TestFigure(Material material) {
        super(material);
        qobj = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(qobj, GLU.GLU_FILL);
        glu.gluQuadricNormals(qobj, GLU.GLU_SMOOTH);
    }

    /**
     * Testfigur wird aus glu-Primitiven zusammengesetzt
     */
    @Override
    protected void mainDraw() {


        gl.glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

        glu.gluCylinder(qobj, 0.4, 0.4, 0.2, 30, 30);

        gl.glTranslatef(0.0f, 0.0f, 0.2f);

        glu.gluCylinder(qobj, 0.4f, 0.1f, 1.2f, 30, 1);

        gl.glTranslatef(0.0f, 0.0f, 1.45f);

        glu.gluSphere(qobj, 0.36f, 30, 30);

    }
}