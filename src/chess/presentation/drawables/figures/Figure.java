package chess.presentation.drawables.figures;

import chess.presentation.utilities.*;
import javax.media.opengl.GL;

/**
 * Spielfigur
 */
public class Figure extends AFigure {

    /**
     * Farbe der Figur
     */
    private String color;

    /**
     * 3D-Model der Figur
     */
    private Model model;


    /**
     * Erstellt eine neue Figure
     * @param figureType Art der Figur, nach der das entsprechende 3D-Modell ausgewählt wird
     * @param material Material der Figur
     * @param color Farbe der Figur
     */
    public Figure(String figureType, Material material, String color) {
        super(material);
        this.color = color;

        if (figureType.equals("pawn")) {
            model = ModelManager.pawnModel();
        } else if (figureType.equals("bishop")) {
            model = ModelManager.bishopModel();
        } else if (figureType.equals("rook")) {
            model = ModelManager.rookModel();
        } else if (figureType.equals("king")) {
            model = ModelManager.kingModel();
        } else if (figureType.equals("queen")) {
            model = ModelManager.queenModel();
        } else if (figureType.equals("knight")) {
            model = ModelManager.knightModel();
        } else {
            throw new RuntimeException("Figure " + figureType + " doesn't exist");
        }
    }

    /**
     * Zeichnen der Figur findet statt. 
     */
    @Override
    protected void mainDraw() {

        if (color.equals("white")) {
            TextureManager.egypt_mat_white().bind();
        } else if (color.equals("black")) {
            TextureManager.egypt_mat_black().bind();
        }

        if (model.equals(ModelManager.knightModel())) {

            if (color.equals("white"))
                gl.glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);

            else if (color.equals("black"))
                gl.glRotatef(90.0f, 0.0f, 1.0f, 0.0f);

        }

        model.pushToVertexArray();

        gl.glDrawArrays(
                GL.GL_TRIANGLES,
                0,
                model.getEndIndex()
                
        );
    }
}