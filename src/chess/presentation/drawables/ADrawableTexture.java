package chess.presentation.drawables;

import com.sun.opengl.util.texture.*;
import chess.presentation.utilities.*;
import javax.media.opengl.GL;

/**
 * Fügt der ADrawable ein Material und eine Textur hinzu.
 */
public abstract class ADrawableTexture extends ADrawable {

    /**
     * Textur des Objektes
     */
    protected Texture texture;

    /**
     * Material des Objektes
     */
    protected Material material;


    /**
     * Erzeugt eine neue ADrawableTexture.
     * @param material
     */
    public ADrawableTexture(Material material) {
        super();
        this.gl = OpenGLContext.getInstance(null).getGL();
        this.material = material;
    }

    /**
     * OGL-Operationen, die immer vor dem eigentlichen Zeichnen erfolgen sollen.
     */
    @Override
    protected void beforeDraw() {
        gl.glPushMatrix();

        gl.glTranslatef(this.position[0], this.position[1], this.position[2]);
        
        // Wichtig: Material muss gesetzt werden, um Beleuchtungseffekte etc. zu realisieren!
        material.setThisMaterial();

        // Multipliziere die Texturfarben mit der Primitive/Lighting Color
        gl.glTexEnvf(gl.GL_TEXTURE_ENV, gl.GL_TEXTURE_ENV_MODE, gl.GL_MODULATE);

        // Setze Specular Highlighting nach dem Texture Mapping
        gl.glLightModelf(gl.GL_LIGHT_MODEL_COLOR_CONTROL, gl.GL_SEPARATE_SPECULAR_COLOR);

        
        // Texturierung einschalten
        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glEnable(GL.GL_LIGHTING);
    }
}