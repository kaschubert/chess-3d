package chess.presentation.drawables;

import chess.presentation.utilities.OpenGLContext;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 * ADrawable.
 * Alle zeichnungsfähigen Klassen erben von dieser Klasse
 * Sie übernimmt OGL-Konfigurationen, die für alle o.g. Klassen
 * notwendig sind.
 */
public abstract class ADrawable {

    /**
     * Objekt für OGL Funktionen
     */
    protected GL gl;

    /**
     * Objekt für OGL-Utilities Funktionen
     */
    protected GLU glu;

    /**
     * Position des Objektes
     */
    protected float[] position;

    /**
     * Erzeugt eine neue ADrawable. 
     * OpenGLKontext wird genutzt, um Objektvariablen zu initialisieren
     */
    public ADrawable() {
        position = new float[3];
        gl = OpenGLContext.getInstance(null).getGL();
        glu = OpenGLContext.getInstance(null).getGLU();
    }

    /**
     * OGL-Operationen, die immer vor dem eigentlichen Zeichnen erfolgen sollen. 
     */
    protected void beforeDraw() {
        gl.glPushMatrix();
        gl.glTranslatef(this.position[0], this.position[1], this.position[2]);
    }

    /**
     * von erbenden Klassen zu überschreibende Methode,
     * in der das Zeichnen stattfinden soll
     */
    protected abstract void mainDraw();

    /**
     * OGL-Operationen, die immer nach dem eigentlichen Zeichnen erfolgen sollen
     */
    protected void afterDraw() {
        gl.glPopMatrix();
    }

    /**
     * Alle drei Teilmethoden des Zeichenprozesses werden
     * in der richtigen Reihenfolge aufgerufen
     */
    public void draw() {
        this.beforeDraw();
        this.mainDraw();
        this.afterDraw();
    }

    /**
     * Setter für die dreidimensionale Position der zu zeichnenden Figur
     * @param getPosition
     */
    public void setPosition(float[] position) {
        this.position = position;
    }

    /**
     * Getter für die dreidimensionale Position der zu zeichnenden Figur
     * @return
     */
    public float[] getPosition() {
        return this.position;
    }
}