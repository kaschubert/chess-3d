package chess.presentation.drawables;

import chess.presentation.utilities.Colors;
import javax.media.opengl.GL;

/**
 * CoordinatePlane.
 * 3D Koordinatenkreuz wird gezeichnet.
 */
public class CoordinatePlane extends ADrawable {

    /**
     * Laenge in x Richtung
     */
    private int xLength;

    /**
     * Laenge in y Richtung
     */
    private int yLength;

    /**
     * Laenge in z Richtung
     */
    private int zLength;

    /**
     * Laenge der drei Achsen wird initialisiert
     * @param xLength
     * @param yLength
     * @param zLength
     */
    public CoordinatePlane(int xLength, int yLength, int zLength) {
        super();
        this.xLength = xLength;
        this.yLength = yLength;
        this.zLength = zLength;
    }

    /**
     * Beleuchtung wird ausgeschaltet.
     * Jede Achse wird gezeichnet und Querstriche kennzeichen die Längeneinheiten
     */
    @Override
    protected void mainDraw() {
        gl.glDisable(GL.GL_LIGHTING);
        
        gl.glLineWidth(2.0f);

	gl.glBegin(gl.GL_LINES);
            gl.glColor4fv(Colors.blue(), 0);
            //x-Axis
            gl.glVertex3f(0, 0, 0);
            gl.glVertex3f(xLength, 0, 0);
            //bars on x-Axis
            for(int i = 1; i <= xLength; i++)
            {
                gl.glVertex3f(i, 0, -0.3f);
                gl.glVertex3f(i, 0, 0.3f);
            }
            //y-Axis
            gl.glColor4fv(Colors.red(), 0);
            gl.glVertex3f(0, 0, 0); 
            gl.glVertex3f(0, yLength, 0);
            //bars on y-Axis
            for(int i = 1; i <= yLength; i++)
            {
                gl.glVertex3f(-0.3f, i, 0);
                gl.glVertex3f(0.3f, i, 0);
            }
            //z-Axis
            gl.glColor4fv(Colors.green(), 0);
            gl.glVertex3f(0, 0, 0);
            gl.glVertex3f(0, 0, zLength);
            //bars on z-Axis
            for(int i = 1; i <= zLength; i++)
            {
                gl.glVertex3f(-0.3f, 0, i);
                gl.glVertex3f(0.3f, 0, i);
            }
	gl.glEnd();

        gl.glColor4fv(Colors.white(), 0);
        gl.glEnable(GL.GL_LIGHTING);
    }
}