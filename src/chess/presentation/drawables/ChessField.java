package chess.presentation.drawables;

import chess.presentation.ChessFieldModel;
import chess.presentation.drawables.figures.AFigure;

import chess.presentation.utilities.*;
import javax.media.opengl.GL;

/**
 * ChessField.
 * Es wird ein Schachfeld mit 8*8 Feldern gezeichnet.
 */
public class ChessField extends ADrawableTexture {

    /**
     * Laenge des Schachfeldes in x Richtung
     */
    private float xLength;

    /**
     * Laenge des Schachfeldes in y Richtung
     */
    private float zLength;

    /**
     * Figuren auf dem Schachfeld
     */
    private AFigure[][] figures;

    /**
     * Aufstellung der Figuren
     */
    private ChessFieldModel model;

    /**
     * Seitenlängen des Feldes und Texturen werden initialisiert
     * @param xLength
     * @param zLength
     */
    public ChessField(float xLength, float zLength, Material material) {
        super(material);
        this.xLength = xLength;
        this.zLength = zLength;
        this.model = ChessFieldModel.getInstance();
        this.figures = model.getFigures();
    }

    /**
     * das Schachfeld wird gezeichnet
     * alle Felder werden einzeln texturiert und gezeichnet
     * ein Rand wird um des Feld gezogen
     */
    @Override
    protected void mainDraw() {
        gl.glTranslatef((-4.0f * xLength), 0.0f, (-4.0f * zLength));
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {

                //&1 Bitweises UND zum modulo-Rechnen
                if ((i & 1) != 0 && (j & 1) != 0 || (i & 1) == 0 && (j & 1) == 0) {
                    if (model.isMarkedField(j, i) && model.isSelectedField(j, i)) {
                        if (model.isFieldEmpty(j, i)) {
                           //TextureManager.tile_black_marked_selected().bind();
                            TextureManager.egypt_tile_black_marked_selected().bind();
                        } else {
                            //TextureManager.tile_black_attack_selected().bind();
                            TextureManager.egypt_tile_black_attack_selected().bind();
                        }
                    }
                    else if (model.isMarkedField(j, i)) {
                        if (model.isFieldEmpty(j, i)) {
                            //TextureManager.tile_black_marked().bind();
                            TextureManager.egypt_tile_black_marked().bind();
                        } else {
                            //TextureManager.tile_black_attack().bind();
                            TextureManager.egypt_tile_black_attack().bind();
                        }
                    } 
                    else if (model.isSelectedField(j, i)) {
                        //TextureManager.tile_black_selected().bind();
                        TextureManager.egypt_tile_black_selected().bind();
                    }
                    else {
                        //TextureManager.tile_black().bind();   // Textur: schwarze Kachel
                        TextureManager.egypt_tile_black().bind();
                    }
                } else if ((i & 1) == 0 && (j & 1) != 0 || (i & 1) != 0 && (j & 1) == 0) {
                    if (model.isMarkedField(j, i) && model.isSelectedField(j, i)) {
                        if (model.isFieldEmpty(j, i)) {
                           //TextureManager.tile_white_marked_selected().bind();
                            TextureManager.egypt_tile_white_marked_selected().bind();
                        } else {
                            //TextureManager.tile_white_attack_selected().bind();
                            TextureManager.egypt_tile_white_attack_selected().bind();
                        }
                    }
                    else if (model.isMarkedField(j, i)) {
                        if (model.isFieldEmpty(j, i)) {
                            //TextureManager.tile_white_marked().bind();
                            TextureManager.egypt_tile_white_marked().bind();
                        } else {
                            //TextureManager.tile_white_attack().bind();
                            TextureManager.egypt_tile_white_attack().bind();
                        }
                    } 
                    else if (model.isSelectedField(j, i)) {
                        //TextureManager.tile_white_selected().bind();
                        TextureManager.egypt_tile_white_selected().bind();
                    }
                    else {
                        //TextureManager.tile_white().bind(); // Textur: weisse Kachel
                        TextureManager.egypt_tile_white().bind();
                    }
                }


                // Kachel zeichnen
                gl.glBegin(GL.GL_QUADS);
                //Normale
                gl.glNormal3f(0.0f, 1.0f, 0.0f);

                gl.glTexCoord2f(0.0f, 0.0f);
                gl.glVertex3f(i * xLength, 0.0f, j * zLength);

                gl.glTexCoord2f(1.0f, 0.0f);
                gl.glVertex3f(i * xLength, 0.0f, zLength + (j * zLength));

                gl.glTexCoord2f(1.0f, 1.0f);
                gl.glVertex3f(xLength + (i * xLength), 0.0f, zLength + (j * zLength));

                gl.glTexCoord2f(0.0f, 1.0f);
                gl.glVertex3f(xLength + (i * xLength), 0.0f, j * zLength);
                gl.glEnd();
            }
        }
        gl.glPopMatrix();


        // Zeichnen des Aussenrandes
        gl.glPushMatrix();
        gl.glTranslatef((-4.0f * xLength), 0.0f, (-4.0f * zLength));

        /* Neue, schickere Texturen benutzen:
         todo: TextureChooser ?
         Achtung: andere Texturen haben andere Koordinaten!
         */

        //Oberflaechen neben Spielbrett

        // Flaeche 1
         TextureManager.egypt_border().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(0.0f, 0.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 8.0f);
        gl.glVertex3f(8 * xLength, 0.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 8.0f);
        gl.glVertex3f(8 * xLength, 0.0f, -zLength / 2);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(0.0f, 0.0f, -zLength / 2);

        gl.glEnd();


        // Ecke 1
        TextureManager.egypt_corner().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(8 * xLength, 0.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(8 * xLength, 0.0f, -zLength / 2);

        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, -zLength / 2);
        
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, 0.0f);

        gl.glEnd();


        // Flaeche 2
        TextureManager.egypt_border().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(8 * xLength, 0.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 8.0f);
        gl.glVertex3f(8 * xLength, 0.0f, 8 * zLength);

        gl.glTexCoord2f(0.0f, 8.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, 8 * zLength);

        gl.glEnd();


        // Ecke 2
        TextureManager.egypt_corner().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, 8 * zLength);

        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(8 * xLength, 0.0f, 8 * zLength);

        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(8 * xLength, 0.0f, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, 8 * zLength + zLength / 2);

        gl.glEnd();


        // Flaeche 3
        TextureManager.egypt_border().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);
        
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(8 * xLength, 0.0f, 8 * zLength);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(8 * xLength, 0.0f, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(1.0f, 8.0f);
        gl.glVertex3f(0.0f, 0.0f, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(0.0f, 8.0f);
        gl.glVertex3f(0.0f, 0.0f, 8 * zLength);

        gl.glEnd();


        // Ecke 3
        TextureManager.egypt_corner().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-xLength / 2, 0.0f, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(-xLength / 2, 0.0f, 8 * zLength);

        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(0.0f, 0.0f, 8 * zLength);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(0.0f, 0.0f, 8 * zLength + zLength / 2);

        gl.glEnd();


        // Flaeche 4
        TextureManager.egypt_border().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(0.0f, 0.0f, 8 * zLength);

        gl.glTexCoord2f(1.0f, 8.0f);
        gl.glVertex3f(0.0f, 0.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 8.0f);
        gl.glVertex3f(-xLength / 2, 0.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-xLength / 2, 0.0f, 8 * zLength);

        gl.glEnd();


        // Ecke 4
        TextureManager.egypt_corner().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(0.0f, 0.0f, -zLength / 2);

        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(-xLength / 2, 0.0f, -zLength / 2);

        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(-xLength / 2, 0.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(0.0f, 0.0f, 0.0f);

        gl.glEnd();



        //Seitenflächen
        TextureManager.egypt_border2().bind();

        gl.glBegin(GL.GL_QUADS);

        // Flaeche 1
        gl.glNormal3f(0.0f, 0.0f, 1.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-xLength / 2, 0.0f, -zLength / 2);

        gl.glTexCoord2f(1.0f, 4.25f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, -zLength / 2);

        gl.glTexCoord2f(0.0f, 4.25f);
        gl.glVertex3f(8 * xLength + xLength / 2, -zLength / 2, -zLength / 2);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-xLength / 2, -zLength / 2, -zLength / 2);


        // Flaeche 2
        gl.glNormal3f(0.0f, 0.0f, 1.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, -zLength / 2);

        gl.glTexCoord2f(1.0f, 4.25f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(0.0f, 4.25f);
        gl.glVertex3f(8 * xLength + xLength / 2, -zLength / 2, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, -zLength / 2, -zLength / 2);


        // Flaeche 3
        gl.glNormal3f(1.0f, 0.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, 0.0f, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(1.0f, 4.25f);
        gl.glVertex3f(-xLength / 2, 0.0f, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(0.0f, 4.25f);
        gl.glVertex3f(-xLength / 2, -zLength / 2, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, -zLength / 2, 8 * zLength + zLength / 2);


        // Flaeche 4
        gl.glNormal3f(1.0f, 0.0f, 0.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-xLength / 2, 0.0f, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(1.0f, 4.25f);
        gl.glVertex3f(-xLength / 2, 0.0f, -zLength / 2);

        gl.glTexCoord2f(0.0f, 4.25f);
        gl.glVertex3f(-xLength / 2, -zLength / 2, -zLength / 2);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-xLength / 2, -zLength / 2, 8 * zLength + zLength / 2);

        gl.glEnd();


        //Unterboden
        TextureManager.egypt_mat_white().bind();

        gl.glBegin(GL.GL_QUADS);

        gl.glNormal3f(0.0f, 1.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-xLength / 2, -zLength / 2, -zLength / 2);

        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, -zLength / 2, -zLength / 2);

        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(8 * xLength + xLength / 2, -zLength / 2, 8 * zLength + zLength / 2);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-xLength / 2, -zLength / 2, 8 * zLength + zLength / 2);
        
        gl.glEnd();
        
        //Figuren zeichnen
        for (int i = 0; i < this.figures.length; i++) {
            for (int j = 0; j < this.figures[i].length; j++) {

                if (this.figures[i][j] != null) {
                    this.figures[i][j].draw();
                }
            }
        }
    }
}