package chess.presentation.drawables;

import chess.presentation.utilities.OpenGLContext;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

public abstract class ADrawable {
    protected GL gl;
    protected GLU glu;
    protected float[] position;
    
    public ADrawable() {
        this.gl = OpenGLContext.getInstance(null).getGL();
        this.glu = OpenGLContext.getInstance(null).getGLU();
    }
    //first part of drawing
    protected void beforeDraw() {
        gl.glPushMatrix();
        gl.glTranslatef(this.position[0], this.position[1], this.position[2]);
    }
    //second part of drawing
    protected abstract void mainDraw();
    //third part of drawing
    private  void afterDraw() {
        gl.glPopMatrix();
    }
    //all parts of drawing
    public void draw() {
        this.beforeDraw();
        this.mainDraw();
        this.afterDraw();
    }
    public void setPosition(float[] position) {
        this.position = position;
    }
    public float[] position() {
        return this.position;
    }
}