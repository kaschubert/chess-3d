package chess.presentation;

import chess.presentation.utilities.Field;
import chess.presentation.utilities.Players;
import chess.presentation.utilities.Resolution;
/**
 * Praesentationsschnittstelle implementierende Klasse.
 */
public class ChessPresentation implements IPresentation{
    
    /**
     * Grafikprogrammteil
     */
    private Engine engine;

    /**
     * Erzeugt eine neue ChessPresentation.
     * Auflösung wird im Singleton Resolution abgespeichert,
     * Engine wird erzeugt und damit gestartet
     * @param xResolution Aufloesung
     * @param yResolution Aufloesung
     */
    public ChessPresentation(int xResolution, int yResolution){
        Resolution res = Resolution.getInstance();
        res.setX(xResolution);
        res.setY(yResolution);
        engine = new Engine();
    }
    
    /**
     * Markierungseingaben aus der Logik.
     * @param x Position
     * @param y Position
     */
    public void logicInput(int x, int y) {
        ChessFieldModel.getInstance().markField(x, y);
    }

    /**
     * Demarkierungseingaben aus der Logik.
     */
    public void logicInput() {
        ChessFieldModel.getInstance().clearMarkedFields();
    }
    
    /**
     * Bewegungseingaben aus der Logik. 
     * @param x Position
     * @param y Position
     */
    public void logicMoveInput(int x, int y) {
        ChessFieldModel.getInstance().clearMarkedFields();
        ChessFieldModel.getInstance().moveFigure(new Field(x, y));
    }

    /**
     * Eingabe aus der Logik, die eine Figur setzt.
     * Falls auf diesem Feld bereits eine Figur steht, wird diese entfernt.
     * @param type Art der Figur
     * @param color Farbe der Figur
     * @param x Position
     * @param y Position
     */
    public void logicInputPutFigure(String type, String color, int x, int y) {
        ChessFieldModel.getInstance().putFigure(type, color, x, y);
    }

    /**
     * Eingabe aus der Logik, die eine Figur loescht. 
     * @param x Position
     * @param y Position
     */
    public void logicInputRemoveFigure(int x, int y) {
        ChessFieldModel.getInstance().removeFigure(x, y);
    }
    
    /**
     * Eingabe aus der Persistenz.
     */
    public void persistenceInput() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Eingabe aus der Logik, die den Status Schachmatt setzt.
     */
    public void logicInputCheckMate() {
        Status.getInstance().setCheckmate();
    }

    /**
     * Eingabe aus der Logik, die den Status Schach setzt.
     * @param check Status auf den Check gesetzt werden soll
     */
    public void logicInputCheck(boolean check) {
        Status.getInstance().setCheck(check);
    }

    /**
     * Eingabe aus dem Modul Net, die beide Spielernamen setzt.
     * @param nameWhite Name des weißen Spielers
     * @param nameBlack Name des schwarzen Spielers
     */
    public void netInputPlayer(String nameWhite, String nameBlack){
        Players.getInstance().setPlayer1(nameWhite);
        Players.getInstance().setPlayer2(nameBlack);
    }

    /**
     * Eingabe aus der Logik, die den Status Remis setzt.
     */
    public void logicInputRemis() {
        Status.getInstance().setRemis();
    }
}