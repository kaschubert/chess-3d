package chess;

import chess.presentation.*;
import chess.logic.*;
import chess.persistance.*;

/**
 * Schachhauptklasse von der aus alle anderen Klassen initialisiert werden.
 */
public class Chess {

    private static IPresentation presentation;
    private static ILogic logic;
    private static IPersistance persistance;

    /**
     * Haupteinstiegspunkt in die Anwendung.
     * 
     * @param args Argumente
     */
    public static void main(String[] args) {

        presentation = new ChessPresentation(800, 600);
        logic = new ChessLogic();
        persistance = new ChessPersistance();

        /*
         * Datenbank funktioniert. Replays und Savegames werden gespeichert.
         * Namen werden auf lowercase geforced.
         * 
         * Beispiel:
         * persistance.presentationInputStartgame("Zat", "Wtf");
         * persistance.logicInputLog("Test1");
         * persistance.logicInputLog("Test2");
         * persistance.logicInput("Wtf", "Spieldauer");
         * persistance.logicInput("Zat", "Anzahl Zuege", 200);
         * persistance.presentationInputSave();
         * persistance.presentationInputEndgame("Zat");
         */
    }

    public static IPresentation getPresentation() {
        return presentation;
    }

    public static ILogic getLogic() {
        return logic;
    }

    public static IPersistance getPersistance() {
        return persistance;
    }
}
